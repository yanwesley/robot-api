*** Settings ***
Library             AppiumLibrary  timeout=300

*** Test Cases ***
Transferencia
    Open Application  remote_url=http://localhost:4723/wd/hub  udid=5200cd5ab898257f  platformName=Android  deviceName=Galaxy A8+ (2018)  appPackage=br.com.bancobari.qa  appActivity=br.com.bancobari.OnboardingActivity

    Start Screen Recording
    Wait Until Element Is Visible  br.com.bancobari.qa:id/accessAccount  300
    Capture Page Screenshot
    Click Element       br.com.bancobari.qa:id/accessAccount
    Wait Until Element Is Visible  br.com.bancobari.qa:id/cpfEditText  300
    Input Text          br.com.bancobari.qa:id/cpfEditText  
    
    
    Capture Page Screenshot
    Click Element       br.com.bancobari.qa:id/continueButton
    Wait Until Element Is Visible  br.com.bancobari.qa:id/passwordEditText  300
    Input Text          br.com.bancobari.qa:id/passwordEditText  123Abc
    Capture Page Screenshot
    Click Element       br.com.bancobari.qa:id/continueButton
    Wait Until Element Is Visible  br.com.bancobari.qa:id/cancelButton  300
    Capture Page Screenshot
    Click Element       br.com.bancobari.qa:id/cancelButton
    # Wait Until Element Is Visible  br.com.bancobari.qa:id/showExtractButton  300
    # Capture Page Screenshot
    # Click Element       br.com.bancobari.qa:id/showExtractButton
    # Capture Page Screenshot
    # Click Element       br.com.bancobari.qa:id/showExtractButton
    # Capture Page Screenshot
    # Click Element       br.com.bancobari.qa:id/showExtractButton
    Wait Until Page Does Not Contain Element  br.com.bancobari.qa:id/imageView
    Capture Page Screenshot
    Click Element       br.com.bancobari.qa:id/drawerToggleButton
    Wait Until Element Is Visible  br.com.bancobari.qa:id/navItem2  300
    Capture Page Screenshot
    Click Element       br.com.bancobari.qa:id/navItem2
    Wait Until Element Is Visible  br.com.bancobari.qa:id/toolbarTitleTextView  300
    Capture Page Screenshot
    Element Text Should Be  br.com.bancobari.qa:id/toolbarTitleTextView  Transferência
    Input Text          br.com.bancobari.qa:id/contactSearchEditText  Luis Eduardo Gritz
    Element Text Should Be  br.com.bancobari.qa:id/nameTextView  Luis Eduardo Gritz
    Capture Page Screenshot
    Click Element       br.com.bancobari.qa:id/nameTextView
    Wait Until Element Is Visible  br.com.bancobari.qa:id/bankNameTextView  300
    Element Text Should Be  br.com.bancobari.qa:id/bankNameTextView  Bco Barigui Inv Fin S/A
    Capture Page Screenshot
    Click Element       br.com.bancobari.qa:id/bankNameTextView
    Wait Until Element Is Visible  br.com.bancobari.qa:id/textView2  300
    Element Text Should Be  br.com.bancobari.qa:id/textView2  Qual valor você quer transferir para esta conta?
    Capture Page Screenshot
    Clear Text          br.com.bancobari.qa:id/amountToTransferET
    Input Text          br.com.bancobari.qa:id/amountToTransferET  100000
    Capture Page Screenshot
    Click Element       br.com.bancobari.qa:id/continueButton
    Wait Until Element Is Visible  br.com.bancobari.qa:id/textView2  300
    Element Text Should Be  br.com.bancobari.qa:id/textView2  Você pode adicionar uma descrição a esta transferência, caso queira.
    Capture Page Screenshot
    Input Text          br.com.bancobari.qa:id/transferenceObsET  Automação Bari
    Click Element       br.com.bancobari.qa:id/continueButton
    Element Should Be Visible  //android.widget.TextView[@text='Banco']
    Element Should Be Visible  //android.widget.TextView[@text='Agência']
    Element Should Be Visible  //android.widget.TextView[@text='Conta']
    Element Should Be Visible  //android.widget.TextView[@text='Favorecido']
    Element Should Be Visible  //android.widget.TextView[@text='CPF']
    Element Should Be Visible  //android.widget.TextView[@text='Tipo de Conta']
    Element Should Be Visible  //android.widget.TextView[@text='Hoje']
    Element Should Be Visible  //android.widget.TextView[@text='Descrição']
    Capture Page Screenshot
    Click Element       br.com.bancobari.qa:id/continueButton
    Wait Until Element Is Visible  //android.widget.TextView[@text='Senha de transação']  300
    Input Text          br.com.bancobari.qa:id/passwordEditText  1470
    Capture Page Screenshot
    Click Element       br.com.bancobari.qa:id/confirmButton
    Wait Until Element Is Visible  //android.widget.TextView[@text='Transferência realizada com sucesso!']  300
    Capture Page Screenshot
    Stop Screen Recording    