*** Settings ***
Library    AppiumLibrary  timeout=300
Library    ImapLibrary2
Library    FakerLibrary    locale=pt_BR
Library    String

*** Variables ***
@{elements}	 Get Webelements	//android.widget.FrameLayout[1]/android.widget.CheckBox

*** Test Cases ***

Regressão Onobarding
    Open Application  remote_url=http://localhost:4723/wd/hub  udid=5200cd5ab898257f  platformName=Android  deviceName=Galaxy A8+ (2018)  appPackage=br.com.bancobari.homolog  appActivity=br.com.bancobari.OnboardingActivity
    
    #Massa de dados
    ${CPF}      FakerLibrary.cpf
    ${NOME}     FakerLibrary.name
    Set Test Variable  ${NOME}   
    ${PrimeiroNome}  Fetch From Left  ${NOME}   ${SPACE}

    #Tela inicial
    Wait Until Element Is Visible  br.com.bancobari.homolog:id/wantToBeClientButton  5000
    Click Element       br.com.bancobari.homolog:id/wantToBeClientButton
    
    #Informar CPF
    Wait Until Element Is Visible  br.com.bancobari.homolog:id/cpfInputLabel  5000
    Input Text    br.com.bancobari.homolog:id/cpfEditText    06537262950
    Wait Until Element Is Visible  br.com.bancobari.homolog:id/continueButton  5000
    Click Element  br.com.bancobari.homolog:id/continueButton
    
    #Informações que o APP está quase pronto
    Sleep    3
    Wait Until Page Contains Element   //android.widget.TextView
    Wait Until Element Is Visible  br.com.bancobari.homolog:id/continueButton  5000
    Element Text Should Be  //android.widget.TextView[@text='Nosso app está quase pronto…']  Nosso app está quase pronto…
    Click Element    br.com.bancobari.homolog:id/continueButton
    
    #Tela com o aceite dos termos
    Wait Until Element Is Visible    br.com.bancobari.homolog:id/formView  5000
    Element Text Should Be  //android.widget.TextView[@text='Vamos iniciar a abertura da sua conta, pode levar no máximo 10 minutos.']  Vamos iniciar a abertura da sua conta, pode levar no máximo 10 minutos.
    Element Should Be Disabled    br.com.bancobari.homolog:id/continueButton
    Click Element    //android.widget.FrameLayout[1]/android.widget.CheckBox
    Click Element    //android.widget.FrameLayout[2]/android.widget.CheckBox
    Click Element  br.com.bancobari.homolog:id/continueButton

    #Habilitar localização
    Wait Until Element Is Visible  //android.widget.LinearLayout  5000
    Element Text Should Be  //android.widget.TextView[1][@text='Habilitar Localização']  Habilitar Localização
    Click Element  br.com.bancobari.homolog:id/confirmButton
    Wait Until Element Is Visible    com.android.packageinstaller:id/permission_allow_button   5000
    Click Element  com.android.packageinstaller:id/permission_allow_button

    #Dados Pessoais Nome
    Wait Until Element Is Visible    br.com.bancobari.homolog:id/toolbarTitleTextView  5000
    Element_text_should_be    br.com.bancobari.homolog:id/textView  Qual é o seu nome completo?  Qual é o seu nome completo?
    Input Text    br.com.bancobari.homolog:id/nameEditText  ${NOME}
    Wait Until Element Is Visible  br.com.bancobari.homolog:id/continueButton
    Click Element    br.com.bancobari.homolog:id/continueButton
    #Dados Pessoais Nascimento
    Wait Until Element Is Visible    br.com.bancobari.homolog:id/intoTextView  5000
    Element_text_should_be    br.com.bancobari.homolog:id/intoTextView  ${PrimeiroNome}, quando você nasceu?  
    Input Text    br.com.bancobari.homolog:id/dateEditText  20/11/2015
    Element Should Be Visible        br.com.bancobari.homolog:id/ageImageView
    ${MenorIdade}  Get Text    //hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.widget.TextView
    Should Be Equal    ${MenorIdade}     Para abrir a conta, você precisa ter 18 anos ou mais.
    Clear Text  br.com.bancobari.homolog:id/dateEditText
    Input Text    br.com.bancobari.homolog:id/dateEditText  20/11/1988
    Wait Until Element Is Visible  br.com.bancobari.homolog:id/continueButton
    Click Element    br.com.bancobari.homolog:id/continueButton

    #Dados Pessoais Email
    Wait Until Element Is Visible    br.com.bancobari.homolog:id/textView  5000
    Input Text    br.com.bancobari.homolog:id/emailEditText  yan.lima@primecontrol.com.br
    Click Element  br.com.bancobari.homolog:id/continueButton

    #Dados Pessoais Validação Email
    Wait Until Element Is Visible    br.com.bancobari.homolog:id/changeDataButton  5000
    ${bodyreturn}  Email Verification                    no-reply@stage.bancobari.com.br      abaixo para
    Log  ${bodyreturn}
    ${teste}  Fetch From Right        ${bodyreturn}     <td style="padding-top: 48px; padding-bottom: 54px;">
    ${teste1}  Fetch From Left        ${teste}          O cÃ³digo Ã© valido por 10 minutos, apÃ³s a expiraÃ§Ã£o solicite
    ${teste2}  Fetch From Right       ${teste1}         font-weight: bold;
    ${teste3}  Fetch From Left        ${teste2}         </p>
    ${Code}  Fetch From Right         ${teste3}         ">
    log  ${Code}
    Set Test Variable    ${Code}  
    Discard Email  no-reply@stage.bancobari.com.br      abaixo para

    


    
    # Wait Until Element Is Visible  br.com.bancobari.qa:id/cpfEditText  30
    # Input Text          br.com.bancobari.qa:id/cpfEditText  06537262950

    # Setva
    # ${index} =  Find index
    # @{list}=  Create List    //android.widget.TextView
    #  ${list} =     Create List  androidWidget
    #  ${args} =     Create Dictionary    args=${list}

  
*** Keywords ***

Email Verification
    [Arguments]  ${sender}  ${content}
    Open Mailbox    host=outlook.office365.com    user=yan.lima@primecontrol.com.br  password=463166Yan  port=993 
    ${LATEST}    Wait For Email    sender=${sender}    timeout=60000
    ${body}    Get Email Body  ${LATEST}
    Should Contain    ${body}    ${content}
    Close Mailbox
    [Return]  ${body}

Discard Email
    [Arguments]  ${sender}  ${content}
    Open Mailbox    host=outlook.office365.com    user=yan.lima@primecontrol.com.br  password=463166Yan  port=993 
    ${LATEST}    Wait For Email    sender=${sender}    timeout=60000
    Delete Email  ${LATEST}

Teste33
    set_test_variable    ${teste34}
    ${teste34}  font-weight: bold;                              font-family: Arial, sans-serif;                              width: 100%;                              text-align: center;                              margin: 0;                            ">
