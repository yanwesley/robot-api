import boto3
import json
import decimal
import ast
import uuid
import requests
import re
import struct
import binascii
import requests
import hashlib
import decimal
from datetime import datetime
from boto3.dynamodb.conditions import Key, Attr
from tkinter import *
from decimal import Decimal

def help_converter(item):
    item2 = json.loads(item)
    return item2

def b64encode(s, altchars=None):
    # Strip off the trailing newline
    encoded = binascii.b2a_base64(s)[:-1]
    if altchars is not None:
        return _translate(encoded, {'+': altchars[0], '/': altchars[1]})
    return encoded

# def help_converter(item, key):
#     item[key] = json.loads(item[key])
#     return item

def consulta_dynamo(nameTable,nameAttribute,value):
    client = boto3.client('dynamodb', region_name='us-east-1')
    table = boto3.resource('dynamodb').Table(nameTable)
    response = table.scan(FilterExpression=Attr(nameAttribute).contains(value))
    item = response['Items']
    while 'LastEvaluatedKey' in response:
        response = table.scan(FilterExpression=Attr(nameAttribute).contains(value), ExclusiveStartKey=response['LastEvaluatedKey'])
        item.append(response['Items'])
    return item

def consulta_dynamo_attribute(nameTable,nameAttribute):
    att = 'attribute_exists('+str(nameAttribute)+')'
    client = boto3.client('dynamodb', region_name='us-east-1')
    table = boto3.resource('dynamodb').Table(nameTable)
    response = table.scan(FilterExpression=att)
    #response = table.scan(AttributesToGet=[nameAttribute])
    item = response['Items']
    while 'LastEvaluatedKey' in response:
        response = table.scan(FilterExpression=att, ExclusiveStartKey=response['LastEvaluatedKey'])
        item.append(response['Items'])
    return item

def consulta_dynamo_two_attributes(nameTable,nameAttribute1,value1,nameAttribute2,value2):
    client = boto3.client('dynamodb', region_name='us-east-1')
    table = boto3.resource('dynamodb').Table(nameTable)
    response = table.scan(FilterExpression=Attr(nameAttribute1).contains(value1) & Attr(nameAttribute2).contains(value2))
    item = response['Items']
    while 'LastEvaluatedKey' in response:
        response = table.scan(FilterExpression=Attr(nameAttribute1).contains(value1) & Attr(nameAttribute2).contains(value2), ExclusiveStartKey=response['LastEvaluatedKey'])
        item.append(response['Items'])
    return item

def consulta_dynamo_Key(nameTable,nameKey,value):
    client = boto3.client('dynamodb', region_name='us-east-1')
    table = boto3.resource('dynamodb').Table(nameTable)
    response = table.query(KeyConditionExpression=Key(nameKey).eq(int(value)))
    item = response['Items']
    return item

def gera_guid():
    a = uuid.uuid4()
    return str(a)

def consulta_dynamo_two_attributes_int_and_bool(nameTable,nameAttribute1,value1,nameAttribute2,value2):
    client = boto3.client('dynamodb', region_name='us-east-1')
    table = boto3.resource('dynamodb').Table(nameTable)
    response = table.scan(FilterExpression=Attr(nameAttribute1).eq(int(value1)) & Attr(nameAttribute2).eq(bool(value2)))
    item = response['Items']
    while 'LastEvaluatedKey' in response:
        response = table.scan(FilterExpression=Attr(nameAttribute1).eq(int(value1)) & Attr(nameAttribute2).eq(bool(value2)), ExclusiveStartKey=response['LastEvaluatedKey'])
        item.append(response['Items'])
    return item

def consulta_dynamo_int(nameTable,nameAttribute,value):
    client = boto3.client('dynamodb', region_name='us-east-1')
    table = boto3.resource('dynamodb').Table(nameTable)
    response = table.scan(FilterExpression=Attr(nameAttribute).eq(int(value)))
    item = response['Items']
    while 'LastEvaluatedKey' in response:
        response = table.scan(FilterExpression=Attr(nameAttribute).eq(int(value)), ExclusiveStartKey=response['LastEvaluatedKey'])
        item.append(response['Items'])
    return item

def consulta_dynamo_bool(nameTable,nameAttribute,value):
    client = boto3.client('dynamodb', region_name='us-east-1')
    table = boto3.resource('dynamodb').Table(nameTable)
    response = table.scan(FilterExpression=Attr(nameAttribute).eq(bool(value)))
    item = response['Items']
    while 'LastEvaluatedKey' in response:
        response = table.scan(FilterExpression=Attr(nameAttribute).eq(bool(value)), ExclusiveStartKey=response['LastEvaluatedKey'])
        item.append(response['Items'])
    return item

def chamada_document(leadId,deviceId):
    
    url = f"https://api.qa.bancobari.com.br/onboarding/v1/leads/{leadId}/documentPicture"

    headers = {
    'Content-Type': 'multipart/form-data',
    'Accept': 'application/json',
    'User-Agent':f'{deviceId}'
    }

    files = {
        'picture': ('cnh.jpg', open(r'C:\Users\yanl\Desktop\Projetos\cadastro\Resources\Images\cnh.jpeg','rb'),'multipart/form-data', headers)
        }

    payload = {'documentType': 'CNH'}    

    response = requests.request("PUT", url, data = payload, files = files)
   
    print(response.status_code)
    print(response.text.encode('utf8'))

    return response.status_code

def chamada_selfie(leadId,deviceId):
    
    url = f"https://api.qa.bancobari.com.br/onboarding/v1/leads/{leadId}/selfie"

    headers = {
    'Content-Type': 'multipart/form-data',
    'Accept': 'application/json',
    'User-Agent': f'{deviceId}'
    }

    files = {
        'pictureFile': ('cnh.jpg', open(r'C:\Users\yanl\Desktop\Projetos\cadastro\Resources\Images\selfie.jpg','rb'),'multipart/form-data', headers)
        }

    payload = {'documentType': 'CNH'}    

    response = requests.request("PUT", url, data = payload, files = files)
   
    print(response.status_code)
    print(response.text.encode('utf8'))

    return response.status_code

# if __name__ == '__main__':
#     chamada_document('44357579-146f-4cc1-abf4-2ba9a382a6bd','99ec2bad-5d68-438d-83d4-7a8267a7d3c2')  
#     #'44357579-146f-4cc1-abf4-2ba9a382a6bd','99ec2bad-5d68-438d-83d4-7a8267a7d3c2')

def encrypt_string(hash_string):
    sha_signature = \
        hashlib.sha256(hash_string.encode()).hexdigest()
    return sha_signature

def delete_dynamo(nameTable,nameAttribute,value,nameAttribute2 = "",value2 = ""):
    print("Attempting a conditional delete...")
    client = boto3.client('dynamodb', region_name='us-east-1')

    if nameTable == 'Barigui.Services.Account_CheckingAccount':
        response = client.delete_item(TableName=nameTable, Key={nameAttribute: {"N": f'{value}'},nameAttribute2: {"S": value2}})
    else:
        response = client.delete_item(TableName=nameTable, Key={nameAttribute: {"S": value}})

    print("Delete Item succeeded:")
    print(response)

def  input_sms():
    codigo    = input("Insira o código de ativação enviado por SMS:  ").lower()
    print(f"Código capturado: {codigo}")
    return codigo

def update_customerscopes(customerId):
    nameTable = 'Barigui.Services.FeatureFlag_Customerscopes'
    nameAttribute = 'CustomerId'
    value = f'{customerId}'
    client = boto3.client('dynamodb', region_name='us-east-1')
    response = client.get_item(TableName=nameTable, Key={nameAttribute: {"S": value}}) 
    print(response.get('Item'))
    item = response['Item']
    print(item['Scopes'])
    item['Scopes'] = {'L': [{'M': {'Code': {'S': 'ACCOUNT'}, 'IsActive': {'BOOL': True}}}, {'M': {'Code': {'S': 'MFA'}, 'IsActive': {'BOOL': True}}}]}
    response = client.put_item(TableName=nameTable, Item=item)
    print(response)

def update_leads(LeadId):
    nameTable = 'Barigui.Services.Onboarding_Leads'
    nameAttribute = 'Id'
    value = f'{LeadId}'
    client = boto3.client('dynamodb', region_name='us-east-1')
    response = client.get_item(TableName=nameTable, Key={nameAttribute: {"S": value}}) 
    print(response.get('Item'))
    item = response['Item']
    print(item['VerifiedEmail'])
    item['VerifiedEmail'] = {'BOOL': True}
    print(item['VerifiedMobilePhoneNumber'])
    item['VerifiedMobilePhoneNumber'] = {'BOOL': True}
    print(item['IndividualRegistrationNovaVida'])
    item['DocumentIsValid'] = {'BOOL': True}
    print(item['DocumentIsValid'])
    item['IndividualRegistrationNovaVida'] = {'M': {'DataInscricao': {'S': '02/02/2008'}, 'ConsultaDataHora': {'S': '06/02/2020 16:21:19'}, 'DataNascimento': {'S': '27/10/1993'}, 'NormalizadoAnoObito': {'S': '0'}, 'CPF': {'S': '065.372.629-50'}, 'ConsultaDigitoVerificador': {'S': '00'}, 'SituacaoCadastral': {'S': 'REGULAR'}, 'ConsultaComprovante': {'S': '0D66.6ACA.49B1.A7B5'}, 'Nome': {'S': 'EDUARDO ANTONIO BRUSAMOLIN'}}}
    response = client.put_item(TableName=nameTable, Item=item)
    print(response)

def get_user_input(label=""):
    root = Tk()
    app = Input(root, label)
    root.mainloop()
    return app.getTextInput()

class Input:

  def init(self, master=None, label = ""):
    self.text = ' '
    self.root = master
    self.widget1 = Frame(master)
    self.widget1.pack()
    self.root.title('Robot')
    if (len(label) > 0):
      self.msg = Label(self.widget1, text=label)
      self.msg.pack ()
    self.input = Entry()
    self.input["width"] = 25
    self.input.pack(side=LEFT)
    self.btnBuscar = Button(text="OK", width=10)
    self.btnBuscar['command'] = self.saveEntry
    self.btnBuscar.pack(side=RIGHT)

  def saveEntry(self):
    self.text = self.input.get()
    self.root.destroy()
    pass

  def getTextInput(self):
    return self.text

def  retorna_data(stringData):
    date = str(datetime.strptime(stringData.strip(' \t\r\n'), '%b %d %Y'))
    data = date[0:10] 
    print(data)
    return  data

if __name__ == '__main__':
    update_leads()