from tkinter import * 

def userInputSMS(label=""):
    root = Tk()
    app = Input(root, label)
    root.mainloop()
    return app.getTextInput()

class Input:

  def __init__(self, master=None, label = ""):
    self.text = ' '
    self.root = master
    self.widget1 = Frame(master)
    self.widget1.pack()
    self.root.title('Robot')
    if (len(label) > 0):
      self.msg = Label(self.widget1, text=label)
      self.msg.pack ()
    self.input = Entry()
    self.input["width"] = 25
    self.input.pack(side=LEFT)
    self.btnBuscar = Button(text="OK", width=10)
    self.btnBuscar['command'] = self.saveEntry
    self.btnBuscar.pack(side=RIGHT)

  def saveEntry(self):
    self.text = self.input.get()
    self.root.destroy()
    pass

  def getTextInput(self):
    return self.text

# root = Tk()
# app = Input(root)
# root.mainloop()