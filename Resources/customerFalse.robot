***Keywords***
02_PUT_{{url}}/v1/Leads/{{lead_id}}FALSE
    [Arguments]  ${lead_id}  ${name}  ${cpf}

    ${json_string}=    catenate
    ...     {
    ...         "name": "${name}",
    ...         "document": "${cpf}",
    ...         "termsAcceptance": [
    ...         {
    ...       "type": "InformationPrivacy",
    ...       "contract": "https://s3.amazonaws.com/",
    ...       "acceptanceDate": "2020-01-30T12:28:20.071Z"
    ...         },
    ...         {
    ...         "type": "SCRAcceptance",
    ...          "contract": "https://s3.amazonaws.com/",
    ...          "acceptanceDate": "2020-01-30T12:28:20.071Z"
    ...         }
    ...         ]
    ...     }

    log  ${json_string}    
    ${HEADERS}  Create Dictionary
    ...     accept=application/json
    ...     User-Agent=${guidDevice}
    ...     Content-Type=application/json-patch+json   
    ${RESPOSTA}     Put Request     newsession      onboarding/v1/leads/${lead_id}
    ...     data=${json_string}
    ...     headers=${HEADERS}
    Log                             ${RESPOSTA.text}
    Should Be Equal As Strings      ${RESPOSTA.status_code}    400

03_PUT_{{url}}/v1/Leads/{{lead_id}}/BirthDateFALSE
    [Arguments]  ${lead_id}  ${dtnasc}
    ${dt}  catenate  SEPARATOR=  ${dtnasc}  T00:00:00
    ${payload}   Create Dictionary    birthDate=${dt}
    ${json}  Convert Dic to Json  ${payload}
    ${HEADERS}  Create Dictionary
    ...     accept=application/json
    ...     User-Agent=${guidDevice}
    ...     Content-Type=application/json-patch+json   
    ${RESPOSTA}     Put Request     newsession      onboarding/v1/leads/${lead_id}/BirthDate
    ...     data=${json}
    ...     headers=${HEADERS}
    Log                             ${RESPOSTA.text}
    ${respostarequest}  Set Variable   ${RESPOSTA.json()}
    Set Test Variable    ${respostarequest}
    Should Be Equal  ${respostarequest['messages'][0]}  Person must be, at least, 18 years old.
    Should Be Equal As Strings      ${RESPOSTA.status_code}    400

05_PUT_{{url}}/v1/Leads/{{lead_id}}/EmailFALSE
    [Arguments]  ${lead_id}  ${email}
    ${payload}   Create Dictionary    email=${email}
    ${json}  Convert Dic to Json  ${payload}
    ${HEADERS}  Create Dictionary
    ...     accept=application/json
    ...     User-Agent=${guidDevice}
    ...     Content-Type=application/json-patch+json   
    ${RESPOSTA}     Put Request     newsession      onboarding/v1/leads/${lead_id}/Email
    ...     data=${json}
    ...     headers=${HEADERS}
    Log                             ${RESPOSTA.text}
    ${respostarequest}  Set Variable   ${RESPOSTA.json()}
    Set Test Variable    ${respostarequest}
    Should Be Equal  ${respostarequest['messages'][0]}  'Email' is not a valid email address.
    Should Be Equal As Strings      ${RESPOSTA.status_code}    400

07_PUT_{{url}}/v1/Leads/{{lead_id}}/mobilePhoneNumberFALSE
    [Arguments]  ${lead_id}  ${telefone}
    ${payload}   Create Dictionary    countryPhoneCode=55   mobilePhoneNumber=${telefone}
    ${json}  Convert Dic to Json  ${payload}
    ${HEADERS}  Create Dictionary
    ...     accept=application/json
    ...     User-Agent=${guidDevice}
    ...     Content-Type=application/json-patch+json   
    ${RESPOSTA}     Put Request     newsession      onboarding/v1/leads/${lead_id}/mobilePhoneNumber
    ...     data=${json}
    ...     headers=${HEADERS}
    Log                             ${RESPOSTA.text}
    Should Be Equal As Strings      ${RESPOSTA.status_code}    400

17_PUT_{{url}}/v1/Leads/{{lead_id}}/documentPictureFALSE
    [Arguments]  ${lead_id}

    ${status}  Chamada Document  ${lead_id}  ${guidDevice}
    Should Be Equal As Strings  ${status}  400

18_PUT_{{url}}/v1/Leads/{{lead_id}}/selfieFALSE
    [Arguments]  ${lead_id}

    ${status}  Chamada Selfie  ${lead_id}  ${guidDevice}
    Should Be Equal As Strings  ${status}  400

