***Keywords***
01_GET_/v1/Leads/contract?types=InformationPrivacy,SCRAcceptance
    ${payload}   Create Dictionary    contract=string   type=string
    ${json}  Convert Dic to Json  ${payload}  
    ${HEADERS}  Create Dictionary
    ...     accept=application/json
    ...     User-Agent=${guidDevice}     
    ${RESPOSTA}     Get Request     newsession      onboarding/v1/leads/contract?types=InformationPrivacy,SCRAcceptance   
    ...     headers=${HEADERS}
    Log                             ${RESPOSTA.text}
    Should Be Equal As Strings      ${RESPOSTA.status_code}    200
02_PUT_{{url}}/v1/Leads/{{lead_id}}
    [Arguments]  ${lead_id}  ${name}  ${cpf}

    ${json_string}=    catenate
    ...     {
    ...         "name": "${name}",
    ...         "document": "${cpf}",
    ...         "termsAcceptance": [
    ...         {
    ...       "type": "InformationPrivacy",
    ...       "contract": "https://s3.amazonaws.com/",
    ...       "acceptanceDate": "2020-01-30T12:28:20.071Z"
    ...         },
    ...         {
    ...         "type": "SCRAcceptance",
    ...          "contract": "https://s3.amazonaws.com/",
    ...          "acceptanceDate": "2020-01-30T12:28:20.071Z"
    ...         }
    ...         ]
    ...     }

    log  ${json_string}    
    ${HEADERS}  Create Dictionary
    ...     accept=application/json
    ...     User-Agent=${guidDevice}
    ...     Content-Type=application/json-patch+json   
    ${RESPOSTA}     Put Request     newsession      onboarding/v1/leads/${lead_id}
    ...     data=${json_string}
    ...     headers=${HEADERS}
    Log                             ${RESPOSTA.text}
    Should Be Equal As Strings      ${RESPOSTA.status_code}    202
03_PUT_{{url}}/v1/Leads/{{lead_id}}/BirthDate
    [Arguments]  ${lead_id}  ${dtnasc}
    ${dt}  catenate  SEPARATOR=  ${dtnasc}  T00:00:00
    ${payload}   Create Dictionary    birthDate=${dt}
    ${json}  Convert Dic to Json  ${payload}
    ${HEADERS}  Create Dictionary
    ...     accept=application/json
    ...     User-Agent=${guidDevice}
    ...     Content-Type=application/json-patch+json   
    ${RESPOSTA}     Put Request     newsession      onboarding/v1/leads/${lead_id}/BirthDate
    ...     data=${json}
    ...     headers=${HEADERS}
    Log                             ${RESPOSTA.text}
    Should Be Equal As Strings      ${RESPOSTA.status_code}    202
04_PUT_{{url}}/v1/Leads/{{lead_id}}/geolocation
    [Arguments]  ${lead_id}  
    ${json}=  catenate
    ...       {
    ...       "latitude": 1,
    ...       "longitude": 1,
    ...       "complement": "string"
    ...       }
    ${HEADERS}  Create Dictionary
    ...     accept=application/json
    ...     User-Agent=${guidDevice}
    ...     Content-Type=application/json-patch+json   
    ${RESPOSTA}     Put Request     newsession      onboarding/v1/leads/${lead_id}/geolocation
    ...     data=${json}
    ...     headers=${HEADERS}
    Log                             ${RESPOSTA.text}
    Should Be Equal As Strings      ${RESPOSTA.status_code}    202
05_PUT_{{url}}/v1/Leads/{{lead_id}}/Email
    [Arguments]  ${lead_id}  ${email}
    ${payload}   Create Dictionary    email=${email}
    ${json}  Convert Dic to Json  ${payload}
    ${HEADERS}  Create Dictionary
    ...     accept=application/json
    ...     User-Agent=${guidDevice}
    ...     Content-Type=application/json-patch+json   
    ${RESPOSTA}     Put Request     newsession      onboarding/v1/leads/${lead_id}/Email
    ...     data=${json}
    ...     headers=${HEADERS}
    Log                             ${RESPOSTA.text}
    Should Be Equal As Strings      ${RESPOSTA.status_code}    202
06_PUT_{{url}}/v1/Leads/{{lead_id}}/verifyEmail
    [Arguments]  ${lead_id}
    ${payload}   Create Dictionary    validationCode=0000
    ${json}  Convert Dic to Json  ${payload}
    ${HEADERS}  Create Dictionary
    ...     accept=application/json
    ...     User-Agent=${guidDevice}
    ...     Content-Type=application/json-patch+json   
    ${RESPOSTA}     Put Request     newsession      onboarding/v1/leads/${lead_id}/verifyEmail
    ...     data=${json}
    ...     headers=${HEADERS}
    Log                             ${RESPOSTA.text}
    Should Be Equal As Strings      ${RESPOSTA.status_code}    202
07_PUT_{{url}}/v1/Leads/{{lead_id}}/mobilePhoneNumber
    [Arguments]  ${lead_id}  ${telefone}
    ${payload}   Create Dictionary    countryPhoneCode=55   mobilePhoneNumber=${telefone}
    ${json}  Convert Dic to Json  ${payload}
    ${HEADERS}  Create Dictionary
    ...     accept=application/json
    ...     User-Agent=${guidDevice}
    ...     Content-Type=application/json-patch+json   
    ${RESPOSTA}     Put Request     newsession      onboarding/v1/leads/${lead_id}/mobilePhoneNumber
    ...     data=${json}
    ...     headers=${HEADERS}
    Log                             ${RESPOSTA.text}
    Should Be Equal As Strings      ${RESPOSTA.status_code}    202
08_PUT_{{url}}/v1/Leads/{{lead_id}}/verifyMobilePhoneNumber
    [Arguments]  ${lead_id}
    ${payload}   Create Dictionary    validationCode=0000
    ${json}  Convert Dic to Json  ${payload}
    ${HEADERS}  Create Dictionary
    ...     accept=application/json
    ...     User-Agent=${guidDevice}
    ...     Content-Type=application/json-patch+json   
    ${RESPOSTA}     Put Request     newsession      onboarding/v1/leads/${lead_id}/verifyMobilePhoneNumber
    ...     data=${json}
    ...     headers=${HEADERS}
    Log                             ${RESPOSTA.text}
    Should Be Equal As Strings      ${RESPOSTA.status_code}    202
09_PUT_{{url}}/v1/Leads/{{lead_id}}/personalInfo
    [Arguments]  ${lead_id}  ${gender}  ${motherName}  ${State}  ${City}  ${maritalStatus}  ${spouseName}
    ${payload}   Create Dictionary   
    ...     gender=${gender}
    ...     motherName=${motherName}
    ...     country=BRA
    ...     state=${State}
    ...     city=${City}
    ...     maritalStatus=${maritalStatus}
    ...     spouseName=${spouseName}   
    ${json}  Convert Dic to Json  ${payload}
    ${HEADERS}  Create Dictionary
    ...     accept=application/json
    ...     User-Agent=${guidDevice}
    ...     Content-Type=application/json-patch+json   
    ${RESPOSTA}     Put Request     newsession      onboarding/v1/leads/${lead_id}/personalInfo
    ...     data=${json}
    ...     headers=${HEADERS}
    Log                             ${RESPOSTA.text}
    Should Be Equal As Strings      ${RESPOSTA.status_code}    202
10_PUT_{{url}}/v1/Leads/{{lead_id}}/homeAddress
    [Arguments]  ${lead_id}  ${postalCode}  ${addressLine1}  ${number}  ${neighborhood}  ${city}  ${state}
    ${payload}   Create Dictionary   
    ...     postalCode=${postalCode}
    ...     addressLine1=${addressLine1}
    ...     addressLine2=string
    ...     number=${number}
    ...     neighborhood=${neighborhood}
    ...     city=${city}
    ...     state=${state}
    ...     country=BRA     
    ${json}  Convert Dic to Json  ${payload}
    ${HEADERS}  Create Dictionary
    ...     accept=application/json
    ...     User-Agent=${guidDevice}
    ...     Content-Type=application/json-patch+json   
    ${RESPOSTA}     Put Request     newsession      onboarding/v1/leads/${lead_id}/homeAddress
    ...     data=${json}
    ...     headers=${HEADERS}
    Log                             ${RESPOSTA.text}
    Should Be Equal As Strings      ${RESPOSTA.status_code}    202
11_PUT_{{url}}/v1/Leads/{{lead_id}}/commercialAddress
    [Arguments]  ${lead_id}
    ${json}=  catenate   
    ...     {
    ...     "hasCommercialAddress": false,
    ...     "postalCode": "string",
    ...     "addressLine1": "string",
    ...     "addressLine2": "string",
    ...     "number": "string",
    ...     "neighborhood": "string",
    ...     "city": "string",
    ...     "state": "PR",
    ...     "country": "BRA",
    ...     "name": "string"
    ...     }
    ${HEADERS}  Create Dictionary
    ...     accept=application/json
    ...     User-Agent=${guidDevice}
    ...     Content-Type=application/json-patch+json   
    ${RESPOSTA}     Put Request     newsession      onboarding/v1/leads/${lead_id}/commercialAddress
    ...     data=${json}
    ...     headers=${HEADERS}
    Log                             ${RESPOSTA.text}
    Should Be Equal As Strings      ${RESPOSTA.status_code}    202
12_GET_{{url}}/v1/Leads/professions       
    ${HEADERS}  Create Dictionary
    ...     accept=application/json
    ...     User-Agent=${guidDevice}
    ${RESPOSTA}     Get Request     newsession      onboarding/v1/Leads/professions   
    ...     headers=${HEADERS}
    Log                             ${RESPOSTA.text}
    Should Be Equal As Strings      ${RESPOSTA.status_code}    200
13_PUT_{{url}}/v1/Leads/{{lead_id}}/profession
    [Arguments]  ${lead_id}
    ${payload}   Create Dictionary    profession=ESPR
    ${json}  Convert Dic to Json  ${payload}
    ${HEADERS}  Create Dictionary
    ...     accept=application/json
    ...     User-Agent=${guidDevice}
    ...     Content-Type=application/json-patch+json   
    ${RESPOSTA}     Put Request     newsession      onboarding/v1/leads/${lead_id}/profession
    ...     data=${json}
    ...     headers=${HEADERS}
    Log                             ${RESPOSTA.text}
    Should Be Equal As Strings      ${RESPOSTA.status_code}    202

14_PUT_{{url}}/v1/Leads/{{lead_id}}/income
    [Arguments]  ${lead_id}
    ${payload}   Create Dictionary
    ...    incomeRange=RANGE1
    ${json}  Convert Dic to Json  ${payload}
    ${HEADERS}  Create Dictionary
    ...     accept=application/json
    ...     User-Agent=${guidDevice}
    ...     Content-Type=application/json-patch+json   
    ${RESPOSTA}     Put Request     newsession      onboarding/v1/leads/${lead_id}/income
    ...     data=${json}
    ...     headers=${HEADERS}
    Log                             ${RESPOSTA.text}
    Should Be Equal As Strings      ${RESPOSTA.status_code}    202
15_GET_{{url}}/v1/Leads/assetsrange     
    ${HEADERS}  Create Dictionary
    ...     accept=application/json
    ...     User-Agent=${guidDevice}
    ${RESPOSTA}     Get Request     newsession      onboarding/v1/Leads/assetsrange    
    ...     headers=${HEADERS}
    Log                             ${RESPOSTA.text}
    Should Be Equal As Strings      ${RESPOSTA.status_code}    200

16_PUT_{{url}}/v1/Leads/{{lead_id}}/assets
    [Arguments]  ${lead_id}
    ${payload}   Create Dictionary    assetsRange=26k-50k
    ${json}  Convert Dic to Json  ${payload}
    ${HEADERS}  Create Dictionary
    ...     accept=application/json
    ...     User-Agent=${guidDevice}
    ...     Content-Type=application/json-patch+json   
    ${RESPOSTA}     Put Request     newsession      onboarding/v1/leads/${lead_id}/assets
    ...     data=${json}
    ...     headers=${HEADERS}
    Log                             ${RESPOSTA.text}
    Should Be Equal As Strings      ${RESPOSTA.status_code}    202

17_PUT_{{url}}/v1/Leads/{{lead_id}}/documentPicture
    [Arguments]  ${lead_id}

    ${status}  Chamada Document  ${lead_id}  ${guidDevice}
    Should Be Equal As Strings  ${status}  202

    # ${GuidRandom}  Gera Guid
    # ${GuidRandom2}  Gera Guid
    # ${file_data}=  Get Binary File  ${CURDIR}\\Images\\cnh.jpg
    # ${base64}=  B64encode  ${file_data}   
    # #${bytes} =    Encode String To Bytes    ${file_data}    UTF-8
    # #log  ${bytes} 
    # ${json_string}=    catenate
    # ...     {    
    # ...         "picture": "${base64}",
    # ...         "documentType": "CNH",
    # ...         "requestId": "${GuidRandom}",
    # ...         "leadId": "${lead_id}",
    # ...         "deviceId": "${GuidRandom2}",
    # ...         "ipAddress": "192.168.1.1"    
    # ...     }
    # # ${file_data}=  Get Binary File  ${CURDIR}\\Images\\cnh.jpg
    # # ${bytes} =    Encode String To Bytes    ${file_data}    UTF-8
    # # ${payload}   Create Dictionary    DocumentType=CNH  Picture=${bytes}   
    # #${files}=  Create Dictionary  Picture  ${base64} 
    # # # ${json}=    evaluate    json.dumps(${files})    json
    # # ${json}  Convert Dic to Json  ${payload}
    # # # achar um forma de colocar o arquivo, tá embassado rs
    # #${data}=  Evaluate  {'Picture': ('cnh.jpg', open("C:/Projetos/automacaoBari/cadastro/Resources/Images/cnh.jpg", 'r+b'), 'image/jpeg'), 'extract': (None, 'false'), 'fileId': (None, 'b3duLWZpbGVzL2ZmZmZmZmZmYTQyNDVmODAvMjAxNTY*')}
    # Abrir conexao   https://api.qa.bancobari.com.br
    # ${HEADERS}  Create Dictionary
    # ...     accept=application/json
    # ...     User-Agent=${guidDevice}
    # ...     Content-Type=multipart/form-data
    # ${RESPOSTA}     Put Request     newsession      onboarding/v1/leads/${lead_id}/documentPicture
    # ...     data=${json_string}    
    # ...     headers=${HEADERS}
    # Log                             ${RESPOSTA.text}
    # Should Be Equal As Strings      ${RESPOSTA.status_code}    202

18_PUT_{{url}}/v1/Leads/{{lead_id}}/selfie
    [Arguments]  ${lead_id}

    ${status}  Chamada Selfie  ${lead_id}  ${guidDevice}
    Should Be Equal As Strings  ${status}  202

    # # ${payload}   Create Dictionary    contract=string   type=string
    # # ${json}  Convert Dic to Json  ${payload}
    # # achar um forma de colocar o arquivo, tá embassado rs
    # Abrir conexao   https://api.qa.bancobari.com.br
    # ${HEADERS}  Create Dictionary
    # ...     accept=application/json
    # ...     User-Agent=bee671c0-bf58-4813-abec-9763f4f491b8
    # ...     Content-Type=multipart/form-data   
    # ${RESPOSTA}     Put Request     newsession      onboarding/v1/leads/${lead_id}/selfie
    # ...     file=${file_jpg}
    # ...     headers=${HEADERS}
    # Log                             ${RESPOSTA.text}
    # Should Be Equal As Strings      ${RESPOSTA.status_code}    202

19_GET_{{url}}/v1/Leads/{{lead_id}}/documentExtraction
    [Arguments]  ${lead_id}    
    ${HEADERS}  Create Dictionary
    ...     accept=application/json
    ...     User-Agent=${guidDevice} 
    ${RESPOSTA}     Get Request     newsession      onboarding/v1/leads/${lead_id}/documentExtraction    
    ...     headers=${HEADERS}
    Log                             ${RESPOSTA.text}
    Should Be Equal As Strings      ${RESPOSTA.status_code}    200
    [Return]  ${RESPOSTA.json()}

20_PUT_{{url}}/v1/Leads/{{lead_id}}/additionalDocumentData
    [Arguments]  ${lead_id}  ${type}  ${issuer}  ${issuingCountry}  ${issuingState}  ${number}  ${originatingCountry}  ${expiryDate}  ${issueDate}      
    ${payload}   Create Dictionary    
    ...     type=${type}   
    ...     issuer=${issuer}  
    ...     issuingCountry=${issuingCountry}
    ...     issuingState=${issuingState}
    ...     number=${number}
    ...     originatingCountry=${originatingCountry} 
    ...     expiryDate=${expiryDate}
    ...     issueDate=${issueDate}
    
    ${json}  Convert Dic to Json  ${payload}
    ${HEADERS}  Create Dictionary
    ...     accept=application/json
    ...     User-Agent=${guidDevice}
    ...     Content-Type=application/json-patch+json   
    ${RESPOSTA}     Put Request     newsession      onboarding/v1/leads/${lead_id}/additionalDocumentData
    ...     data=${json}
    ...     headers=${HEADERS}
    Log                             ${RESPOSTA.text}    
    Should Be Equal As Strings      ${RESPOSTA.status_code}    202
    

21_PUT_{{url}}/v1/Leads/{{lead_id}}/PoliticalExposition
    [Arguments]  ${lead_id}
    ${json}=  catenate
    ...    {
    ...    "self": false,
    ...    "friendName": "string",
    ...    "familyMemberName": "string"
    ...    }
    ${HEADERS}  Create Dictionary
    ...     accept=application/json
    ...     User-Agent=${guidDevice}
    ...     Content-Type=application/json-patch+json   
    ${RESPOSTA}     Put Request     newsession      onboarding/v1/leads/${lead_id}/politicalExposition
    ...     data=${json}
    ...     headers=${HEADERS}
    Log                             ${RESPOSTA.text}
    Should Be Equal As Strings      ${RESPOSTA.status_code}    202

22_PUT_{{url}}/v1/Leads/{{lead_id}}/USPerson
    [Arguments]  ${lead_id}
    ${json}=  catenate
    ...    {
    ...    "self": false,
    ...    "number": "123456789"
    ...    }
    ${HEADERS}  Create Dictionary
    ...     accept=application/json
    ...     User-Agent=${guidDevice}
    ...     Content-Type=application/json-patch+json   
    ${RESPOSTA}     Put Request     newsession      onboarding/v1/leads/${lead_id}/uSPerson
    ...     data=${json}
    ...     headers=${HEADERS}
    Log                             ${RESPOSTA.text}
    Should Be Equal As Strings      ${RESPOSTA.status_code}    202

23_GET_{{url}}/v1/Leads/contract?types=AccountOpening
    # ${payload}   Create Dictionary    contract=string   type=string
    # ${json}  Convert Dic to Json  ${payload}
    ${HEADERS}  Create Dictionary
    ...     accept=application/json
    ...     User-Agent=${guidDevice}     
    ${RESPOSTA}     Get Request     newsession      onboarding/v1/leads/contract?types=AccountOpening   
    ...     headers=${HEADERS}
    Log                             ${RESPOSTA.text}
    Should Be Equal As Strings      ${RESPOSTA.status_code}    200
    [Return]  ${RESPOSTA.json()}

24_PUT_{{url}}/v1/Leads/{{lead_id}}/TermsAcceptance
    [Arguments]  ${lead_id}  ${contract}  ${type}
    ${payload}   Create Dictionary    contract=${contract}   type=${type}
    ${json}  Convert Dic to Json  ${payload}
    ${HEADERS}  Create Dictionary
    ...     accept=application/json
    ...     User-Agent=${guidDevice}
    ...     Content-Type=application/json-patch+json   
    ${RESPOSTA}     Put Request     newsession      onboarding/v1/leads/${lead_id}/termsAcceptance
    ...     data=${json}
    ...     headers=${HEADERS}
    Log                             ${RESPOSTA.text}
    Should Be Equal As Strings      ${RESPOSTA.status_code}    202