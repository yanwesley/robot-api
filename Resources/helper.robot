
*** Keywords ***
Nova Aba
    [arguments]     ${url}
    Execute Javascript          window.open()    
    Switch Window               locator=NEW
    Go To                       ${url}

Valida compliance
    [Arguments]  ${consulta}  ${itemDaLista}  ${numeroItem}  ${nomeValidar}    
    ${compliance}          Set Variable                           ${consulta[${itemDaLista}]['Compliance']}
    log                    ${compliance}
    ${bcCorreios}          Set Variable                           ${compliance[${numeroItem}]}
    ${validador}           Set Variable                           ${bcCorreios['Type']}
    Should Be True         "${validador}"=="${nomeValidar}"

Gera Token Cadastro
    Abrir conexao   https://api.qa.bancobari.com.br
    ${HEADERS}  Create Dictionary
    ...  Content-Type=application/x-www-form-urlencoded
    ${PARAMS}  Create Dictionary
    ...  grant_type=client_credentials
    ...  client_id=onboarding
    ...  client_secret=7sjJhLEyKIIz
    Set Test Variable  ${HEADERS}
    ${RESPOSTAREQUEST}     Post Request     newsession      auth/connect/token
    ...     data=${PARAMS}
    ...     headers=${HEADERS}
    Log                          ${RESPOSTAREQUEST.text}
    ${json_value}  Set Variable  ${RESPOSTAREQUEST.json()}
    ${token}  Catenate  Bearer  ${json_value['access_token']}
    Set Test Variable  ${token}
    [Return]        ${token}

Gera Token Customer
    Abrir conexao   https://api.qa.bancobari.com.br
    ${HEADERS}  Create Dictionary
    ...  Content-Type=application/x-www-form-urlencoded
    ${PARAMS}  Create Dictionary
    ...  grant_type=client_credentials
    ...  client_id=customer
    ...  client_secret=cu5Rpu32i7S
    Set Test Variable  ${HEADERS}
    ${RESPOSTAREQUEST}     Post Request     newsession      auth/connect/token
    ...     data=${PARAMS}
    ...     headers=${HEADERS}
    Log                          ${RESPOSTAREQUEST.text}
    ${json_value}  Set Variable  ${RESPOSTAREQUEST.json()}
    ${token}  Catenate  Bearer  ${json_value['access_token']}
    Set Test Variable  ${token}
    [Return]        ${token}

Convert Dic to Json
    [Arguments]     ${dic}
    log  ${dic}
    ${json}=    evaluate    json.dumps(${dic})    json
    log  ${json}
    ${json_json}  To Json  ${json}  pretty_print=True
    log  ${json_json}
    [Return]        ${json_json}

Gera Token
    [Arguments]     ${cpf}  ${senha} 
    #Abrir conexao   https://api.qa.bancobari.com.br/
    ${PARAMS}       Create Dictionary       username=${cpf}  password=${senha}  grant_type=password  client_id=mobile  client_secret=mH8341Kop  content-Type=text/plain
    ${HEADERS}      Create Dictionary       content-type=application/x-www-form-urlencoded
    Set Test Variable  ${HEADERS}
    ${RESPOSTAREQUEST}     Post Request     newsession      auth/connect/token
    ...     data=${PARAMS}
    ...     headers=${HEADERS}
    Log                          ${RESPOSTAREQUEST.text}
    ${json_value}  Set Variable  ${RESPOSTAREQUEST.json()}
    ${token}  Catenate  Bearer  ${json_value['access_token']}
    Set Test Variable  ${token}
    [Return]        ${token}

Gera Token Aproved
    Abrir conexao   https://api.qa.bancobari.com.br
    ${HEADERS}  Create Dictionary
    ...  Content-Type=application/x-www-form-urlencoded
    ${PARAMS}  Create Dictionary
    ...  grant_type=client_credentials
    ...  client_id=onboard_fraud
    ...  client_secret=wz1w0J8z6UL181w8
    Set Test Variable  ${HEADERS}
    ${RESPOSTAREQUEST}     Post Request     newsession      auth/connect/token
    ...     data=${PARAMS}
    ...     headers=${HEADERS}
    Log                          ${RESPOSTAREQUEST.text}
    ${json_value}  Set Variable  ${RESPOSTAREQUEST.json()}
    ${token}  Catenate  Bearer  ${json_value['access_token']}
    Set Test Variable  ${token}
    [Return]        ${token}

Gera Token PayRoll
    Abrir conexao   https://api.qa.bancobari.com.br
    ${HEADERS}  Create Dictionary
    ...  Content-Type=application/x-www-form-urlencoded
    ${PARAMS}  Create Dictionary
    ...  grant_type=client_credentials
    ...  client_id=payrollloans
    ...  client_secret=d7uiR98lomW
    Set Test Variable  ${HEADERS}
    ${RESPOSTAREQUEST}     Post Request     newsession      auth/connect/token
    ...     data=${PARAMS}
    ...     headers=${HEADERS}
    Log                          ${RESPOSTAREQUEST.text}
    ${json_value}  Set Variable  ${RESPOSTAREQUEST.json()}
    ${token}  Catenate  Bearer  ${json_value['access_token']}
    Set Test Variable  ${token}
    [Return]        ${token}

Gera Token Onboarding
    Abrir conexao   https://api.qa.bancobari.com.br
    ${HEADERS}  Create Dictionary
    ...  Content-Type=application/x-www-form-urlencoded
    ${PARAMS}  Create Dictionary
    ...  grant_type=client_credentials
    ...  client_id=onboarding
    ...  client_secret=7sjJhLEyKIIz
    Set Test Variable  ${HEADERS}
    ${RESPOSTAREQUEST}     Post Request     newsession      auth/connect/token
    ...     data=${PARAMS}
    ...     headers=${HEADERS}
    Log                          ${RESPOSTAREQUEST.text}
    ${json_value}  Set Variable  ${RESPOSTAREQUEST.json()}
    ${token}  Catenate  Bearer  ${json_value['access_token']}
    Set Test Variable  ${token}
    [Return]        ${token}

Gera Token Kapmug
    Abrir conexao   https://api.qa.bancobari.com.br
    ${HEADERS}  Create Dictionary
    ...  Content-Type=application/x-www-form-urlencoded
    ${PARAMS}  Create Dictionary
    ...  grant_type=client_credentials
    ...  client_id=kapmug
    ...  client_secret=hP50CaFH1bYLZg9Us6KD0IcGAvJzpfW2
    Set Test Variable  ${HEADERS}
    ${RESPOSTAREQUEST}     Post Request     newsession      auth/connect/token
    ...     data=${PARAMS}
    ...     headers=${HEADERS}
    Log                          ${RESPOSTAREQUEST.text}
    ${json_value}  Set Variable  ${RESPOSTAREQUEST.json()}
    ${token}  Catenate  Bearer  ${json_value['access_token']}
    Set Test Variable  ${token}
    [Return]        ${token}

Gera Token Geral
    Abrir conexao   https://api.qa.bancobari.com.br
    ${HEADERS}  Create Dictionary
    ...  Content-Type=application/x-www-form-urlencoded
    ${PARAMS}  Create Dictionary
    ...  grant_type=client_credentials
    ...  client_id=mobile
    ...  client_secret=mH8341Kop
    Set Test Variable  ${HEADERS}
    ${RESPOSTAREQUEST}     Post Request     newsession      auth/connect/token
    ...     data=${PARAMS}
    ...     headers=${HEADERS}
    Log                          ${RESPOSTAREQUEST.text}
    ${json_value}  Set Variable  ${RESPOSTAREQUEST.json()}
    ${token}  Catenate  Bearer  ${json_value['access_token']}
    Set Test Variable  ${token}
    [Return]        ${token}

Email Verification
    [Arguments]  ${sender}  ${content}
    Open Mailbox    host=imap.gmail.com    user=yan.lima@baritecnologia.com.br  password=bari@2020  port=993 
    ${LATEST}    Wait For Email    sender=${sender}    timeout=12000
    ${body}    Get Email Body  ${LATEST}
    Should Contain    ${body}    ${content}
    Close Mailbox
    [Return]  ${body}

Discard Email
    [Arguments]  ${sender}  ${content}
    Open Mailbox    host=imap.gmail.com    user=yan.lima@baritecnologia.com.br  password=bari@2020  port=993 
    ${LATEST}    Wait For Email    sender=${sender}    timeout=12000
    Delete Email  ${LATEST}

New Token DynamoDB
    Abrir Navegador  https://bancobari.awsapps.com/start#/
    Wait Until Element Is Visible         id=mainFrame                120 second
    Input Text                            id=wdc_username             yan.lima
    Input Text                            id=wdc_password             463166Bari!    
    Click Element                         id=wdc_login_button
    ${bodyreturn}  Email Verification                    no-reply@login.awsapps.com        Verification Code:
    Log  ${bodyreturn}
    ${teste}  Fetch From Right        ${bodyreturn}     40px;">
    ${teste1}  Fetch From Left        ${teste}          </div><div
    log  ${teste1}
    Wait Until Element Is Visible         xpath=//*[@id="wdc_mfacode"]         60 second 
    Input Text                            xpath=//*[@id="wdc_mfacode"]         ${teste1}
    Click Element                         xpath=//*[@id="wdc_login_button"]/span/span/button
    Wait Until Element Is Visible         xpath=//*[@id="app-03e8643328913682"]        20 second
    Discard Email  no-reply@login.awsapps.com  Verification Code:
    Click Element                         xpath=//*[@id="app-03e8643328913682"]
    Click Element                         xpath=//*[@id="ins-07f2eb6bec3f867f"]/div/div
    Wait Until Element Is Visible         xpath=//*[@id="temp-credentials-button"]      20 second
    Click Element                         xpath=//*[@id="temp-credentials-button"]
    Wait Until Element Is Visible         xpath=//div[@class='dialog-body']
    Wait Until Element Is Visible         xpath=//div[4][@class='ng-tns-c18-10']      60 second
    Wait Until Element Is Visible  xpath=//*[@id="cli-cred-file-code"]/div[2]  60 second
    ${resultextract2}  Get Text            xpath=//*[@id="cli-cred-file-code"]/div[2]
    Wait Until Element Is Visible  xpath=//*[@id="cli-cred-file-code"]/div[3]  60 second
    ${resultextract4}  Get Text            xpath=//*[@id="cli-cred-file-code"]/div[3]
    Wait Until Element Is Visible  xpath=//*[@id="cli-cred-file-code"]/div[4]  60 second
    ${resultextract6}  Get Text            xpath=//*[@id="cli-cred-file-code"]/div[4]
   
    Log  ${resultextract2}
    ${stripped2}=	Strip String  ${resultextract2}
    Log  ${resultextract4}
    ${stripped4}=	Strip String  ${resultextract4}
    Log  ${resultextract6}
    ${stripped6}=	Strip String  ${resultextract6}
    ${defaultText}  Set Variable  [default]
    ${string}  catenate  ${defaultText}${\n}${stripped2}${\n}${stripped4}${\n}${stripped6}${\n}
    Remove File  C:\\Users\\yanl\\.aws\\credentials 
    Append To File  C:\\Users\\yanl\\.aws\\credentials  ${string}
    Close Browser

New Token DynamoDB HML
    Abrir Navegador  https://bancobari.awsapps.com/start#/
    Wait Until Element Is Visible         id=mainFrame                120 second
    Input Text                            id=wdc_username             yan.lima
    Input Text                            id=wdc_password             463166Bari!    
    Click Element                         id=wdc_login_button
    ${bodyreturn}  Email Verification                    no-reply@login.awsapps.com        Verification Code:
    Log  ${bodyreturn}
    ${teste}  Fetch From Right        ${bodyreturn}     40px;">
    ${teste1}  Fetch From Left        ${teste}          </div><div
    log  ${teste1}
    Wait Until Element Is Visible         xpath=//*[@id="wdc_mfacode"]         60 second 
    Input Text                            xpath=//*[@id="wdc_mfacode"]         ${teste1}
    Click Element                         xpath=//*[@id="wdc_login_button"]/span/span/button
    Wait Until Element Is Visible         xpath=//*[@id="app-03e8643328913682"]        20 second
    Discard Email  no-reply@login.awsapps.com  Verification Code:
    Click Element                         xpath=//*[@id="app-03e8643328913682"]
    
    Click Element                         xpath=//*[@id="ins-7efdd7de91d8832d"]/div/div
    Wait Until Element Is Visible         xpath=//*[@id="temp-credentials-button"]      20 second
    Click Element                         xpath=//*[@id="temp-credentials-button"]
    Wait Until Element Is Visible         xpath=//div[@class='dialog-body']
    Wait Until Element Is Visible         xpath=//div[4][@class='ng-tns-c18-10']      60 second
    Wait Until Element Is Visible  xpath=//*[@id="cli-cred-file-code"]/div[2]  60 second
    ${resultextract2}  Get Text            xpath=//*[@id="cli-cred-file-code"]/div[2]
    Wait Until Element Is Visible  xpath=//*[@id="cli-cred-file-code"]/div[3]  60 second
    ${resultextract4}  Get Text            xpath=//*[@id="cli-cred-file-code"]/div[3]
    Wait Until Element Is Visible  xpath=//*[@id="cli-cred-file-code"]/div[4]  60 second
    ${resultextract6}  Get Text            xpath=//*[@id="cli-cred-file-code"]/div[4]
   
    Log  ${resultextract2}
    ${stripped2}=	Strip String  ${resultextract2}
    Log  ${resultextract4}
    ${stripped4}=	Strip String  ${resultextract4}
    Log  ${resultextract6}
    ${stripped6}=	Strip String  ${resultextract6}
    ${defaultText}  Set Variable  [default]
    ${string}  catenate  ${defaultText}${\n}${stripped2}${\n}${stripped4}${\n}${stripped6}${\n}
    Remove File  C:\\Users\\yanl\\.aws\\credentials 
    Append To File  C:\\Users\\yanl\\.aws\\credentials  ${string}
    Close Browser

New Token DynamoDB DEV
    Abrir Navegador  https://bancobari.awsapps.com/start#/
    Wait Until Element Is Visible         id=mainFrame                120 second
    Input Text                            id=wdc_username             yan.lima
    Input Text                            id=wdc_password             463166Bari!    
    Click Element                         id=wdc_login_button
    ${bodyreturn}  Email Verification                    no-reply@login.awsapps.com        Verification Code:
    Log  ${bodyreturn}
    ${teste}  Fetch From Right        ${bodyreturn}     40px;">
    ${teste1}  Fetch From Left        ${teste}          </div><div
    log  ${teste1}
    Wait Until Element Is Visible         xpath=//*[@id="wdc_mfacode"]         60 second 
    Input Text                            xpath=//*[@id="wdc_mfacode"]         ${teste1}
    Click Element                         xpath=//*[@id="wdc_login_button"]/span/span/button
    Wait Until Element Is Visible         xpath=//*[@id="app-03e8643328913682"]        20 second
    Discard Email  no-reply@login.awsapps.com  Verification Code:
    Click Element                         xpath=//*[@id="app-03e8643328913682"]
    Click Element                         xpath=//*[@id="ins-ec62bf013102a2a1"]/div/div
    Wait Until Element Is Visible         xpath=//*[@id="temp-credentials-button"]      20 second
    Click Element                         xpath=//*[@id="temp-credentials-button"]
    Wait Until Element Is Visible         xpath=//div[@class='dialog-body']
    Wait Until Element Is Visible         xpath=//div[4][@class='ng-tns-c18-10']      60 second
    Wait Until Element Is Visible  xpath=//*[@id="cli-cred-file-code"]/div[2]  60 second
    ${resultextract2}  Get Text            xpath=//*[@id="cli-cred-file-code"]/div[2]
    Wait Until Element Is Visible  xpath=//*[@id="cli-cred-file-code"]/div[3]  60 second
    ${resultextract4}  Get Text            xpath=//*[@id="cli-cred-file-code"]/div[3]
    Wait Until Element Is Visible  xpath=//*[@id="cli-cred-file-code"]/div[4]  60 second
    ${resultextract6}  Get Text            xpath=//*[@id="cli-cred-file-code"]/div[4]
   
    Log  ${resultextract2}
    ${stripped2}=	Strip String  ${resultextract2}
    Log  ${resultextract4}
    ${stripped4}=	Strip String  ${resultextract4}
    Log  ${resultextract6}
    ${stripped6}=	Strip String  ${resultextract6}
    ${defaultText}  Set Variable  [default]
    ${string}  catenate  ${defaultText}${\n}${stripped2}${\n}${stripped4}${\n}${stripped6}${\n}
    Remove File  C:\\Users\\yanl\\.aws\\credentials 
    Append To File  C:\\Users\\yanl\\.aws\\credentials  ${string}
    Close Browser
    