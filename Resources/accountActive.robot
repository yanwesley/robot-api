***Keywords***

PUT_subjects/{subjectId}
    [Arguments]  ${token}  ${subjectId}
    ${payload}   Create Dictionary    status=approved
    ${json}  Convert Dic to Json  ${payload}  
    ${HEADERS}  Create Dictionary
    ...     accept=application/json
    ...     Content-Type=application/json 
    ...     Authorization=${token}   
    ${RESPOSTA}     Put Request     newsession      onboard_fraud/v1/subjects/${subjectId}
    ...     headers=${HEADERS}
    ...     data=${json}
    Log                             ${RESPOSTA.text}
    Should Be Equal As Strings      ${RESPOSTA.status_code}    200
01_GET_customer/v1/customers/{{customerId}}/checkUserInvited
    [Arguments]  ${CPF}    
    ${HEADERS}  Create Dictionary
    ...     accept=application/json
    ...     User-Agent=${guidDevice}     
    ${RESPOSTA}     Get Request     newsession      customer/v1/customers/${CPF}/checkUserInvited   
    ...     headers=${HEADERS}
    Log                             ${RESPOSTA.text}
    Should Be Equal As Strings      ${RESPOSTA.status_code}    200
    [Return]  ${RESPOSTA.json()}

02_POST_customer/v1/customers/{{customerId}}/accessCodeValidation
    [Arguments]  ${CPF}  ${code}
    ${payload}   Create Dictionary    code=${code}
    ${json}  Convert Dic to Json  ${payload}  
    ${HEADERS}  Create Dictionary
    ...     accept=application/json
    ...     content-type=application/json-patch+json
    ...     User-Agent=${guidDevice}     
    ${RESPOSTA}     Post Request     newsession      customer/v1/customers/${CPF}/accessCodeValidation  
    ...     headers=${HEADERS}
    ...     data=${json}
    Log                             ${RESPOSTA.text}
    Should Be Equal As Strings      ${RESPOSTA.status_code}    200

03_POST_auth/v1/accounts/{{customerId}}/activate
    [Arguments]  ${CPF}  ${accountId}  ${activationToken}
    ${payload}   Create Dictionary    accountId=${accountId}  activationToken=${activationToken}
    ${json}  Convert Dic to Json  ${payload}  
    ${HEADERS}  Create Dictionary
    ...     accept=application/json
    ...     content-type=application/json-patch+json
    ...     User-Agent=${guidDevice}     
    ${RESPOSTA}     Post Request     newsession      auth/v1/accounts/${CPF}/activate 
    ...     headers=${HEADERS}
    ...     data=${json}
    Log                             ${RESPOSTA.text}
    Should Be Equal As Strings      ${RESPOSTA.status_code}    200

04_POST_auth/v1/accounts/{{customerId}}/resetPassword
    [Arguments]  ${CPF}  ${accountId}  ${resetToken}  ${newPassword}
    ${payload}   Create Dictionary    accountId=${accountId}  resetToken=${resetToken}  newPassword=${newPassword}
    ${json}  Convert Dic to Json  ${payload}  
    ${HEADERS}  Create Dictionary
    ...     accept=application/json
    ...     content-type=application/json-patch+json
    ...     User-Agent=${guidDevice}     
    ${RESPOSTA}     Post Request     newsession      auth/v1/accounts/${CPF}/resetPassword  
    ...     headers=${HEADERS}
    ...     data=${json}
    Log                             ${RESPOSTA.text}
    Should Be Equal As Strings      ${RESPOSTA.status_code}    200

05_POST_auth/connect/token
    [Arguments]  ${username}  ${password}  ${client_id}  ${client_secret}  ${grant_type}
    ${payload}   Create Dictionary    
    ...  username=${username}
    ...  password=${password}
    ...  client_id=${client_id}
    ...  client_secret=${client_secret}
    ...  grant_type=${grant_type}
    #${json}  Convert Dic to Json  ${payload}
    ${HEADERS}  Create Dictionary
    #...     accept=application/json
    ...     content-type=application/x-www-form-urlencoded
    #...     User-Agent=${guidDevice}
    ${RESPOSTA}     Post Request     newsession      auth/connect/token
    ...     headers=${HEADERS}
    ...     data=${payload}
    Log                             ${RESPOSTA.text}
    Should Be Equal As Strings      ${RESPOSTA.status_code}    200
    ${json_value}  Set Variable  ${RESPOSTA.json()}
    ${token}  Catenate  Bearer  ${json_value['access_token']}
    Set Test Variable  ${token}
    [Return]        ${token}

06_GET_featureflag/v1/customerScope
    #[Arguments]  ${CPF}
    # ${payload}   Create Dictionary    contract=string   type=string
    # ${json}  Convert Dic to Json  ${payload}  
    ${HEADERS}  Create Dictionary
    ...     accept=application/json
    ...     content-type=application/json-patch+json
    ...     User-Agent=${guidDevice}
    ...     Authorization=${token}        
    ${RESPOSTA}     Get Request     newsession      featureflag/v1/customerScope  
    ...     headers=${HEADERS}
    Log                             ${RESPOSTA.text}
    Should Be Equal As Strings      ${RESPOSTA.status_code}    200
    [Return]  ${RESPOSTA.json()}

07_GET_customer/v1/terms/{{customerId}}/termsofuse
    [Arguments]  ${CPF}
    # ${payload}   Create Dictionary    contract=string   type=string
    # ${json}  Convert Dic to Json  ${payload}  
    ${HEADERS}  Create Dictionary
    ...     accept=application/json
    #...     content-type=application/json-patch+json
    ...     User-Agent=${guidDevice}  
    ...     Authorization=${token}   
    ${RESPOSTA}     Get Request     newsession      customer/v1/terms/${CPF}/termsofuse   
    ...     headers=${HEADERS}
    Log                             ${RESPOSTA.text}
    Should Be Equal As Strings      ${RESPOSTA.status_code}    200
    [Return]  ${RESPOSTA.json()}

08_PUT_communication/v1/endpoints/{{customerId}}
    [Arguments]  ${CPF}  ${deviceId}  ${address}  ${token}
    ${json_string}=    catenate
    ...     {
	...     "channelType": "GCM",
	...     "address": "${address}",
	...     "demographic": {
	...     	"make": "samsung",
	...     	"model": "SM-A730F",
	...     	"modelVersion": "28",
	...     	"timezone": "America/Bahia",
	...     	"locale": "pt_BR",
	...     	"appVersion": "1 (1.0)",
	...     	"platform": "Android",
	...     	"platformVersion": "28/9/2020-01-01"
	...     }
    ...     }
    ${HEADERS}  Create Dictionary
    ...     accept=application/json
    ...     User-Agent=${guidDevice}  
    ...     Authorization=${token}
    ...     Content-Type=application/json-patch+json    
    ${RESPOSTA}     Put Request     newsession      communication/v1/endpoints/${CPF}
    ...     headers=${HEADERS}
    ...     data=${json_string}
    Log                             ${RESPOSTA.text}
    Should Be Equal As Strings      ${RESPOSTA.status_code}    202

09_GET_customer/v1/customers/{{customerId}}/earlyAdopter
    [Arguments]  ${CPF}
    # ${payload}   Create Dictionary    contract=string   type=string
    # ${json}  Convert Dic to Json  ${payload}  
    ${HEADERS}  Create Dictionary
    ...     accept=application/json
    ...     User-Agent=${guidDevice}  
    ...     Authorization=${token}   
    ${RESPOSTA}     Get Request     newsession      customer/v1/customers/${CPF}/earlyAdopter   
    ...     headers=${HEADERS}
    Log                             ${RESPOSTA.text}
    Should Be Equal As Strings      ${RESPOSTA.status_code}    200
    [Return]  ${RESPOSTA.json()}

10_POST_auth/v1/accounts/{{customerId}}/createTransactionPassword
    [Arguments]  ${CPF}  ${token}  ${newTransactionPassword}
    ${payload}   Create Dictionary    newTransactionPassword=${newTransactionPassword}
    ${json}  Convert Dic to Json  ${payload}  
    ${HEADERS}  Create Dictionary
    ...     accept=application/json   #application/javascript??
    ...     Content-Type=application/json-patch+json
    ...     User-Agent=${guidDevice}  
    ...     Authorization=${token}   
    ${RESPOSTA}     Post Request     newsession      auth/v1/accounts/${CPF}/createTransactionPassword 
    ...     headers=${HEADERS}
    ...     data=${json}
    Log                             ${RESPOSTA.text}
    Should Be Equal As Strings      ${RESPOSTA.status_code}    200

11_GET_account/v1/accounts/{{customerId}}
    [Arguments]  ${customerId}  ${token} 
    # ${payload}   Create Dictionary    contract=string   type=string
    # ${json}  Convert Dic to Json  ${payload}  
    ${HEADERS}  Create Dictionary
    ...     accept=application/json
    #...     content-type=application/json-patch+json
    ...     User-Agent=${guidDevice}  
    ...     Authorization=${token}   
    ${RESPOSTA}     Get Request     newsession      account/v1/accounts/${customerId}
    ...     headers=${HEADERS}
    Log                             ${RESPOSTA.text}
    Should Be Equal As Strings      ${RESPOSTA.status_code}    200
    [Return]  ${RESPOSTA.json()}

12_GET_account/v1/accounts/{{customerId}}/financialGoals
    [Arguments]  ${customerId}  ${token}      
    ${HEADERS}  Create Dictionary
    ...     accept=application/json
    #...     content-type=application/json-patch+json
    ...     User-Agent=${guidDevice}  
    ...     Authorization=${token}   
    ${RESPOSTA}     Get Request     newsession      account/v1/accounts/${customerId}/financialGoals
    ...     headers=${HEADERS}
    Log                             ${RESPOSTA.text}
    Should Be Equal As Strings      ${RESPOSTA.status_code}    200
    [Return]  ${RESPOSTA.json()}