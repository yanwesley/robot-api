*** Settings ***
Resource	${EXECDIR}/Resources/main.resource

*** Test Cases ***

valida ativação
    Abrir conexao  https://api.qa.bancobari.com.br

    #aprovação na mesa
    ${email}  Set Variable  yan_wesley@hotmail.com
    ${Leads}  Consulta Dynamo  Barigui.Services.Onboarding_Leads  Email  ${email}
    Log  ${Leads}
    ${msgA}  Run Keyword And Ignore Error  Log  ${Leads[0][0]['Id']}
    ${msgB}  Run Keyword And Ignore Error  Log  ${Leads[0]['Id']}
    Run Keyword If  '${msgA[0]}'=='PASS'  setVariableLeadId  ${Leads[0][0]['Id']} 
    Run Keyword If  '${msgB[0]}'=='PASS'  setVariableLeadId  ${Leads[0]['Id']}
    ${valida}  Set Variable    false
    Set Test Variable    ${valida}
    :FOR    ${COUNT}    IN RANGE    0    300
    \    ${Subjects}  Consulta Dynamo  Barigui.Services.OnboardFraud_Subjects  externalId  ${leadsId}
    \    ${msg}  Run Keyword And Ignore Error  log  ${Subjects[0]['subjectId']}
    \    Run Keyword If   "${msg[0]}" == "PASS"         validaStatusMesa     ${Subjects[0]['status']}  
    \    Exit For Loop IF  "${valida}" == "True"     
    \    Run Keyword If    ${COUNT} == 300    Log    Foram feitas 300 tentativas, registro não encontrado!     
    ${Token}  Gera Token Aproved
    log  ${Token}
    PUT_subjects/{subjectId}  ${Token}  ${Subjects[0]['subjectId']}

    # primeira etapa
    ${guidDevice}  Gera Guid
    log  ${guidDevice}
    Set Test Variable  ${guidDevice}
    ${CPF}  Set Variable  02183488190
    Set Test Variable  ${CPF} 
   :FOR    ${COUNT}    IN RANGE    0    300
    \    ${ActivationCode}  Consulta Dynamo  Barigui.Services.Customer_Customers  Id  ${CPF}
    \    ${msg}  Run Keyword And Ignore Error  log  ${ActivationCode[0]['ActivationCode']}
    \    Exit For Loop IF    "${msg[0]}" == "PASS"    
    \    Run Keyword If    ${COUNT} == 300    Log    Foram feitas 300 tentativas, registro não encontrado! 
    01_GET_customer/v1/customers/{{customerId}}/checkUserInvited  ${CPF}
    02_POST_customer/v1/customers/{{customerId}}/accessCodeValidation  ${CPF}  ${ActivationCode[0]['ActivationCode']}
    
    
    # segunda etapa        
    ${accountId}  Set Variable  ${EMPTY} 
    Sleep    20s    reason=None
    ${activationToken}  userInputSMS   
    03_POST_auth/v1/accounts/{{customerId}}/activate  ${CPF}  ${accountId}  ${activationToken}
    ${password}  Set Variable  123Abc
    04_POST_auth/v1/accounts/{{customerId}}/resetPassword  ${CPF}  ${accountId}  ${activationToken}  ${password}
    ${tokencad}  Gera Token Cadastro
    log  ${tokencad}   
    :FOR    ${COUNT}    IN RANGE    0    300
    \    ${msg}  Run Keyword And Ignore Error  generationToken
    \    Exit For Loop IF    "${msg[0]}" == "PASS"
    \    Run Keyword If    ${COUNT} == 300    Log    Foram feitas 300 tentativas, Erro na geração do token!
    log  ${token}
    ${resposta}  06_GET_featureflag/v1/customerScope
    ${termos}  07_GET_customer/v1/terms/{{customerId}}/termsofuse  ${CPF}
    ${deviceId}  Gera Guid
    ${address}  Set Variable  dzs1tEuwmac:APA91bHvGxNYjZatM72rkpKTTxYCXUVJJFydkLG91idRpudrbEuBMOna3Mqru7NcW4GIOZeyWgfl54hoPVnjWH1tpOf5MNLFljBguZ_yA_rohGCBUeh8c42jTBpaWrT4yy3ZwJttOVTZ
    08_PUT_communication/v1/endpoints/{{customerId}}  ${CPF}  ${deviceId}  ${address}  ${token}    
    09_GET_customer/v1/customers/{{customerId}}/earlyAdopter  ${CPF}
    ${newTransactionPassword}  Set Variable  1470
    10_POST_auth/v1/accounts/{{customerId}}/createTransactionPassword  ${CPF}  ${token}  ${newTransactionPassword}
    ${Id}  Consulta Dynamo  Barigui.Services.Account_CheckingAccount  CustomerId  ${CPF}
    log  ${Id}
    log  ${Id[0]['Id']} 
    11_GET_account/v1/accounts/{{customerId}}  ${Id[0]['Id']}  ${token}
    12_GET_account/v1/accounts/{{customerId}}/financialGoals  ${Id[0]['Id']}  ${token}
    ${hash256}  Encrypt String  ${CPF}
    log  ${hash256} 
    Update Customerscopes  ${hash256}
    
*** Keywords ***

setVariableLeadId
    [Arguments]  ${index}
    ${leadsId}  Set Variable  ${index}
    Set Test Variable  ${leadsId}

generationToken
    ${username}  Set Variable  02183488190
    ${password}  Set Variable  123Abc
    ${client_id}  Set Variable  mobile
    ${client_secret}  Set Variable  mH8341Kop
    ${grant_type}  Set Variable  password
    ${guidDevice}  Set Variable  8dcd23d7-4a9c-472c-9707-32f2039e62c4
    Set Test Variable  ${guidDevice}
    ${token}  05_POST_auth/connect/token  ${username}  ${password}  ${client_id}  ${client_secret}  ${grant_type}
    Set Test Variable  ${token}

validaStatusMesa
    [Arguments]  ${status}  
    Log  ${status}
    ${valida}    Evaluate   "${status}"=="pending"
    Set Test Variable    ${valida}
    