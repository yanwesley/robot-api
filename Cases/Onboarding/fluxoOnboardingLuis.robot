*** Settings ***
Resource	${EXECDIR}/Resources/main.resource
Suite Setup   Abrir conexao  https://api.qa.bancobari.com.br

*** Test Cases ***

Testes Costumer
    
    #gera guid
    ${guidLead}  Gera Guid
    #${guidLead}  Set Variable  e0a9732f-3771-429d-bea1-776ea2bd7e12
    Set Test Variable  ${guidLead}
    #gera guid   
    ${guidDevice}  Gera Guid
    #${guidDevice}  Set Variable  8d70c1b3-6126-4214-b1c2-aceab1275fd0
    Set Test Variable  ${guidDevice}
    # gerando cpf
    # ${CPFnum}  FakerLibrary.cpf
    # ${CPF}  Remove String  ${CPFnum}  .  -
    
    # #gerando nome
    # ${name}  FakerLibrary.name
    # Set Test Variable  ${name}
    # ${contains}=  Evaluate   "." in """${name}"""    
    # Run Keyword If  ${contains}  Split Name  ${name}
    # ...  ELSE  No Split Name
    # #gerando e-mail
    # ${email}  FakerLibrary.email

    # #gerando telefone
    # ${phone}  FakerLibrary.Phone Number
    # ${numbers}=    Generate Random String    length=7    chars=[NUMBERS]
    # ${telefone}  catenate  SEPARATOR=  4199  ${numbers}

    # #gerando data de nascimento
    # ${dtnasc}  FakerLibrary.Date Of Birth  minimum_age=18  maximum_age=70
    
    ${CPF}  Set Variable   10127955941
    ${NOME}  Set Variable  Tobias Murano  #Luis Eduardo Gritz
    ${dtnasc}  Set Variable  2000-02-07
    ${email}  Set Variable  yan_wesley@hotmail.com
    ${telefone}  Set Variable  41991645776
    ${gender}  Set Variable  MALE
    ${motherName}  Set Variable  Marcia Regina Gritz
    ${State}  Set Variable  Paraná
    ${City}  Set Variable  Curitiba
    ${maritalStatus}  Set Variable  MARRIED 
    ${spouseName}  Set Variable  Xiaomi Tamura
    ${postalCode}  Set Variable  80630065
    ${addressLine1}  Set Variable  Rua daisy Luci Berno
    ${number}  Set Variable  2586
    ${neighborhood}  Set Variable  Guaíra
    ${city}  Set Variable  Curitiba
    ${state}  Set Variable  PR


    01_GET_/v1/Leads/contract?types=InformationPrivacy,SCRAcceptance
    Sleep    5s
    02_PUT_{{url}}/v1/Leads/{{lead_id}}  ${guidLead}  ${NOME}  ${CPF}
    Sleep    5s
    03_PUT_{{url}}/v1/Leads/{{lead_id}}/BirthDate  ${guidLead}  ${dtnasc}
    Sleep    5s
    04_PUT_{{url}}/v1/Leads/{{lead_id}}/geolocation  ${guidLead}
    Sleep    5s
    05_PUT_{{url}}/v1/Leads/{{lead_id}}/Email  ${guidLead}  ${email}
    Sleep    5s
    07_PUT_{{url}}/v1/Leads/{{lead_id}}/mobilePhoneNumber  ${guidLead}  ${telefone}
    Sleep    5s
    update_leads  ${guidLead}
    # ${token}  Gera Token SF
    # Integracao SF  ${CPF}  ${NOME}  ${email}  ${telefone}  ${dtnasc}  ${token}    
    # Consulta SF  ${token}  ${CPF}
    Abrir conexao   https://api.qa.bancobari.com.br    
    09_PUT_{{url}}/v1/Leads/{{lead_id}}/personalInfo  ${guidLead}  ${gender}  ${motherName}  ${State}  ${City}  ${maritalStatus}  ${spouseName}
    Sleep    5s
    10_PUT_{{url}}/v1/Leads/{{lead_id}}/homeAddress  ${guidLead}  ${postalCode}  ${addressLine1}  ${number}  ${neighborhood}  ${city}  ${state}
    Sleep    5s
    11_PUT_{{url}}/v1/Leads/{{lead_id}}/commercialAddress  ${guidLead}
    Sleep    5s
    12_GET_{{url}}/v1/Leads/professions
    Sleep    5s
    13_PUT_{{url}}/v1/Leads/{{lead_id}}/profession  ${guidLead}    
    Sleep    5s
    14_PUT_{{url}}/v1/Leads/{{lead_id}}/income  ${guidLead}
    Sleep    5s
    15_GET_{{url}}/v1/Leads/assetsrange     
    Sleep    5s
    16_PUT_{{url}}/v1/Leads/{{lead_id}}/assets  ${guidLead}
    Sleep    5s
    17_PUT_{{url}}/v1/Leads/{{lead_id}}/documentPicture  ${guidLead}
    Sleep    5s
    18_PUT_{{url}}/v1/Leads/{{lead_id}}/selfie  ${guidLead}
    Sleep    5s
    ${CNHDados}  19_GET_{{url}}/v1/Leads/{{lead_id}}/documentExtraction  ${guidLead}
    ${type}  Set Variable  CNH
    ${issuer}  Set Variable  Órgão de Transito
    ${issuingCountry}  Set Variable  BRA 
    ${issuingState}  Set Variable  PR
    ${number}  Set Variable  04043022043
    ${originatingCountry}  Set Variable  BRA 
    ${expiryDate}  Set Variable  2021-03-03T12:58:21.160Z  
    ${date}  Set Variable  2007-02-23
    ${issueDate}  catenate  SEPARATOR=  ${date}  T12:58:21.160Z
    20_PUT_{{url}}/v1/Leads/{{lead_id}}/additionalDocumentData  ${guidLead}  ${type}  ${issuer}  ${issuingCountry}  ${issuingState}  ${number}  ${originatingCountry}  ${expiryDate}  ${issueDate}  
    Sleep    5s
    21_PUT_{{url}}/v1/Leads/{{lead_id}}/PoliticalExposition  ${guidLead}
    Sleep    5s
    22_PUT_{{url}}/v1/Leads/{{lead_id}}/USPerson  ${guidLead}
    Sleep    5s
    ${AccountOpening}  23_GET_{{url}}/v1/Leads/contract?types=AccountOpening
    24_PUT_{{url}}/v1/Leads/{{lead_id}}/TermsAcceptance  ${guidLead}  ${AccountOpening[0]['url']}  ${AccountOpening[0]['type']}
  
***Keywords***

Split Name
    [Arguments]  ${name}
    ${nameList}  Split String  ${name}  .
    ${NOME}  Set Variable  ${nameList[1]}
    Set Test Variable  ${NOME}

No Split Name
    ${NOME}  Set Variable  ${name}
    Set Test Variable  ${NOME}

Gera Token SF
    Abrir conexao   https://test.salesforce.com
    ${HEADERS}  Create Dictionary
    ...  Content-Type=application/x-www-form-urlencoded
    ${PARAMS}  Create Dictionary
    ...  grant_type=password
    ...  client_id=3MVG9PE4xB9wtoY.8zAdxB8mUc4yr9iK.d5uto1BHKkv_c3msG8UZPaAe0A.VtqyzpCS28BEeLTnGSGxEizky
    ...  client_secret=B070BFCF61A1A7D40F0D078238187B282D25AC71A514E917BD0E22393B8FBE37
    ...  username=admin.integracao@bari.com.br.dev
    ...  password=P@ssIntegracao@3
    Set Test Variable  ${HEADERS}
    ${RESPOSTAREQUEST}     Post Request     newsession      services/oauth2/token
    ...     data=${PARAMS}
    ...     headers=${HEADERS}
    Log                          ${RESPOSTAREQUEST.text}
    ${json_value}  Set Variable  ${RESPOSTAREQUEST.json()}
    ${token}  Catenate  Bearer  ${json_value['access_token']}
    Set Test Variable  ${token}
    [Return]        ${token}

Consulta SF
    [Arguments]  ${token}  ${CPF}
    Abrir conexao   https://barigui--qabari.my.salesforce.com
    ${payload}   Create Dictionary    contract=string   type=string
    ${json}  Convert Dic to Json  ${payload}  
    ${HEADERS}  Create Dictionary
    ...     Authorization=${token}    
    ${RESPOSTA}     Get Request     newsession      services/apexrest/Bari/WS_Get_Account?individualRegistrationNumber=${CPF}  
    ...     headers=${HEADERS}
    Log                             ${RESPOSTA.text}
    Should Be Equal As Strings      ${RESPOSTA.status_code}    200

Integracao SF
    [Arguments]  ${CPF}  ${name}  ${email}  ${telefone}  ${dtnasc}  ${token}
    ${nome}  Split String  ${name}  
    Abrir Conexao  https://barigui--qabari.my.salesforce.com
    ${json_string}=    catenate
    ...     {
    ...         "email": "${email}",
    ...         "firstName": "${nome[0]}",
    ...         "lastName": "${nome[1]}",
    ...         "mobilePhoneNumber": "${telefone}",
    ...         "individualRegistrationNumber": "${CPF}",
    ...         "birthDate": "${dtnasc}",
    ...         "leadStatus": "new",
    ...         "leadID": "${guidLead}",
    ...         "onboardingStep": "started"
    ...     }    
    log  ${json_string}    
    ${HEADERS}  Create Dictionary
    ...     Content-Type=application/json
    ...     Authorization=${token}    
    ${RESPOSTA}     Put Request     newsession      services/apexrest/Bari/WS_Account
    ...     data=${json_string}
    ...     headers=${HEADERS}
    Log                             ${RESPOSTA.text}
    Should Be Equal As Strings      ${RESPOSTA.status_code}    200

