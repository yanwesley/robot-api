*** Settings ***
Resource	${EXECDIR}/Resources/main.resource

*** Variables ***
@{ids}


*** Test Cases ***

Discard Client
    
    ${CPF}  Set Variable  06537262950
    ${address}  Set Variable  dzs1tEuwmac:APA91bHvGxNYjZatM72rkpKTTxYCXUVJJFydkLG91idRpudrbEuBMOna3Mqru7NcW4GIOZeyWgfl54hoPVnjWH1tpOf5MNLFljBguZ_yA_rohGCBUeh8c42jTBpaWrT4yy3ZwJttOVTZ
    ${email}  Set Variable  yan.lima@baritecnologia.com.br
    ${hash256}  Encrypt String  ${CPF}
    log  ${hash256}    
    
    ${Customerscopes}  Consulta Dynamo  Barigui.Services.FeatureFlag_Customerscopes  CustomerId  ${hash256}
	log  ${Customerscopes}
    ${stringCompare}  Convert To String  ${Customerscopes}
    ${equal}=  evaluate  "CustomerId" in """${stringCompare}"""
    Run Keyword If  ${equal}  Delete Dynamo  Barigui.Services.FeatureFlag_Customerscopes  CustomerId  ${hash256}

    ${Customers}  Consulta Dynamo  Barigui.Services.Customer_Customers  Id  ${CPF}
    log  ${Customers}
    ${stringCompare}  Convert To String  ${Customers}    
    ${equal}=  evaluate  "DocumentList" in """${stringCompare}"""
    Run Keyword If  ${equal}  Delete Dynamo  Barigui.Services.Customer_Customers  Id  ${CPF}

    ${IdentityServer}  Consulta Dynamo  Barigui.Services.IdentityServer_Users  Id  ${CPF}
    log  ${IdentityServer}
    ${stringCompare}  Convert To String  ${IdentityServer}
    ${equal}=  evaluate  "EmailIsValid" in """${stringCompare}"""
    Run Keyword If  ${equal}  Delete Dynamo  Barigui.Services.IdentityServer_Users  Id  ${CPF}

    ${TrustedRecipients}  Consulta Dynamo  Barigui.Services.Communication_TrustedRecipients  Id  ${CPF}
    log  ${TrustedRecipients}
    ${stringCompare}  Convert To String  ${TrustedRecipients}
    ${equal}=  evaluate  "FullMobilePhoneNumber" in """${stringCompare}"""
    Run Keyword If  ${equal}  Delete Dynamo  Barigui.Services.Communication_TrustedRecipients  Id  ${CPF}

    ${MFA}  Consulta Dynamo  Barigui.Services.MFA_Users  Id  ${CPF}
    log  ${MFA}
    ${stringCompare}  Convert To String  ${MFA}
    ${equal}=  evaluate  "ValidSelfie" in """${stringCompare}"""
    Run Keyword If  ${equal}  Delete Dynamo  Barigui.Services.MFA_Users  Id  ${CPF}
    
    ${Leads}  Consulta Dynamo  Barigui.Services.Onboarding_Leads  Email  ${email}
    log  ${Leads}
    ${leadsId}  Set Variable  ${EMPTY}
    ${msgA}  Run Keyword And Ignore Error  Log  ${Leads[0][0]['Id']}
    ${msgB}  Run Keyword And Ignore Error  Log  ${Leads[0]['Id']}
    Run Keyword If  '${msgA[0]}'=='PASS'  setVariableLeadId  ${Leads[0][0]['Id']} 
    Run Keyword If  '${msgB[0]}'=='PASS'  setVariableLeadId  ${Leads[0]['Id']}
    ${stringCompare}  Convert To String  ${Leads}
    ${equal}=  evaluate  "BigdatacorpPeopleResponse" in """${stringCompare}"""
    Run Keyword If  ${equal}  Delete Dynamo  Barigui.Services.Onboarding_Leads  Id  ${leadsId}
    
    # ${Subjects}  Consulta Dynamo  Barigui.Services.OnboardFraud_Subjects  externalId  ${leadsId}
    # log  ${Subjects} 
    # ${stringCompare}  Convert To String  ${Subjects} 
    # ${equal}=  evaluate  "acertResponse" in """${stringCompare}"""
    # Run Keyword If  ${equal}  Delete Dynamo  Barigui.Services.OnboardFraud_Subjects  subjectId  ${Subjects[0]['subjectId']}

    ${communication}  Consulta Dynamo  Barigui.Services.Communication_Endpoints  Address  ${address}
    log  ${communication}
    ${stringCompare}  Convert To String  ${communication}    
    ${equal}=  evaluate  "Demographic" in """${stringCompare}"""    
    Run Keyword If  ${equal}  Delete Dynamo  Barigui.Services.Communication_Endpoints  Id  ${communication[0]['Id']}

    ${CheckingAccount}  Consulta Dynamo  Barigui.Services.Account_CheckingAccount  CustomerId  ${CPF}
    log  ${CheckingAccount}    
    ${stringCompare}  Convert To String  ${CheckingAccount}
    ${equal}=  evaluate  "CreationDate" in """${stringCompare}"""
    Run Keyword If  ${equal}  Delete Dynamo  Barigui.Services.Account_CheckingAccount  Id  ${CheckingAccount[0]['Id']}  CustomerId  ${CPF}

    # ${CustomerEvents}  Consulta Dynamo  Barigui.Services.Customer_CustomerEvents  CustomerId  ${CPF}
    # log  ${CustomerEvents[0]}
    # FOR   ${ITEM}  IN RANGE  0  300   
    #     ${PROGRESS}  Run Keyword and Ignore Error  log  ${CustomerEvents[0][${ITEM}]['Id']}
    #     Run Keyword If  '${PROGRESS[0]}'=='PASS'  Append To List  ${ids}  ${CustomerEvents[0][${ITEM}]['Id']}
    #     Exit For Loop If    '${PROGRESS[0]}'=='FAIL'
    # END

    # FOR  ${idDelete}  IN  @{ids}
    #     Delete Dynamo  Barigui.Services.Customer_CustomerEvents  Id  ${idDelete}
    # END          



***Keywords***

setVariableLeadId
    [Arguments]  ${index}
    ${leadsId}  Set Variable  ${index}
    Set Test Variable   ${leadsId}

