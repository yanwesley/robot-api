*** Settings ***
Resource	${EXECDIR}/Resources/main.resource
Suite Setup   Abrir conexao  https://api.qa.bancobari.com.br

*** Test Cases ***

Testes Costumer
    
    #gera guid
    ${guidLead}  Gera Guid
    Set Test Variable  ${guidLead}
    #gera guid   
    ${guidDevice}  Gera Guid
    Set Test Variable  ${guidDevice}
    
    ${CPF}  Set Variable  06537262950
    ${NOME}  Set Variable  Yan Wesley de Lima
    ${dtnasc}  Set Variable  1988-11-20
    ${email}  Set Variable  yan.lima@baritecnologia.com.br
    ${telefone}  Set Variable  41991645776 
    ${gender}  Set Variable  MALE
    ${motherName}  Set Variable  Tania Cristina Mazieri
    ${State}  Set Variable  Paraná
    ${City}  Set Variable  Maringá
    ${maritalStatus}  Set Variable  MARRIED 
    ${spouseName}  Set Variable  Juliana Kamada
    ${postalCode}  Set Variable  80730000
    ${addressLine1}  Set Variable  Rua Padre Anchieta
    ${number}  Set Variable  2540
    ${neighborhood}  Set Variable  Bigorrilho
    ${city}  Set Variable  Curitiba
    ${state}  Set Variable  PR


    01_GET_/v1/Leads/contract?types=InformationPrivacy,SCRAcceptance
    Sleep    5s
    02_PUT_{{url}}/v1/Leads/{{lead_id}}  ${guidLead}  ${NOME}  ${CPF}
    Sleep    5s
    03_PUT_{{url}}/v1/Leads/{{lead_id}}/BirthDate  ${guidLead}  ${dtnasc}
    Sleep    5s
    04_PUT_{{url}}/v1/Leads/{{lead_id}}/geolocation  ${guidLead}
    Sleep    5s
    05_PUT_{{url}}/v1/Leads/{{lead_id}}/Email  ${guidLead}  ${email}
    Sleep    5s
    07_PUT_{{url}}/v1/Leads/{{lead_id}}/mobilePhoneNumber  ${guidLead}  ${telefone}
    Sleep    5s
    update_leads  ${guidLead}
    Abrir conexao   https://api.qa.bancobari.com.br    
    09_PUT_{{url}}/v1/Leads/{{lead_id}}/personalInfo  ${guidLead}  ${gender}  ${motherName}  ${State}  ${City}  ${maritalStatus}  ${spouseName}
    Sleep    5s
    10_PUT_{{url}}/v1/Leads/{{lead_id}}/homeAddress  ${guidLead}  ${postalCode}  ${addressLine1}  ${number}  ${neighborhood}  ${city}  ${state}
    Sleep    5s
    11_PUT_{{url}}/v1/Leads/{{lead_id}}/commercialAddress  ${guidLead}
    Sleep    5s
    12_GET_{{url}}/v1/Leads/professions
    Sleep    5s
    13_PUT_{{url}}/v1/Leads/{{lead_id}}/profession  ${guidLead}    
    Sleep    5s
    14_PUT_{{url}}/v1/Leads/{{lead_id}}/income  ${guidLead}
    Sleep    5s
    15_GET_{{url}}/v1/Leads/assetsrange     
    Sleep    5s
    16_PUT_{{url}}/v1/Leads/{{lead_id}}/assets  ${guidLead}
    Sleep    5s
    17_PUT_{{url}}/v1/Leads/{{lead_id}}/documentPicture  ${guidLead}
    Sleep    5s
    18_PUT_{{url}}/v1/Leads/{{lead_id}}/selfie  ${guidLead}
    Sleep    5s
    ${CNHDados}  19_GET_{{url}}/v1/Leads/{{lead_id}}/documentExtraction  ${guidLead}
    ${type}  Set Variable  CNH
    ${issuer}  Set Variable  Órgão de Transito
    ${issuingCountry}  Set Variable  BRA 
    ${issuingState}  Set Variable  PR
    ${number}  Set Variable  04043022043
    ${originatingCountry}  Set Variable  BRA 
    ${expiryDate}  Set Variable  2021-03-03T12:58:21.160Z  
    ${date}  Set Variable  2007-02-23
    ${issueDate}  catenate  SEPARATOR=  ${date}  T12:58:21.160Z
    20_PUT_{{url}}/v1/Leads/{{lead_id}}/additionalDocumentData  ${guidLead}  ${type}  ${issuer}  ${issuingCountry}  ${issuingState}  ${number}  ${originatingCountry}  ${expiryDate}  ${issueDate}  
    Sleep    5s
    21_PUT_{{url}}/v1/Leads/{{lead_id}}/PoliticalExposition  ${guidLead}
    Sleep    5s
    22_PUT_{{url}}/v1/Leads/{{lead_id}}/USPerson  ${guidLead}
    Sleep    5s
    ${AccountOpening}  23_GET_{{url}}/v1/Leads/contract?types=AccountOpening
    24_PUT_{{url}}/v1/Leads/{{lead_id}}/TermsAcceptance  ${guidLead}  ${AccountOpening[0]['url']}  ${AccountOpening[0]['type']}
  
***Keywords***

Split Name
    [Arguments]  ${name}
    ${nameList}  Split String  ${name}  .
    ${NOME}  Set Variable  ${nameList[1]}
    Set Test Variable  ${NOME}

No Split Name
    ${NOME}  Set Variable  ${name}
    Set Test Variable  ${NOME}

