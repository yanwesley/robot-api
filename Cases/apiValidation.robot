*** Settings ***
Resource	${EXECDIR}/Resources/main.resource
Suite Setup   Abrir conexao  https://api.qa.bancobari.com.br

*** Test Cases ***

Teste
    TokenTeste

*** Keywords ***
TokenTeste
    ${Leads}  Consulta Dynamo  Barigui.Services.Onboarding_Leads  Email  daffyne.dias@primecontrol.com.br
    log  ${Leads}
    ${leadsId}  Set Variable  ${EMPTY}
    ${msgA}  Run Keyword And Ignore Error  Log  ${Leads[0][0]['Id']}
    ${msgB}  Run Keyword And Ignore Error  Log  ${Leads[0]['Id']}
    Run Keyword If  '${msgA[0]}'=='PASS'  setVariableLeadId  ${Leads[0][0]['Id']} 
    Run Keyword If  '${msgB[0]}'=='PASS'  setVariableLeadId  ${Leads[0]['Id']}
    ${stringCompare}  Convert To String  ${Leads}
    ${equal}=  evaluate  "BigdatacorpPeopleResponse" in """${stringCompare}"""

    Update Leads  ${leadsId}

setVariableLeadId
    [Arguments]  ${index}
    ${leadsId}  Set Variable  ${index}
    Set Test Variable   ${leadsId}