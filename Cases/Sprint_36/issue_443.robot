*** Settings ***
Resource	${EXECDIR}/Resources/main.resource
Suite Setup   Abrir conexao  https://api.qa.bancobari.com.br

*** Test Cases ***

Gravação IP após onboarding
    consulta deviceId
    consulta a tabela

*** Keywords ***
consulta deviceId
    ${retorno}  consulta_dynamo    Barigui.Services.Onboarding_Leads    Email    yan.lima@primecontrol.com.br
    log  ${retorno[0]['DeviceId']}  
    Set Test Variable    ${retorno[0]['DeviceId']}

consulta a tabela
#DeviceId deve ser obtido na tabela Barigui.Services.Onboarding_Leads
    ${retorno}  consulta_dynamo    Barigui.Services.Onboarding_LeadEvents    DeviceId    ${retorno[0]['DeviceId']}
    log  ${retorno[0]} 
    Should Not Be Empty    ${retorno[0]} 
    log  ${retorno[3]} 
    Should Not Be Empty    ${retorno[3]} 
    log  ${retorno[5]} 
    Should Not Be Empty    ${retorno[5]} 
    log  ${retorno[8]} 
    Should Not Be Empty    ${retorno[8]} 
    log  ${retorno[13]} 
    Should Not Be Empty    ${retorno[13]} 
    log  ${retorno[17]} 
    Should Not Be Empty    ${retorno[17]} 
    log  ${retorno[19]} 
    Should Not Be Empty    ${retorno[19]} 
    log  ${retorno[35]} 
    Should Not Be Empty    ${retorno[35]} 
    log  ${retorno[41]} 
    Should Not Be Empty    ${retorno[41]} 
    log  ${retorno[46]} 
    Should Not Be Empty    ${retorno[46]} 
    log  ${retorno[52]} 
    Should Not Be Empty    ${retorno[52]}
    log  ${retorno[55]} 
    Should Not Be Empty    ${retorno[55]}
    log  ${retorno[61]} 
    Should Not Be Empty    ${retorno[61]}
    log  ${retorno[79]} 
    Should Not Be Empty    ${retorno[79]}
    log  ${retorno[81]} 
    Should Not Be Empty    ${retorno[81]}
    log  ${retorno[82]} 
    Should Not Be Empty    ${retorno[82]}
    log  ${retorno[83]} 
    Should Not Be Empty    ${retorno[83]}
    log  ${retorno[85]} 
    Should Not Be Empty    ${retorno[85]}
    log  ${retorno[88]} 
    Should Not Be Empty    ${retorno[88]}
    log  ${retorno[89]} 
    Should Not Be Empty    ${retorno[89]}
    log  ${retorno[91]} 
    Should Not Be Empty    ${retorno[91]}
    log  ${retorno[99]} 
    Should Not Be Empty    ${retorno[99]}
    log  ${retorno[101]} 
    Should Not Be Empty    ${retorno[101]}
    log  ${retorno[105]} 
    Should Not Be Empty    ${retorno[105]}
    log  ${retorno[108]} 
    Should Not Be Empty    ${retorno[108]}
    log  ${retorno[109]} 
    Should Not Be Empty    ${retorno[109]}
    log  ${retorno[127]} 
    Should Not Be Empty    ${retorno[127]}