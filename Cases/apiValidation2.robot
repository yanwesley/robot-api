*** Settings ***
Resource	${EXECDIR}/Resources/main.resource
Suite Setup   Abrir conexao  https://api.qa.bancobari.com.br

*** Variables ***
@{ids}

*** Test Cases ***

05_PUT_{{url}}/v1/Leads/{{lead_id}}/Email
    ${CustomerEvents}  Consulta Dynamo  Barigui.Services.Customer_CustomerEvents  CustomerId  06537262950
    log  ${CustomerEvents[0][0]['Id']}
    FOR   ${ITEM}  IN RANGE  0  300   
        ${PROGRESS}  Run Keyword and Ignore Error  log  ${CustomerEvents[0][${ITEM}]['Id']}
        Run Keyword If  '${PROGRESS[0]}'=='PASS'  Append To List  ${ids}  ${CustomerEvents[0][${ITEM}]['Id']}
        Exit For Loop If    '${PROGRESS[0]}'=='FAIL'
    END

    FOR  ${idDelete}  IN  @{ids}
        Delete Dynamo  Barigui.Services.Customer_CustomerEvents  Id  ${idDelete}
    END          

    ${CustomerEvents}  Consulta Dynamo  Barigui.Services.Customer_CustomerEvents  CustomerId  06537262950
    log  ${CustomerEvents[0]['Id']}
    FOR   ${ITEM}  IN RANGE  0  300   
        ${PROGRESS}  Run Keyword and Ignore Error  log  ${CustomerEvents[${ITEM}]['Id']}
        Run Keyword If  '${PROGRESS[0]}'=='PASS'  Append To List  ${ids}  ${CustomerEvents[${ITEM}]['Id']}
        Exit For Loop If    '${PROGRESS[0]}'=='FAIL'
    END

    FOR  ${idDelete}  IN  @{ids}
        Delete Dynamo  Barigui.Services.Customer_CustomerEvents  Id  ${idDelete}
    END   

