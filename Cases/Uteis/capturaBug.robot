*** Settings ***
Resource        ${EXECDIR}/Resources/main.resource

*** Variable ***
${number}=  0


*** Test Case ***

capturaBugs HML Cadastro
    Abrir Navegador  https://gitlab.com/users/sign_in
    Wait Until Element Is Visible         id=user_login                20 second
    Input Text                            id=user_login                william.filho@primecontrol.com.br
    Input Text                            id=user_password             W@140416    
    Click Element                         xpath=//*[@id="new_user"]/div[5]/input
    Go To  https://gitlab.com/bancobarigui/projects/digitalbank/clientes/cadastro/-/issues?label_name%5B%5D=Bug&page=100&scope=all&sort=created_asc&state=all
    ${linkultima}  Get Location
    log  ${linkultima}
    Go To  https://gitlab.com/bancobarigui/projects/digitalbank/clientes/cadastro/-/issues?label_name%5B%5D=Bug&page=1&scope=all&sort=created_asc&state=all
    ${bugType}  Set Variable  HML

    Excel Generation  ${bugType}

    ${resultextract1}  Fetch From Right        ${linkultima}    &page=
    ${finalPage}  Fetch From Left         ${resultextract1}   &scope=    
    ${finalRange}  Evaluate  ${finalPage}+1
    Set Test Variable  ${finalRange}   
    
    :for  ${Page}    IN RANGE  1    ${finalRange}
    \  Set Test Variable  ${Page}
    \  Set Test Variable  ${pagina}  https://gitlab.com/bancobarigui/projects/digitalbank/clientes/cadastro/-/issues?label_name%5B%5D=Bug&page=${Page}&scope=all&sort=created_asc&state=all
    \  Go To  ${pagina}
    \  Pages Bug
    
    Save Excel Document  ${namedoc}
    Close Browser  

capturaBugs PROD Cadastro
    Abrir Navegador  https://gitlab.com/users/sign_in
    Wait Until Element Is Visible         id=user_login                20 second
    Input Text                            id=user_login                william.filho@primecontrol.com.br
    Input Text                            id=user_password             W@140416    
    Click Element                         xpath=//*[@id="new_user"]/div[5]/input
    Go To  https://gitlab.com/bancobarigui/projects/digitalbank/clientes/cadastro/-/issues?label_name%5B%5D=Bug+-+Producao&page=100&scope=all&sort=created_asc&state=all
    ${linkultima}  Get Location
    log  ${linkultima}
    Go To  https://gitlab.com/bancobarigui/projects/digitalbank/clientes/cadastro/-/issues?label_name%5B%5D=Bug+-+Producao&page=1&scope=all&sort=created_asc&state=all
    ${bugType}  Set Variable  PROD

    Excel Generation  ${bugType}

    ${resultextract1}  Fetch From Right        ${linkultima}    &page=
    ${finalPage}  Fetch From Left         ${resultextract1}   &scope=    
    ${finalRange}  Evaluate  ${finalPage}+1
    Set Test Variable  ${finalRange}    
    
    Run Keyword If  '${finalRange}'=='2'  One Page
    Run Keyword If  '${finalRange}'>'2'  Multiples pages
    
    Save Excel Document  ${namedoc}
    Close Browser

*** Keywords ***

Excel Generation
    [Arguments]  ${bugType}
    ${date} =	Get Current Date  result_format=%d-%m-%Y
    ${namedoc}  catenate  Bugs${bugType}Cadastro${date}.xlsx
    Set Global Variable  ${namedoc}	
    ${doc1}  Create Excel Document	${namedoc}
    Write Excel Cell                      row_num=1  col_num=1       value=NOME        
    Write Excel Cell                      row_num=1  col_num=2       value=CODIGO        
    Write Excel Cell                      row_num=1  col_num=3       value=PRIORIDADE        
    Write Excel Cell                      row_num=1  col_num=4       value=DATA DE ABERTURA        
    Write Excel Cell                      row_num=1  col_num=5       value=DATA DE FECHAMENTO
    Write Excel Cell                      row_num=1  col_num=6       value=BUGS POR CRITICIDADE       
    ${number}  Set Variable  0
    ${number}  evaluate  ${number}+1
    Set Global Variable  ${number} 
    Write Excel Cell                      row_num=2  col_num=6       value=P1        
    Write Excel Cell                      row_num=3  col_num=6       value=P2        
    Write Excel Cell                      row_num=4  col_num=6       value=P3        
    Write Excel Cell                      row_num=5  col_num=6       value=P4
    Write Excel Cell                      row_num=6  col_num=6       value=Total de Bugs
    Write Excel Cell                      row_num=2  col_num=7       value= =COUNTIF(C2:C1000;"P1")       
    Write Excel Cell                      row_num=3  col_num=7       value= =COUNTIF(C2:C1000;"P2")        
    Write Excel Cell                      row_num=4  col_num=7       value= =COUNTIF(C2:C1000;"P3")        
    Write Excel Cell                      row_num=5  col_num=7       value= =COUNTIF(C2:C1000;"P4")
    Write Excel Cell                      row_num=6  col_num=7       value= =SUM(G2:G5)        

Convert Data
    [Arguments]  ${time}
    ${date} 	Convert Date	${time} 	date_format=%Y-%m-%d
    [Return]  ${date}

Register Bug
    [Arguments]     ${Count}     
    ${nome}  Get Text                     xpath=//li[${Count}][@data-qa-selector='issue']//a
    ${numero}  Get Text                   xpath=//li[${Count}][@data-qa-selector='issue']//div[2]/span
    ${time}  Get Element Attribute        xpath=//li[${Count}][@data-qa-selector='issue']//time  datetime
    ${dataDeAbertura}  Get Substring  ${time}  0  10    
    ${Progress}  Run Keyword And Ignore Error  Element Should Be Visible  xpath=//li[${Count}][@data-qa-selector='issue']//span/a[contains(@data-title,'Prioridade')]/span
    Run Keyword If  '${Progress[0]}'=='PASS'  Get prioridade  ${Count}
    Run Keyword If  '${Progress[0]}'=='FAIL'  Sem prioridade
    ${status}  Get Text                   xpath=//li[${Count}][@data-qa-selector='issue']//div[2]/div[2]/ul/li
    #${time2}  Get Element Attribute       xpath=//li[${Count}][@data-qa-selector='issue']//div[2]/div[2]/div/span/time  datetime
    #${dataDeFechamento}  Get Substring  ${time2}  0  10    
    #Number Definition  ${Page}  ${Count}
    ${number}  evaluate  ${number}+1  
    Write Excel Cell                      row_num=${number}  col_num=1       value=${nome}        
    Write Excel Cell                      row_num=${number}  col_num=2       value=${numero}      
    Write Excel Cell                      row_num=${number}  col_num=3       value=${prioridade}  
    Write Excel Cell                      row_num=${number}  col_num=4       value=${dataDeAbertura}
    Set Test Variable  ${number}        
    Run Keyword If  """${status}"""=="""CLOSED"""  Closed Bug  ${number}  ${Count}

Closed Bug
    [Arguments]  ${number}  ${Count}
    Wait Until Element Is Visible  xpath=//li[${Count}][@data-qa-selector='issue']//a
    Click Element  xpath=//li[${Count}][@data-qa-selector='issue']//a
    Wait Until Element Is Visible         xpath=//span[contains(text(),'closed')]/../../a/time  60 second                20 second
    ${time}  Get Element Attribute   xpath=//span[contains(text(),'closed')]/../../a/time  title
    ${Data}  Get Substring  ${time}  0  12
    ${stringData}  Remove String  ${Data}  ,
    ${dataDeFechamento}  Retorna Data  ${stringData}    
    Write Excel Cell                      row_num=${number}  col_num=5       value=${dataDeFechamento}
    Go To  ${pagina}    

Reader Bug
    [Arguments]     ${Count}    
    ${Progress}  Run Keyword And Ignore Error  Element Should Be Visible  xpath=//li[${Count}][@data-qa-selector='issue']//span/a[contains(@data-title,'produção')]/span
    Run Keyword If  '${Progress[0]}'=='FAIL'  Register Bug  ${Count}

Pages Bug
    :for  ${Count}    IN RANGE  1    21
    \    ${time}  Get Element Attribute        xpath=//li[${Count}][@data-qa-selector='issue']//time  datetime
    \    ${dataDeAbertura}  Get Substring  ${time}  0  10
    \    ${ano}  Get Substring  ${dataDeAbertura}  0  4
    \    ${mes}  Get Substring  ${dataDeAbertura}  5  7
    \    ${Progress}  Run Keyword And Ignore Error  Element Should Be Visible  xpath=//li[${Count}][@data-qa-selector='issue']//a
    \    Run Keyword If  '${Progress[0]}'=='PASS' and '${ano}'>='2020' and '${mes}'>='02'  Reader Bug  ${Count} 

Get prioridade
    [Arguments]     ${Count} 
    ${prioridade}  Get Text               xpath=//li[${Count}][@data-qa-selector='issue']//span/a[contains(@data-title,'Prioridade')]/span
    Set Test Variable  ${prioridade} 

Sem prioridade
    ${prioridade}  Set Variable  N/A
    Set Test Variable  ${prioridade} 

Number Definition
    [Arguments]     ${Page}  ${Count}
    Run Keyword If  """${Page}"""=="""1"""  Page1  ${Count}
    Run Keyword Unless  """${Page}"""=="""1"""  Other Pages
      
Page1
    [Arguments]  ${Count}
    ${number}  evaluate  ${Count}+1
    Set Test Variable  ${number}

Other Pages
    ${num}  evaluate  ${number}+1
    ${number}  Set Variable  ${num}
    Set Test Variable  ${number}

Reader Prod Bugs
    :for  ${Count}    IN RANGE  1    21
    \  ${Progress}  Run Keyword And Ignore Error  Element Should Be Visible  xpath=//li[${Count}][@data-qa-selector='issue']//a
    \  Run Keyword If  '${Progress[0]}'=='PASS'  Register Bug  ${Count} 
 
Multiples pages    
    :for  ${Page}    IN RANGE  1    ${finalRange}
    \  Set Test Variable  ${Page}
    \  Set Test Variable  ${pagina}  https://gitlab.com/bancobarigui/projects/digitalbank/clientes/cadastro/-/issues?label_name%5B%5D=Bug+-+Producao&page=${Page}&scope=all&sort=created_asc&state=all
    \  Go To  ${pagina}
    \  Reader Prod Bugs

One Page
    Set Test Variable  ${Page}  1
    Set Test Variable  ${pagina}  https://gitlab.com/bancobarigui/projects/digitalbank/clientes/cadastro/-/issues?label_name%5B%5D=Bug+-+Producao&page=${Page}&scope=all&sort=created_asc&state=all
    Go To  ${pagina}
    Reader Prod Bugs

    

