*** Settings ***
Resource        ${EXECDIR}/Resources/main.resource

*** Variable ***
${number}=  0
${nome}  
${numero} 
${time}
${dataDeAbertura}
${prioridade}
${namedoc}	
${json}
${totalPages}

*** Test Case ***

capturaBugs HML
   
    ${bugType}  Set Variable  HML

    Excel Generation  ${bugType}

    Abrir Conexao  https://gitlab.com/api/v4/projects/14259687   

    ${HEADERS}  Create Dictionary
    ...     Authorization=Bearer vEG8sLxnGKCuR6BhYS2s
    ${RESPOSTA}     Get Request     newsession      issues?id=14259687&labels%5B%5D=Bug&not%5Blabels%5D%5B%5D=Bug+-+Producao&order_by=created_at&page=1&pagination=keyset&per_page=100&sort=asc&state=all&with_labels_details=false&created_after=2020-02-01
    ...     headers=${HEADERS}
    Log                             ${RESPOSTA.text}
    Log                             ${RESPOSTA.headers}
    ${totalPages}  Set Variable  ${RESPOSTA.headers['X-Total-Pages']}
    Log  ${totalPages}
    Set Global Variable  ${totalPages}      
    Should Be Equal As Strings      ${RESPOSTA.status_code}    200   
    

    FOR  ${page}  IN RANGE  1  ${totalPages}+1
        ${json}  Consulta HML  ${page}
        Log  ${json}
        Run Keyword and Ignore Error  Register Bug  ${json}
    END

    Save Excel Document  ${namedoc}

capturaBugs PROD
    ${bugType}  Set Variable  PROD

    Excel Generation  ${bugType}
    
    Abrir Conexao  https://gitlab.com/api/v4/projects/14259687   

    ${HEADERS}  Create Dictionary
    ...     Authorization=Bearer vEG8sLxnGKCuR6BhYS2s
    ${RESPOSTA}     Get Request     newsession      issues?id=14259687&labels%5B%5D=Bug+-+Producao&order_by=created_at&page=1&pagination=keyset&per_page=100&sort=asc&state=all&with_labels_details=false&created_after=2020-02-01
    ...     headers=${HEADERS}
    Log                             ${RESPOSTA.text}
    Log                             ${RESPOSTA.headers}
    ${totalPages}  Set Variable  ${RESPOSTA.headers['X-Total-Pages']}
    Log  ${totalPages}
    Set Global Variable  ${totalPages}      
    Should Be Equal As Strings      ${RESPOSTA.status_code}    200   
    
    FOR  ${page}  IN RANGE  1  ${totalPages}+1
        ${json}  Consulta PROD  ${page}
        Log  ${json}
        Run Keyword and Ignore Error  Register Bug  ${json}
    END

    Save Excel Document  ${namedoc}

*** Keywords ***

Consulta HML
    [Arguments]  ${page}
    Abrir Conexao  https://gitlab.com/api/v4/projects/14259687   

    ${HEADERS}  Create Dictionary
    ...     Authorization=Bearer vEG8sLxnGKCuR6BhYS2s
    ${RESPOSTA}     Get Request     newsession      issues?id=14259687&labels%5B%5D=Bug&not%5Blabels%5D%5B%5D=Bug+-+Producao&order_by=created_at&page=${page}&pagination=keyset&per_page=100&sort=asc&state=all&with_labels_details=false&created_after=2020-02-01
    ...     headers=${HEADERS}     
    Should Be Equal As Strings      ${RESPOSTA.status_code}    200
    ${json}  Set Variable  ${RESPOSTA.json()}
    Set Global Variable  ${json}
    [Return]  ${json}

Consulta PROD
    [Arguments]  ${page}
    Abrir Conexao  https://gitlab.com/api/v4/projects/14259687   

    ${HEADERS}  Create Dictionary
    ...     Authorization=Bearer vEG8sLxnGKCuR6BhYS2s
    ${RESPOSTA}     Get Request     newsession      issues?id=14259687&labels%5B%5D=Bug+-+Producao&order_by=created_at&page=${page}&pagination=keyset&per_page=100&sort=asc&state=all&with_labels_details=false&created_after=2020-02-01
    ...     headers=${HEADERS}     
    Should Be Equal As Strings      ${RESPOSTA.status_code}    200
    ${json}  Set Variable  ${RESPOSTA.json()}
    Set Global Variable  ${json}
    [Return]  ${json}


Excel Generation
    [Arguments]  ${bugType}
    ${date} =	Get Current Date  result_format=%d-%m-%Y
    ${namedoc}  catenate  Bugs${bugType}Cadastro${date}.xlsx
    Set Global Variable  ${namedoc}
    ${doc1}  Create Excel Document	${namedoc}
    Write Excel Cell                      row_num=1  col_num=1       value=NOME        
    Write Excel Cell                      row_num=1  col_num=2       value=CODIGO        
    Write Excel Cell                      row_num=1  col_num=3       value=PRIORIDADE        
    Write Excel Cell                      row_num=1  col_num=4       value=DATA DE ABERTURA        
    Write Excel Cell                      row_num=1  col_num=5       value=DATA DE FECHAMENTO
    Write Excel Cell                      row_num=1  col_num=6       value=BUGS POR CRITICIDADE       
    ${number}  Set Variable  0
    ${number}  evaluate  ${number}+1
    Set Global Variable  ${number} 
    Write Excel Cell                      row_num=2  col_num=6       value=P1        
    Write Excel Cell                      row_num=3  col_num=6       value=P2        
    Write Excel Cell                      row_num=4  col_num=6       value=P3        
    Write Excel Cell                      row_num=5  col_num=6       value=P4
    Write Excel Cell                      row_num=6  col_num=6       value=Total de Bugs
    Write Excel Cell                      row_num=2  col_num=7       value= =COUNTIF(C2:C1000;"P1")       
    Write Excel Cell                      row_num=3  col_num=7       value= =COUNTIF(C2:C1000;"P2")        
    Write Excel Cell                      row_num=4  col_num=7       value= =COUNTIF(C2:C1000;"P3")        
    Write Excel Cell                      row_num=5  col_num=7       value= =COUNTIF(C2:C1000;"P4")
    Write Excel Cell                      row_num=6  col_num=7       value= =SUM(G2:G5)       


Register Bug
    [Arguments]  ${json}
    ${issue}  Set Variable  ${json}
    Log  ${issue} 
    Set Global Variable  ${issue}
    FOR  ${item}  IN RANGE  0  101  
    Log  ${issue} 
    Log  ${issue[${item}]['title']}
    Log  ${issue[${item}]['iid']}
    Log  ${issue[${item}]['labels']}
    Log  ${issue[${item}]['created_at']}
    Log  ${issue[${item}]['closed_at']}

    Set Test Variable  ${nome}        ${issue[${item}]['title']}
    Set Test Variable  ${numero}      ${issue[${item}]['iid']}
    ${time}  Convert To String        ${issue[${item}]['created_at']}
    ${dataDeAbertura}  Get Substring  ${time}  0  10   
    Set Test Variable  ${dataDeAbertura} 
    ${time}  Convert To String  ${issue[${item}]['closed_at']}
    ${dataDeFechamento}  Get Substring  ${time}  0  10
    Set Test Variable  ${dataDeFechamento}
    Prioridade  ${item}
    
    ${number}  evaluate  ${number}+1  
    Write Excel Cell                      row_num=${number}  col_num=1       value=${nome}        
    Write Excel Cell                      row_num=${number}  col_num=2       value=${numero}      
    Write Excel Cell                      row_num=${number}  col_num=3       value=${prioridade}  
    Write Excel Cell                      row_num=${number}  col_num=4       value=${dataDeAbertura}
    Write Excel Cell                      row_num=${number}  col_num=5       value=${dataDeFechamento}
    Set Global Variable  ${number} 

    END

Prioridade
    [Arguments]  ${item}
    @{labels}  Set Variable  ${issue[${item}]['labels']}
    FOR  ${label}  IN  @{labels}
        Log  ${label}
        ${label}  Convert To String  ${label}
        ${aprove}  evaluate  "P1" in """${label}"""
        Run Keyword If  ${aprove}  Set Test Variable  ${prioridade}  ${label}
        Exit for Loop If  ${aprove}
        ${aprove}  evaluate  "P2" in """${label}"""
        Run Keyword If  ${aprove}  Set Test Variable  ${prioridade}  ${label}
        Exit for Loop If  ${aprove}
        ${aprove}  evaluate  "P3" in """${label}"""
        Run Keyword If  ${aprove}  Set Test Variable  ${prioridade}  ${label}
        Exit for Loop If  ${aprove}
        ${aprove}  evaluate  "P4" in """${label}"""
        Run Keyword If  ${aprove}  Set Test Variable  ${prioridade}  ${label}
        Exit for Loop If  ${aprove}
    END
    Set Test Variable  ${prioridade}
    Set Global Variable  ${issue}
