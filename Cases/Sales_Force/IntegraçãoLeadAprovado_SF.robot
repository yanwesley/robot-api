*** Settings ***
Resource	${EXECDIR}/Resources/main.resource
Suite Setup   Abrir conexao  https://api.qa.bancobari.com.br
Default Tags	Sprint35

*** Test Cases ***

Testes Costumer
    
    #gera guid
    ${guidLead}  Gera Guid
    Set Test Variable  ${guidLead}
    ${guidDevice}  Gera Guid
    Set Test Variable  ${guidDevice}
    
    ${CPF}  Set Variable  06537262950
    ${NOME}  Set Variable  Yan Wesley de Lima
    ${dtnasc}  Set Variable  1988-11-20
    ${email}  Set Variable  yan.lima@primecontrol.com.br
    ${telefone}  Set Variable  41991645776 
    ${gender}  Set Variable  MALE
    ${motherName}  Set Variable  Tania Cristina Mazieri
    ${State}  Set Variable  Paraná
    ${City}  Set Variable  Maringá
    ${maritalStatus}  Set Variable  MARRIED 
    ${spouseName}  Set Variable  Juliana Kamada
    ${postalCode}  Set Variable  80730000
    ${addressLine1}  Set Variable  Rua Padre Anchieta
    ${number}  Set Variable  2540
    ${neighborhood}  Set Variable  Bigorrilho
    ${city}  Set Variable  Curitiba
    ${state}  Set Variable  PR


    01_GET_/v1/Leads/contract?types=InformationPrivacy,SCRAcceptance
    02_PUT_{{url}}/v1/Leads/{{lead_id}}  ${guidLead}  ${NOME}  ${CPF}
    03_PUT_{{url}}/v1/Leads/{{lead_id}}/BirthDate  ${guidLead}  ${dtnasc}
    04_PUT_{{url}}/v1/Leads/{{lead_id}}/geolocation  ${guidLead}
    05_PUT_{{url}}/v1/Leads/{{lead_id}}/Email  ${guidLead}  ${email}
    07_PUT_{{url}}/v1/Leads/{{lead_id}}/mobilePhoneNumber  ${guidLead}  ${telefone}
    
    Abrir conexao   https://api.qa.bancobari.com.br    
    09_PUT_{{url}}/v1/Leads/{{lead_id}}/personalInfo  ${guidLead}  ${gender}  ${motherName}  ${State}  ${City}  ${maritalStatus}  ${spouseName}
    10_PUT_{{url}}/v1/Leads/{{lead_id}}/homeAddress  ${guidLead}  ${postalCode}  ${addressLine1}  ${number}  ${neighborhood}  ${city}  ${state}
    11_PUT_{{url}}/v1/Leads/{{lead_id}}/commercialAddress  ${guidLead}
    12_GET_{{url}}/v1/Leads/professions
    13_PUT_{{url}}/v1/Leads/{{lead_id}}/profession  ${guidLead}    
    14_PUT_{{url}}/v1/Leads/{{lead_id}}/income  ${guidLead}
    15_GET_{{url}}/v1/Leads/assetsrange     
    16_PUT_{{url}}/v1/Leads/{{lead_id}}/assets  ${guidLead}
    17_PUT_{{url}}/v1/Leads/{{lead_id}}/documentPicture  ${guidLead}
    18_PUT_{{url}}/v1/Leads/{{lead_id}}/selfie  ${guidLead}
    ${CNHDados}  19_GET_{{url}}/v1/Leads/{{lead_id}}/documentExtraction  ${guidLead}
    ${type}  Set Variable  CNH
    ${issuer}  Set Variable  Órgão de Transito
    ${issuingCountry}  Set Variable  BRA 
    ${issuingState}  Set Variable  PR
    ${number}  Set Variable  04043022043
    ${originatingCountry}  Set Variable  BRA 
    ${expiryDate}  Set Variable  2021-03-03T12:58:21.160Z  
    ${date}  Set Variable  2007-02-23
    ${issueDate}  catenate  SEPARATOR=  ${date}  T12:58:21.160Z
    20_PUT_{{url}}/v1/Leads/{{lead_id}}/additionalDocumentData  ${guidLead}  ${type}  ${issuer}  ${issuingCountry}  ${issuingState}  ${number}  ${originatingCountry}  ${expiryDate}  ${issueDate}  
    21_PUT_{{url}}/v1/Leads/{{lead_id}}/PoliticalExposition  ${guidLead}
    22_PUT_{{url}}/v1/Leads/{{lead_id}}/USPerson  ${guidLead}
    ${AccountOpening}  23_GET_{{url}}/v1/Leads/contract?types=AccountOpening
    24_PUT_{{url}}/v1/Leads/{{lead_id}}/TermsAcceptance  ${guidLead}  ${AccountOpening[0]['url']}  ${AccountOpening[0]['type']}
  
    ${token}  Gera Token SF
    Consulta SF  ${token}  ${CPF}

***Keywords***

Split Name
    [Arguments]  ${name}
    ${nameList}  Split String  ${name}  .
    ${NOME}  Set Variable  ${nameList[1]}
    Set Test Variable  ${NOME}

No Split Name
    ${NOME}  Set Variable  ${name}
    Set Test Variable  ${NOME}

Gera Token SF
    Abrir conexao   https://test.salesforce.com
    ${HEADERS}  Create Dictionary
    ...  Content-Type=application/x-www-form-urlencoded
    ${PARAMS}  Create Dictionary
    ...  grant_type=password
    ...  client_id=3MVG9Vik22TUgUphKhs9dOlQTssExvUo53Ez3Jl1IAYrDcy5Ete.gED_B5Or1jjCyQSv_hKXViKgmnTym9R1T
    ...  client_secret=FA050230A427D30689D08F4DA22F9ABAFCE5AF0602CFC65A5D940DF6D7D5A4F4
    ...  username=admin.integracao@bari.com.br.qabari
    ...  password=P@ssIntegracao@5
    Set Test Variable  ${HEADERS}
    ${RESPOSTAREQUEST}     Post Request     newsession      services/oauth2/token
    ...     data=${PARAMS}
    ...     headers=${HEADERS}
    Log                          ${RESPOSTAREQUEST.text}
    ${json_value}  Set Variable  ${RESPOSTAREQUEST.json()}
    ${token}  Catenate  Bearer  ${json_value['access_token']}
    Set Test Variable  ${token}
    [Return]        ${token}

Consulta SF
    [Arguments]  ${token}  ${CPF}
    Abrir conexao   https://barigui--qabari.my.salesforce.com
    ${payload}   Create Dictionary    contract=string   type=string
    ${json}  Convert Dic to Json  ${payload}  
    ${HEADERS}  Create Dictionary
    ...     Authorization=${token}    
    ${RESPOSTA}     Get Request     newsession      services/apexrest/Bari/WS_Get_Account?individualRegistrationNumber=${CPF}  
    ...     headers=${HEADERS}
    Log                             ${RESPOSTA.text}
    Should Be Equal As Strings      ${RESPOSTA.status_code}    200

setVariableLeadId
    [Arguments]  ${index}
    ${leadsId}  Set Variable  ${index}
    Set Test Variable  ${leadsId}

validaStatusMesa
    [Arguments]  ${status}  
    Log  ${status}
    ${valida}    Evaluate   "${status}"=="pending"
    Set Test Variable    ${valida}