*** Settings ***
# ... Validar que o onboardingState da tabela de fraude está
# ... sendo enviado para o SalesForce
Resource	${EXECDIR}/Resources/main.resource
Suite Setup   Abrir conexao  https://api.qa.bancobari.com.br
Default Tags	Sprint38

*** Test Cases ***

Testes Costumer
    
    #gera guid
    ${guidLead}  Gera Guid
    Set Test Variable  ${guidLead}
    ${guidDevice}  Gera Guid
    Set Test Variable  ${guidDevice}
    
    ${CPF}  Set Variable  06537262950
    ${NOME}  Set Variable  Yan Wesley de Lima
    ${dtnasc}  Set Variable  1988-11-20
    ${email}  Set Variable  yan.lima@primecontrol.com.br
    ${telefone}  Set Variable  41991645776 
    ${gender}  Set Variable  MALE
    ${motherName}  Set Variable  Tania Cristina Mazieri
    ${State}  Set Variable  Paraná
    ${City}  Set Variable  Maringá
    ${maritalStatus}  Set Variable  MARRIED 
    ${spouseName}  Set Variable  Juliana Kamada
    ${postalCode}  Set Variable  80730000
    ${addressLine1}  Set Variable  Rua Padre Anchieta
    ${number}  Set Variable  2540
    ${neighborhood}  Set Variable  Bigorrilho
    ${city}  Set Variable  Curitiba
    ${state}  Set Variable  PR
    ${tipoOnboarding}  Set Variable   internalDesk


    01_GET_/v1/Leads/contract?types=InformationPrivacy,SCRAcceptance
    Sleep    5    
    02_PUT_{{url}}/v1/Leads/{{lead_id}}  ${guidLead}  ${NOME}  ${CPF}
    Sleep    5
    03_PUT_{{url}}/v1/Leads/{{lead_id}}/BirthDate  ${guidLead}  ${dtnasc}
    Sleep    5
    04_PUT_{{url}}/v1/Leads/{{lead_id}}/geolocation  ${guidLead}
    Sleep    5
    05_PUT_{{url}}/v1/Leads/{{lead_id}}/Email  ${guidLead}  ${email}
    Sleep    5
    07_PUT_{{url}}/v1/Leads/{{lead_id}}/mobilePhoneNumber  ${guidLead}  ${telefone}
    Sleep    5
    update_leads  ${guidLead}
    
    Abrir conexao   https://api.qa.bancobari.com.br    
    09_PUT_{{url}}/v1/Leads/{{lead_id}}/personalInfo  ${guidLead}  ${gender}  ${motherName}  ${State}  ${City}  ${maritalStatus}  ${spouseName}
    Sleep    2
    10_PUT_{{url}}/v1/Leads/{{lead_id}}/homeAddress  ${guidLead}  ${postalCode}  ${addressLine1}  ${number}  ${neighborhood}  ${city}  ${state}
    Sleep    2
    11_PUT_{{url}}/v1/Leads/{{lead_id}}/commercialAddress  ${guidLead}
    Sleep    2
    12_GET_{{url}}/v1/Leads/professions
    Sleep    2
    13_PUT_{{url}}/v1/Leads/{{lead_id}}/profession  ${guidLead}    
    Sleep    2
    14_PUT_{{url}}/v1/Leads/{{lead_id}}/income  ${guidLead}
    Sleep    2
    15_GET_{{url}}/v1/Leads/assetsrange     
    Sleep    2
    16_PUT_{{url}}/v1/Leads/{{lead_id}}/assets  ${guidLead}
    Sleep    2
    17_PUT_{{url}}/v1/Leads/{{lead_id}}/documentPicture  ${guidLead}
    Sleep    4
    18_PUT_{{url}}/v1/Leads/{{lead_id}}/selfie  ${guidLead}
    Sleep    4
    ${CNHDados}  19_GET_{{url}}/v1/Leads/{{lead_id}}/documentExtraction  ${guidLead}
    ${type}  Set Variable  CNH
    ${issuer}  Set Variable  Órgão de Transito
    ${issuingCountry}  Set Variable  BRA 
    ${issuingState}  Set Variable  PR
    ${number}  Set Variable  04043022043
    ${originatingCountry}  Set Variable  BRA 
    ${expiryDate}  Set Variable  2021-03-03T12:58:21.160Z  
    ${date}  Set Variable  2007-02-23
    ${issueDate}  catenate  SEPARATOR=  ${date}  T12:58:21.160Z
    20_PUT_{{url}}/v1/Leads/{{lead_id}}/additionalDocumentData  ${guidLead}  ${type}  ${issuer}  ${issuingCountry}  ${issuingState}  ${number}  ${originatingCountry}  ${expiryDate}  ${issueDate}  
    Sleep    2
    21_PUT_{{url}}/v1/Leads/{{lead_id}}/PoliticalExposition  ${guidLead}
    Sleep    2
    22_PUT_{{url}}/v1/Leads/{{lead_id}}/USPerson  ${guidLead}
    Sleep    2
    ${AccountOpening}  23_GET_{{url}}/v1/Leads/contract?types=AccountOpening
    Sleep    2
    24_PUT_{{url}}/v1/Leads/{{lead_id}}/TermsAcceptance  ${guidLead}  ${AccountOpening[0]['url']}  ${AccountOpening[0]['type']}
     
    #aprovação na mesa
    ${email}  Set Variable  yan.lima@primecontrol.com.br
    ${Leads}  Consulta Dynamo  Barigui.Services.Onboarding_Leads  Email  ${email}
    Log  ${Leads}
    ${msgA}  Run Keyword And Ignore Error  Log  ${Leads[0][0]['Id']}
    ${msgB}  Run Keyword And Ignore Error  Log  ${Leads[0]['Id']}
    Run Keyword If  '${msgA[0]}'=='PASS'  setVariableLeadId  ${Leads[0][0]['Id']} 
    Run Keyword If  '${msgB[0]}'=='PASS'  setVariableLeadId  ${Leads[0]['Id']}
    ${valida}  Set Variable    false
    Set Test Variable    ${valida}
    :FOR    ${COUNT}    IN RANGE    0    400
    \    ${Subjects}  Consulta Dynamo  Barigui.Services.OnboardFraud_Subjects  externalId  ${leadsId}
    \    ${msg}  Run Keyword And Ignore Error  log  ${Subjects[0]['subjectId']}
    \    Run Keyword If   "${msg[0]}" == "PASS"         validaStatusMesa     ${Subjects[0]['status']}  
    \    Exit For Loop IF  "${valida}" == "True"     
    \    Run Keyword If    ${COUNT} == 400    Log    Foram feitas 400 tentativas, registro não encontrado!     
    ${Token}  Gera Token Aproved
    log  ${Token}
    PUT_subjects/{subjectId}  ${Token}  ${Subjects[0]['subjectId']}

    #Get OnboardingStep
    ${retorno}  Consulta Dynamo  Barigui.Services.OnboardFraud_Subjects  externalId  ${leadsId}
    log  ${retorno[0]['approvalType']}
    Should Be Equal     ${retorno[0]['approvalType']}   ${tipoOnboarding}
    Set Test Variable    ${onboardingStep}    ${tipoOnboarding}
  
    ${token}  Gera Token SF
    Integracao SF  ${guidLead}  ${email}  ${dtnasc}  ${date}  ${token}  ${expiryDate}  ${CPF}  
    Consulta SF  ${token}  ${CPF}

***Keywords***

Split Name
    [Arguments]  ${name}
    ${nameList}  Split String  ${name}  .
    ${NOME}  Set Variable  ${nameList[1]}
    Set Test Variable  ${NOME}

No Split Name
    ${NOME}  Set Variable  ${name}
    Set Test Variable  ${NOME}

Gera Token SF
    Abrir conexao   https://test.salesforce.com
    ${HEADERS}  Create Dictionary
    ...  Content-Type=application/x-www-form-urlencoded
    ${PARAMS}  Create Dictionary
    ...  grant_type=password
    ...  client_id=3MVG9jfQT7vUue.HarAmjlR0LN6LnjlyIqXYwNU5bSuFQPZ2teUTsnSyUlokH6s5wQ5raNXQA8amsBLZgcKwA
    ...  client_secret=96F2F68AC3A5DAF5B7FCF6BA22E726FE3D01DD2989387AB1E227F973B27B25EF
    ...  username=admin.integracao@bari.com.br.prod.qabari
    ...  password=P@ssIntegracao@3
    Set Test Variable  ${HEADERS}
    ${RESPOSTAREQUEST}     Post Request     newsession      services/oauth2/token
    ...     data=${PARAMS}
    ...     headers=${HEADERS}
    Log                          ${RESPOSTAREQUEST.text}
    ${json_value}  Set Variable  ${RESPOSTAREQUEST.json()}
    ${token}  Catenate  Bearer  ${json_value['access_token']}
    Set Test Variable  ${token}
    [Return]        ${token}

Consulta SF
    [Arguments]  ${token}  ${CPF}
    Abrir conexao   https://barigui--qabari.my.salesforce.com
    ${payload}   Create Dictionary    contract=string   type=string
    ${json}  Convert Dic to Json  ${payload}  
    ${HEADERS}  Create Dictionary
    ...     Authorization=${token}    
    ${RESPOSTA}     Get Request     newsession      services/apexrest/Bari/WS_Get_Account?individualRegistrationNumber=${CPF}  
    ...     headers=${HEADERS}
    Log                             ${RESPOSTA.text}
    Should Be Equal As Strings      ${RESPOSTA.status_code}    200

Integracao SF
    [Arguments]  ${guidLead}  ${email}  ${dtnasc}  ${date}  ${token}  ${expiryDate}  ${CPF}
    # ${nome}  Split String  ${name}  
    Abrir Conexao  https://barigui--qabari.my.salesforce.com
    ${json_string}=    catenate
    ...     { 
	...        "leadStatus": "active", 
	...        "leadID": "${guidLead}", 
	...        "onboardingStep": "${onboardingStep}",
	...        "customerData": {
	...            "Assets": 2.24,
	...            "AssetsRange" : "string",
	...            "BirthDate": "1990-01-01",
	...            "BirthPlaceCity" : "string",
	...            "BirthPlaceCountry" : "string",
	...            "BirthPlaceState" : "string",
	...            "CountryPhoneCode": "string",
	...            "DocumentBackPictureReference" : "string",
	...            "DocumentFrontPictureReference" : "string",
	...            "Email" : "${email}",
	...            "Gender" : "string",
	...            "Income" : 1200.00,
	...            "IncomeRange" : "string",
	...            "IndividualRegistrationNumber" : "${CPF}",
	...            "MaritalStatus" : "string",
	...            "MobilePhoneNumber" : "string",
	...            "MotherName" : "string",
	...            "FirstName" : "string",
	...            "LastName" : "string"
	...            },
	...    "commercialAddress": {
	...        "AddressLine1" : "string",
	...        "AddressLine2" : "string",
	...        "City" : "string",
	...        "Country" : "string",
	...        "Neighborhood" : "string",
	...        "NumberAddress" : "string",
	...        "PostalCode" : "string",
	...        "State" : "string"	
	...        },
	...        "document": {
	...        "ExpiryDate" : "2021-03-03",
	...        "Issuer" : "string",
	...        "IssueDate" : "${date}",
	...        "IssuingCountry" : "string",
	...        "IssuingState" : "string",
	...        "NumberDocument" : "string",
	...        "OriginatingCountry" : "string",
	...        "TypeDocument" : "string"	
	...        },
	...    "homeAddress": {
	...        "AddressLine1" : "string",
	...        "AddressLine2" : "string",
	...        "City" : "Maringá",
	...        "Country" : "string",
	...        "Neighborhood" : "string",
	...        "NumberAddress" : "string",
	...        "PostalCode" : "string",
	...        "State" : "Pr"	
	...        }
    ...    }    
    log  ${json_string}    
    ${HEADERS}  Create Dictionary
    ...     Content-Type=application/json
    ...     Authorization=${token}    
    ${RESPOSTA}     Put Request     newsession      services/apexrest/Bari/WS_Account
    ...     data=${json_string}
    ...     headers=${HEADERS}
    Log                             ${RESPOSTA.text}
    Should Be Equal As Strings      ${RESPOSTA.status_code}    200

setVariableLeadId
    [Arguments]  ${index}
    ${leadsId}  Set Variable  ${index}
    Set Test Variable  ${leadsId}

validaStatusMesa
    [Arguments]  ${status}  
    Log  ${status}
    ${valida}    Evaluate   "${status}"=="pending"
    Set Test Variable    ${valida}