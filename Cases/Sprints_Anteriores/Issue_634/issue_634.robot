*** Settings ***
Resource	${EXECDIR}/Resources/main.resource
Suite Setup   Abrir conexao  https://api.qa.bancobari.com.br

*** Test Cases ***

Scenario 01: Validação de CPF 
        Dados básicos
        CPF inválido 

Scenario 02: Validação de Email 
        Dados básicos
        Email inválido 

Scenario 03: Validação de SMS 
        Dados básicos
        SMS inválido 

Scenario 04: Validação de documento 
        Dados básicos
        Documento inválido 

Scenario 05: Validação de selfie
        Dados básicos
        Selfie inválida 

Scenario 06: Validação de idade
        Dados básicos
        Idade inválida 

  
***Keywords***

Dados básicos
    
    #gera guid
    ${guidLead}  Gera Guid
    Set Global Variable  ${guidLead}
    #gera guid   
    ${guidDevice}  Gera Guid
    Set Global Variable  ${guidDevice}
    
    #gerando nome
     ${name}  FakerLibrary.name
     Set Global Variable  ${name}
     ${contains}=  Evaluate   "." in """${name}"""    
     Run Keyword If  ${contains}  Split Name  ${name}
     ...  ELSE  No Split Name

CPF inválido

    ${CPF}  Set Variable  53541522263
    01_GET_/v1/Leads/contract?types=InformationPrivacy,SCRAcceptance
    02_PUT_{{url}}/v1/Leads/{{lead_id}}FALSE  ${guidLead}  ${name}  ${CPF}

Email inválido

    ${email}  Set Variable  tobias@gmail
    01_GET_/v1/Leads/contract?types=InformationPrivacy,SCRAcceptance
    05_PUT_{{url}}/v1/Leads/{{lead_id}}/EmailFALSE  ${guidLead}  ${email}

SMS inválido

    ${telefone}  Set Variable  41999999999
    01_GET_/v1/Leads/contract?types=InformationPrivacy,SCRAcceptance
    07_PUT_{{url}}/v1/Leads/{{lead_id}}/mobilePhoneNumber  ${guidLead}  ${telefone}

Documento inválido

    01_GET_/v1/Leads/contract?types=InformationPrivacy,SCRAcceptance    
    17_PUT_{{url}}/v1/Leads/{{lead_id}}/documentPictureFALSE  ${guidLead}

Selfie inválida 
    
    01_GET_/v1/Leads/contract?types=InformationPrivacy,SCRAcceptance
    18_PUT_{{url}}/v1/Leads/{{lead_id}}/selfieFALSE  ${guidLead}

Idade inválida

    ${dtnasc}  Set Variable  2008-09-02
    01_GET_/v1/Leads/contract?types=InformationPrivacy,SCRAcceptance
    03_PUT_{{url}}/v1/Leads/{{lead_id}}/BirthDateFALSE  ${guidLead}  ${dtnasc}

Split Name
    [Arguments]  ${name}
    ${nameList}  Split String  ${name}  .
    ${NOME}  Set Variable  ${nameList[1]}
    Set Test Variable  ${NOME}

No Split Name
    ${NOME}  Set Variable  ${name}
    Set Test Variable  ${NOME}

