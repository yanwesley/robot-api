*** Settings ***
Documentation
...  As usuário do Backoffice de cadastro de clientes do Banco Bari,
...  Want saber faz parte da lista de BC Correio,
...  So atender processos regulatórios do Banco Central, impedindo ou não,
...  a abertura de conta de pessoas na white list.
Default Tags	Sprint27
Resource	${EXECDIR}/Resources/main.resource

*** Test Case ***
Consultando os dados dos usuario que nao esta na lista BC Correio
	[Tags]	backend	regression	automation
	Given consultar_usuario_na_lista_de_bc_correio
	When nao_tem_o_nome_na_lista_de_bc_correio
	Then ocorre_o_retorno_da_resposta_como_negativo
	And gravo_o_status_da_resposta_de_bc_correio

Consultando os dados dos usuario que esta na lista BC Correio
	[Tags]	backend	regression	automation
	Given consultar_usuario_na_lista_de_bc_correio
	When tem_o_nome_na_lista_bc_correio
	Then ocorre_o_retorno_da_resposta_como_positiva
	And gravo_o_status_da_resposta_de_bc_correio

*** Keywords ***

consultar_usuario_na_lista_de_bc_correio
	${item}                Consulta Dynamo Attribute              Barigui.Services.Customer_Customers    Compliance
    log                    ${item}

nao_tem_o_nome_na_lista_de_bc_correio
	log  Por volume de massas não é possível buscar essa informação específica.

ocorre_o_retorno_da_resposta_como_negativo
	log  Como não é algo impeditivo pra o cadastro do cliente nos abstemos de encontrar cliente com este cenário.

gravo_o_status_da_resposta_de_bc_correio
	:FOR    ${COUNT}    IN RANGE    0    5
    \  Valida compliance  ${item}  ${COUNT}  0  BCCORREIOS

tem_o_nome_na_lista_bc_correio
	log  Por volume de massas não é possível buscar essa informação específica.

ocorre_o_retorno_da_resposta_como_positiva
	log  Como não é algo impeditivo pra o cadastro do cliente nos abstemos de encontrar cliente com este cenário.