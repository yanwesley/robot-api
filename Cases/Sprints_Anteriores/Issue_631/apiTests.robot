*** Settings ***

Resource	${EXECDIR}/Resources/main.resource


*** Test Cases ***

Validação do retorno do FraudScore
    Consulta do Fraud Score


*** Keywords ***
Consulta do Fraud Score
    ${retorno}  Consulta Dynamo  Barigui.Services.Onboarding_Leads  Email  yan_wesley@hotmail.com
    log  ${retorno[0]['FraudScoreResponse']}
    Should Be Equal  ${retorno[0]['FraudScoreResponse']['status']}  approved
    Should Not Be Equal  ${retorno[0]['FraudScoreResponse']['score']}  ${EMPTY}

