*** Settings ***
Documentation
...  As usuário do backoffice de abertura de contas do Banco Bari,
...  Want fazer consultas relacionadas a exposição e processos jurídicos dos novos clientes,
...  So atender processos regulatórios do Banco Central e fazer monitoramento contra lavagem de dinheiro.
Default Tags	Sprint27
Resource	${EXECDIR}/Resources/main.resource

*** Test Case ***

Usuario com PEP positivo para ele mesmo
	[Tags]	backend	regression	automation
	Given consultar_para_identificacao_de_usuario_como_pep
	When tem_situacao_de_pep_positivo_para_ele_mesmo
	Then grava_o_status_no_cadastro_como_pep_com_os_campos_relacionados

Usuario com PEP positivo de relacionamento proximo
	[Tags]	backend	regression	automation
	Given consultar_para_identificacao_de_usuario_como_pep
	When tem_situacao_de_pep_positivo_de_relacionamento_proximo
	Then grava_o_status_no_cadastro_como_pep_com_os_campos_relacionados

Usuario com PEP negativo
	[Tags]	backend	regression	automation
	Given consultar_para_identificacao_de_usuario_como_pep
	When tem_situacao_de_pep_negativo
	Then grava_o_status_no_cadastro_como_pep_com_os_campos_relacionados

Usuario que possui Midia negativa
	[Tags]	backend	regression	automation
	Given consultar_para_identificacao_de_usuario_com_midia_negativa
	When situacao_de_possuir_midia_negativa
	Then grava_no_status_do_cadastro_a_informacao_de_midia_negativa

Usuario que nao possui Midia negativa
	[Tags]	backend	regression	automation
	Given consultar_para_identificacao_de_usuario_com_midia_negativa
	When situacao_de_nao_possuir_midia_negativa
	Then grava_no_status_do_cadastro_a_informacao_de_midia_negativa

*** Keywords ***

consultar_para_identificacao_de_usuario_como_pep
    ${item}                Consulta Dynamo Attribute              Barigui.Services.Customer_Customers    Compliance
    Set Test Variable  ${item}
    log                    ${item}
tem_situacao_de_pep_positivo_para_ele_mesmo
    log  Por volume de massas não é possível buscar essa informação específica.Como não é algo impeditivo pra o cadastro do cliente nos abstemos de encontrar cliente com este cenário. 
grava_o_status_no_cadastro_como_pep_com_os_campos_relacionados
    :FOR    ${COUNT}    IN RANGE    0    5
    \  Valida compliance  ${item}  ${COUNT}  3  OFACANDPEP

tem_situacao_de_pep_positivo_de_relacionamento_proximo
	log  Por volume de massas não é possível buscar essa informação específica.Como não é algo impeditivo pra o cadastro do cliente nos abstemos de encontrar cliente com este cenário.

tem_situacao_de_pep_negativo
	log  Por volume de massas não é possível buscar essa informação específica.Como não é algo impeditivo pra o cadastro do cliente nos abstemos de encontrar cliente com este cenário.

consultar_para_identificacao_de_usuario_com_midia_negativa
 	${item}                Consulta Dynamo Attribute              Barigui.Services.Customer_Customers    Compliance
    Set Test Variable  ${item}
    log                ${item}
situacao_de_possuir_midia_negativa
	log  Por volume de massas não é possível buscar essa informação específica.Como não é algo impeditivo pra o cadastro do cliente nos abstemos de encontrar cliente com este cenário.

grava_no_status_do_cadastro_a_informacao_de_midia_negativa
	:FOR    ${COUNT}    IN RANGE    0    5
    \  Valida compliance  ${item}  ${COUNT}  2  MEDIA

situacao_de_nao_possuir_midia_negativa
	log  Por volume de massas não é possível buscar essa informação específica.Como não é algo impeditivo pra o cadastro do cliente nos abstemos de encontrar cliente com este cenário.













Valida MEDIA
    ${item}                Consulta Dynamo Attribute              Barigui.Services.Customer_Customers    Compliance
    log                    ${item}
    :FOR    ${COUNT}    IN RANGE    0    5
    \  Valida compliance  ${item}  ${COUNT}  2  MEDIA

Valida PEP
    ${item}                Consulta Dynamo Attribute              Barigui.Services.Customer_Customers    Compliance
    log                    ${item}
    :FOR    ${COUNT}    IN RANGE    0    5
    \  Valida compliance  ${item}  ${COUNT}  3  OFACANDPEP