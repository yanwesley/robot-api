*** Settings ***
Resource	${EXECDIR}/Resources/main.resource
Suite Setup   Abrir conexao  https://api.qa.bancobari.com.br

*** Test Cases ***

Validar Integração do final do Onboarding no Sales Force
    Geração do token para o SF
    Consulta na base do SF
    Validar a persistencia dos dados

*** Keywords ***

Geração do token para o SF
    Abrir conexao   https://test.salesforce.com
    ${HEADERS}  Create Dictionary
    ...  Content-Type=application/x-www-form-urlencoded
    ${PARAMS}  Create Dictionary
    ...  grant_type=password
    ...  client_id=3MVG9Vik22TUgUphKhs9dOlQTssExvUo53Ez3Jl1IAYrDcy5Ete.gED_B5Or1jjCyQSv_hKXViKgmnTym9R1T
    ...  client_secret=FA050230A427D30689D08F4DA22F9ABAFCE5AF0602CFC65A5D940DF6D7D5A4F4
    ...  username=admin.integracao@bari.com.br.qabari
    ...  password=P@ssIntegracao@5
    Set Test Variable  ${HEADERS}
    ${RESPOSTAREQUEST}     Post Request     newsession      services/oauth2/token
    ...     data=${PARAMS}
    ...     headers=${HEADERS}
    Log                          ${RESPOSTAREQUEST.text}
    ${json_value}  Set Variable  ${RESPOSTAREQUEST.json()}
    ${token}  Catenate  Bearer  ${json_value['access_token']}
    Set Test Variable  ${token}
    #[Return]        ${token}


Consulta na base do SF
    #[Arguments]  ${token}
    Abrir conexao   https://barigui--qabari.my.salesforce.com
    ${payload}   Create Dictionary    contract=string   type=string
    ${json}  Convert Dic to Json  ${payload}  
    ${HEADERS}  Create Dictionary
    ...     Authorization=${token}    
    ${RESPOSTA}     Get Request     newsession      services/apexrest/Bari/WS_Get_Account?individualRegistrationNumber=06537262950
    ...     headers=${HEADERS}
    Log                             ${RESPOSTA.text}
    Should Be Equal As Strings      ${RESPOSTA.status_code}    200
    ${consulta}  Set Variable  ${RESPOSTA.json()}
    Set Test Variable  ${consulta}


Validar a persistencia dos dados
    Log  ${consulta}
    Should Be Equal  ${consulta['body'][0]['onboardingStep']}  finished
    Log  O resultado esperado é finished
    Log  ${consulta['body'][0]['onboardingStep']}
    
    ${consulta2}                     Consulta Dynamo    Barigui.Services.Onboarding_Leads    Email    yan_wesley@hotmail.com 
    
    Should Be Equal    ${consulta['body'][0]['leadID']}  ${consulta2[0][0]['Id']}
    Log  Os Ids devem ser iguais
    Log  ${consulta2[0][0]['Id']} 
    Log  ${consulta['body'][0]['leadID']}
    


    
    