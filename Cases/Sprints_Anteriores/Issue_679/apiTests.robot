*** Settings ***
Resource	${EXECDIR}/Resources/main.resource
Suite Setup   Abrir conexao  https://api.qa.bancobari.com.br

*** Test Cases ***

Validar a selfie na troca de device
    bypass na API selfie

*** Keywords ***

bypass na API selfie
    ${token}  Gera Token  06537262950  123Abc
    ${id}  Set Variable  06537262950
    ${HEADERS1}  Create Dictionary   accept=application/json  authorization=${token}
    ${RESPOSTAREQUEST1}  Put Request     newSession      mfa/v1/webauthn/recovery/selfie/${id}
    ...                                headers=${HEADERS1} 
    Should Be Equal As Strings    ${RESPOSTAREQUEST1.status_code}    202
    Log            ${RESPOSTAREQUEST1.text}
 