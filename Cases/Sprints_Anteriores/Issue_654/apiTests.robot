*** Settings ***
Documentation
...  
Default Tags   Sprint33
Resource	   ${EXECDIR}/Resources/main.resource

*** Test Cases ***

Teste
    Validar chamadas das apis

*** Keywords ***

Validar chamadas das apis

    ${HEADERS1}  Create Dictionary   
    ${RESPOSTAREQUEST1}  Put Request     newSession      mfa/v1/webauthn/recovery/${mode}/${id}
    ...                                headers=${HEADERS1} 
    Should Be Equal As Strings    ${RESPOSTAREQUEST1.status_code}    200
    Log            ${RESPOSTAREQUEST1.text}


    ${HEADERS2}  Create Dictionary   
    ${RESPOSTAREQUEST2}  Put Request     newSession      mfa/v1/webauthn/recovery/${mode}/${id}/code
    ...                                headers=${HEADERS2} 
    Should Be Equal As Strings    ${RESPOSTAREQUEST2.status_code}    200
    Log            ${RESPOSTAREQUEST2.text}

    ${HEADERS3}  Create Dictionary   
    ${RESPOSTAREQUEST3}  Put Request     newSession      mfa/v1/webauthn/trustedEndpoint/recovery/${id}
    ...                                headers=${HEADERS3} 
    Should Be Equal As Strings    ${RESPOSTAREQUEST3.status_code}    200
    Log            ${RESPOSTAREQUEST3.text}

    ${HEADERS4}  Create Dictionary   
    ${RESPOSTAREQUEST4}  Get Request     newSession      mfa/v1/webauthn/makecredential/recovery/${id}
    ...                                headers=${HEADERS4} 
    Should Be Equal As Strings    ${RESPOSTAREQUEST4.status_code}    200
    Log            ${RESPOSTAREQUEST4.text}

    ${HEADERS5}  Create Dictionary   
    ${RESPOSTAREQUEST5}  Put Request     newSession      mfa/v1/webauthn/makecredential/recovery/${id}
    ...                                headers=${HEADERS5} 
    Should Be Equal As Strings    ${RESPOSTAREQUEST5.status_code}    200
    Log            ${RESPOSTAREQUEST5.text}

    ${HEADERS6}  Create Dictionary   
    ${RESPOSTAREQUEST6}  Put Request     newSession      mfa/v1/webauthn/selfie/recovery/${id}
    ...                                headers=${HEADERS6} 
    Should Be Equal As Strings    ${RESPOSTAREQUEST6.status_code}    200
    Log            ${RESPOSTAREQUEST6.text}