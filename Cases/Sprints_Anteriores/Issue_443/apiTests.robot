*** Settings ***
Documentation
...  As usuário Banco Bari,
...  Want garantir qua ao realizar o fluxo de Onboarding, a gravação do IP em todas as chamadas de api
...  So gerar informações mais robustas para o backend
Resource	${EXECDIR}/Resources/main.resource

*** Test Cases ***

Validar a geracao do IP em todas as chamadas do Onboarding
	Given usuario_realizou_o_fluxo_de_onboarding
	When verificar_os_dados_das_chamadas_das_apis
	Then devera_constar_a_presenca_do_ip_em_todas

*** Keywords ***

Given usuario_realizou_o_fluxo_de_onboarding
    Log  teste
When verificar_os_dados_das_chamadas_das_apis

    ${consulta1}                     Consulta Dynamo    Barigui.Services.Onboarding_Leads    Email    fabiane.rocha@baritecnologia.com.br
    Log    ${consulta1}
    Set Test Variable  ${Id}         ${consulta1[0][0]['Id']}
    #Set Test Variable  ${Id}         ${consulta1[0]['Id']}

    ${consulta2}  Consulta Dynamo  Barigui.Services.Onboarding_LeadEvents  Id  ${Id}
    Log  ${consulta2}
    Set Test Variable  ${consulta2}
    ${countIp}  Set Variable  0
    Set Global Variable  ${countIp}

    : FOR    ${INDEX}    IN RANGE    0    100
    \    log  ${consulta2[${INDEX}]}
    \    ${data}  Set Variable  ${consulta2[${INDEX}]}
    \    ${stringCompare}  Convert To String  ${consulta2[${INDEX}]}
    \    ${payloadVazio}  Set Variable   []
    \    ${complexPayload}  Set Variable   [{
    \    ${simplePayload}  Set Variable   {
    \    ${equal}=  evaluate  ${stringCompare} == ${payloadVazio}
    \    Run Keyword If  ${equal}  log  Payload Vazio!
    \    ${splitResult}  Split String  ${stringCompare}  '
    \    ${contains}=  evaluate  "${splitResult[0]}" == "${complexPayload}"
    \    Run Keyword If  ${contains}  Payload Validation with []  ${data}
    \    ${contains}=  evaluate  "${simplePayload}" == "${splitResult[0]}"
    \    Run Keyword If  ${contains}  Payload Validation  ${data}

    log  ${countIp}



Then devera_constar_a_presenca_do_ip_em_todas

    Should Be Equal As Numbers    ${countIp}    24  

Payload Validation
    [Arguments]  ${data}
    log  Payload sem [] !
    log  ${data['Payload']}
    ${stringData}  Convert To String    ${data['Payload']}
    ${contains}=  evaluate  "IpAddress" in """${stringData}"""
    Run Keyword If  ${contains}  Capture IpAddress  ${data}

Payload Validation with []
    [Arguments]  ${data}
    log  Payload com [] !
    log  ${data[0]['Payload']}
    ${stringData}  Convert To String    ${data[0]['Payload']}
    ${contains}=  evaluate  "IpAddress" in """${stringData}"""
    Run Keyword If  ${contains}  Capture IpAddress with []  ${data}

Capture IpAddress with []
    [Arguments]  ${data}
    ${json}  Help Converter  ${data[0]['Payload']}
    log  ${json['Id']['IpAddress']}
    ${countIp}=  Evaluate  ${countIp}+1
    Set Global Variable  ${countIp}

Capture IpAddress
    [Arguments]  ${data}
    ${json}  Help Converter  ${data['Payload']}
    log  ${json['Id']['IpAddress']}
    ${countIp}=  Evaluate  ${countIp}+1
    Set Global Variable  ${countIp}