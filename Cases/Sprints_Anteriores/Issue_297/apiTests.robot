*** Settings ***
Documentation
...  As usuário Backoffice de cadastro de clientes do Banco Bari,
...  Want faz parte da lista do Ibama,
...  So atender processos regulatórios do Banco Central impedindo ou não,
...  a abertura de conta pelos usuários presentes na lista.
Default Tags	Sprint27
Resource	${EXECDIR}/Resources/main.resource

*** Test Case ***

Consultando os dados do usuario que esta na lista IBAMA
	[Tags]	backend	regression	automation
	Given possivel_consultar_usuario_na_lista_ibama
	When esteja_na_lista_do_ibama
	Then ocorre_o_retorno_da_resposta_como_positivo
	And grava_o_status_da_resposta_do_ibama_na_tabela

Consultando os dados do usuario que nao esta na lista IBAMA
	[Tags]	backend	regression	automation
	Given possivel_consultar_usuario_na_lista_ibama
	When nao_esteja_na_lista_do_ibama
	Then ocorre_o_retorno_da_resposta_como_negativo
	And grava_o_status_da_resposta_do_ibama_na_tabela

Valida IBAMA
    ${item}                Consulta Dynamo Attribute              Barigui.Services.Customer_Customers    Compliance
    log                    ${item}
    :FOR    ${COUNT}    IN RANGE    0    5
    \  Valida compliance  ${item}  ${COUNT}  1  IBAMA