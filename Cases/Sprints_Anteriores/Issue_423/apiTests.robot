*** Settings ***
Documentation
...            As usuário lead que realizou o fluxo Onboarding de abertura de conta pagamento,
...            Want que sua selfie seja validada,
...            So ter sua conta pagamento aberta para uso.
Default Tags   Sprint30
Resource	   ${EXECDIR}/Resources/main.resource

*** Test Cases ***

Validacao da selfie quando registro e atualizado
	[Tags]	automation	backend
	Given usuario_lead_que_concluiu_o_fluxo_de_onboarding
	But obteve_liveness_com_status_divergente
	When acesso_digital_enviar_a_resposta_da_mesa_manual  3
	And o_status_foi_atualizado
	Then registrado_sera_atualizado_na_base

Validacao da selfie quando registro nao sofre alteracao
	[Tags]	automation	backend
	Given usuario_lead_que_concluiu_o_fluxo_de_onboarding
	But obteve_liveness_com_status_divergente
	When acesso_digital_enviar_a_resposta_da_mesa_manual  2
	But o_status_nao_foi_atualizado
	Then nao_sera_registrado_o_dado_na_base

Validacao da selfie quando chamada tem acesso negado
	[Tags]	automation	backend
	Given usuario_lead_que_concluiu_o_fluxo_de_onboarding
	But obteve_liveness_com_status_divergente
	When acesso_digital_enviar_a_resposta_da_mesa_manual_com os_dados_da_chamada_incorretos
	Then status_e_de_acesso_negado

*** Keywords ***

#Case 01:
usuario_lead_que_concluiu_o_fluxo_de_onboarding
    Log  O usuário será resultado de uma consulta realizada na tabela Barigui.Services.Onboarding_LivenessDivergentProcesses

obteve_liveness_com_status_divergente
    ${livenessId}   Consulta Dynamo Attribute    Barigui.Services.Onboarding_LivenessDivergentProcesses     Id
    ${result}       Set Variable                 ${livenessId[0]['Id']}
    ${lead}         Set Variable                 ${livenessId[0]['LeadId']}
    ${leadid}  Convert to String  ${lead}
    Set Test Variable  ${leadid}
    ${guidId}=      Convert To String            ${result}
    Set Test Variable  ${guidId}

acesso_digital_enviar_a_resposta_da_mesa_manual
    [Arguments]  ${status}
    ${guid}         Gera Guid 
    Set Test Variable   ${guid}
    ${DATA}         Create Dictionary            id=${guidId}  status=${status}
    ${PARAMS}       Create Dictionary            eventDate=2020-03-18T12:12.000Z  event=create_process   data=${DATA}
    ${HEADERS}      Create Dictionary            accept=application/json  User-Agent=${guid}  Content-Type=application/json-patch+json   X-API-Key=12f64981109a46e3
    Abrir conexao   https://api.qa.bancobari.com.br
    ${RESPOSTAREQUEST}     Post Request          new session         onboarding/v1/endpoints/livenessProcess
    ...                                          data=${PARAMS}
    ...                                          headers=${HEADERS}
    Set Test Variable  ${RESPOSTAREQUEST} 

o_status_foi_atualizado
    log  ${RESPOSTAREQUEST.text} 
    ${STATUSCODE_DESEJADO}  Set Variable  202
    Should Be Equal As Strings    ${RESPOSTAREQUEST.status_code}    ${STATUSCODE_DESEJADO}

registrado_sera_atualizado_na_base
    ${resultadoRegistrado}   Consulta Dynamo    Barigui.Services.Onboarding_Leads     Id   ${leadid}    
    Log  ${resultadoRegistrado[0]['LivenessResult']['LivenessStatus']}

#Case:02
o_status_nao_foi_atualizado
    Log  ${RESPOSTAREQUEST.text}
    ${STATUSCODE_DESEJADO}   Set Variable   200
    Should Be Equal As Strings    ${RESPOSTAREQUEST.status_code}    ${STATUSCODE_DESEJADO}

nao_sera_registrado_o_dado_na_base
    ${resultadoRegistrado}   Consulta Dynamo     Barigui.Services.Onboarding_Leads     Id     ${leadid}  
    Log  ${resultadoRegistrado[0]['LivenessResult']['LivenessStatus']}

#Case 03:
acesso_digital_enviar_a_resposta_da_mesa_manual_com os_dados_da_chamada_incorretos
    ${guid}         Gera Guid 
    Set Test Variable   ${guid}
    ${DATA}         Create Dictionary            id=${guidId}  status=0
    ${PARAMS}       Create Dictionary            eventDate=2020-03-18T12:12.000Z  event=create_process   data=${DATA}
    ${HEADERS}      Create Dictionary            accept=application/json  User-Agent=${guid}  Content-Type=application/json-patch+json
    Abrir conexao   https://api.qa.bancobari.com.br
    ${RESPOSTAREQUEST}     Post Request          new session         onboarding/v1/endpoints/livenessProcess
    ...                                          data=${PARAMS}
    ...                                          headers=${HEADERS}
    Set Test Variable  ${RESPOSTAREQUEST} 

status_e_de_acesso_negado
    Log  ${RESPOSTAREQUEST.text}
    ${STATUSCODE_DESEJADO}   Set Variable   401
    Should Be Equal As Strings    ${RESPOSTAREQUEST.status_code}    ${STATUSCODE_DESEJADO}





