*** Settings ***
Documentation
...  As usuário do aplicativo Banco Bari,
...  Want ,
...  So .
Resource	${EXECDIR}/Resources/main.resource
Suite Setup   Abrir conexao  https://api.qa.bancobari.com.br

*** Test Cases ***

Aceite e leitura dos 3 tres documentos atualizados
	[Tags]	backend  automation
	Given usuario_lead_que_concluiu_o_fluxo_de_onboarding_criando_o_customer
	When verifica_os_contratos_pendentes_nao_aceitos_pelo_usuario
	Then devera_ser_registrado_os_aceites_dos_contratos
    And atualizacao_das_informacoes_dos_contratos_aceito



*** Test Cases ***

Testes Costumer
    
    #gera guid
    ${guidLead}  Gera Guid
    ${guidLead}  Set Variable  58b7693b-4ec8-46ec-ad63-325d77f58c38
    Set Test Variable  ${guidLead}
    #gera guid   
    ${guidDevice}  Gera Guid
    ${guidDevice}  Set Variable  a8b2db73-fbd9-4ac5-bc35-9f9452f3375f
    Set Test Variable  ${guidDevice}

    
    ${CPF}  Set Variable  02183488190
    ${NOME}  Set Variable  William Filho
    ${dtnasc}  Set Variable  1989-05-31
    ${email}  Set Variable  william.filho18@primecontrol.com.br
    ${telefone}  Set Variable  41991942233 
    ${gender}  Set Variable  MALE  
    ${motherName}  Set Variable  Luzia de Fatima Macedo Arantes
    ${State}  Set Variable  Distrito Federal
    ${City}  Set Variable  Brasília
    ${maritalStatus}  Set Variable  MARRIED 
    ${spouseName}  Set Variable  Niedja Kaliene Maciel de Sousa
    ${postalCode}  Set Variable  81830290
    ${addressLine1}  Set Variable  Rua João Batista Zagonel Passos
    ${number}  Set Variable  881
    ${neighborhood}  Set Variable  Xaxim
    ${city}  Set Variable  Curitiba
    ${state}  Set Variable  PR


    01_GET_/v1/Leads/contract?types=InformationPrivacy,SCRAcceptance
    02_PUT_{{url}}/v1/Leads/{{lead_id}}  ${guidLead}  ${NOME}  ${CPF}
    03_PUT_{{url}}/v1/Leads/{{lead_id}}/BirthDate  ${guidLead}  ${dtnasc}
    04_PUT_{{url}}/v1/Leads/{{lead_id}}/geolocation  ${guidLead}
    05_PUT_{{url}}/v1/Leads/{{lead_id}}/Email  ${guidLead}  ${email}
    07_PUT_{{url}}/v1/Leads/{{lead_id}}/mobilePhoneNumber  ${guidLead}  ${telefone}
    ${token}  Gera Token SF
    Integracao SF  ${CPF}  ${NOME}  ${email}  ${telefone}  ${dtnasc}  ${token}    
    Consulta SF  ${token}  ${CPF}    
    09_PUT_{{url}}/v1/Leads/{{lead_id}}/personalInfo  ${guidLead}  ${gender}  ${motherName}  ${State}  ${City}  ${maritalStatus}  ${spouseName}
    10_PUT_{{url}}/v1/Leads/{{lead_id}}/homeAddress  ${guidLead}  ${postalCode}  ${addressLine1}  ${number}  ${neighborhood}  ${city}  ${state}
    11_PUT_{{url}}/v1/Leads/{{lead_id}}/commercialAddress  ${guidLead}
    12_GET_{{url}}/v1/Leads/professions
    13_PUT_{{url}}/v1/Leads/{{lead_id}}/profession  ${guidLead}    
    14_PUT_{{url}}/v1/Leads/{{lead_id}}/income  ${guidLead}
    15_GET_{{url}}/v1/Leads/assetsrange     
    16_PUT_{{url}}/v1/Leads/{{lead_id}}/assets  ${guidLead}
    17_PUT_{{url}}/v1/Leads/{{lead_id}}/documentPicture  ${guidLead}
    18_PUT_{{url}}/v1/Leads/{{lead_id}}/selfie  ${guidLead}
    ${CNHDados}  19_GET_{{url}}/v1/Leads/{{lead_id}}/documentExtraction  ${guidLead}
    ${type}  Set Variable  CNH
    ${issuer}  Set Variable  Órgão de Transito
    ${issuingCountry}  Set Variable  BRA 
    ${issuingState}  Set Variable  DF
    ${number}  Set Variable  04229282078
    ${originatingCountry}  Set Variable  BRA 
    ${expiryDate}  Set Variable  2020-11-09T12:58:21.160Z  
    ${date}  Set Variable  2015-11-19
    ${issueDate}  catenate  SEPARATOR=  ${date}  T12:58:21.160Z
    20_PUT_{{url}}/v1/Leads/{{lead_id}}/additionalDocumentData  ${guidLead}  ${type}  ${issuer}  ${issuingCountry}  ${issuingState}  ${number}  ${originatingCountry}  ${expiryDate}  ${issueDate}  
    21_PUT_{{url}}/v1/Leads/{{lead_id}}/PoliticalExposition  ${guidLead}
    22_PUT_{{url}}/v1/Leads/{{lead_id}}/USPerson  ${guidLead}
    ${contracts}  23_GET_{{url}}/v1/Leads/contract?types=AccountOpening
    24_PUT_{{url}}/v1/Leads/{{lead_id}}/TermsAcceptance  ${guidLead}  ${contracts[0]['url']}  ${contracts[0]['type']}

    

***Keywords***

Split Name
    [Arguments]  ${name}
    ${nameList}  Split String  ${name}  .
    ${NOME}  Set Variable  ${nameList[1]}
    Set Test Variable  ${NOME}

No Split Name
    ${NOME}  Set Variable  ${name}
    Set Test Variable  ${NOME}

Gera Token SF
    Abrir conexao   https://test.salesforce.com
    ${HEADERS}  Create Dictionary
    ...  Content-Type=application/x-www-form-urlencoded
    ${PARAMS}  Create Dictionary
    ...  grant_type=password
    ...  client_id=3MVG9PE4xB9wtoY.8zAdxB8mUc4yr9iK.d5uto1BHKkv_c3msG8UZPaAe0A.VtqyzpCS28BEeLTnGSGxEizky
    ...  client_secret=B070BFCF61A1A7D40F0D078238187B282D25AC71A514E917BD0E22393B8FBE37
    ...  username=admin.integracao@bari.com.br.dev
    ...  password=P@ssIntegracao@3
    Set Test Variable  ${HEADERS}
    ${RESPOSTAREQUEST}     Post Request     newsession      services/oauth2/token
    ...     data=${PARAMS}
    ...     headers=${HEADERS}
    Log                          ${RESPOSTAREQUEST.text}
    ${json_value}  Set Variable  ${RESPOSTAREQUEST.json()}
    ${token}  Catenate  Bearer  ${json_value['access_token']}
    Set Test Variable  ${token}
    [Return]        ${token}

Consulta SF
    [Arguments]  ${token}  ${CPF}
    Abrir conexao   https://barigui--devbari.my.salesforce.com
    ${payload}   Create Dictionary    contract=string   type=string
    ${json}  Convert Dic to Json  ${payload}  
    ${HEADERS}  Create Dictionary
    ...     Authorization=${token}    
    ${RESPOSTA}     Get Request     newsession      services/apexrest/Bari/WS_Get_Account?individualRegistrationNumber=${CPF}  
    ...     headers=${HEADERS}
    Log                             ${RESPOSTA.text}
    Should Be Equal As Strings      ${RESPOSTA.status_code}    200

Integracao SF
    [Arguments]  ${CPF}  ${name}  ${email}  ${telefone}  ${dtnasc}  ${token}
    ${nome}  Split String  ${name}  
    Abrir Conexao  https://barigui--devbari.my.salesforce.com
    ${json_string}=    catenate
    ...     {
    ...         "email": "${email}",
    ...         "firstName": "${nome[0]}",
    ...         "lastName": "${nome[1]}",
    ...         "mobilePhoneNumber": "${telefone}",
    ...         "individualRegistrationNumber": "${CPF}",
    ...         "birthDate": "${dtnasc}",
    ...         "leadStatus": "new",
    ...         "leadID": "${guidLead}",
    ...         "onboardingStep": "started"
    ...     }
    
    log  ${json_string}    
    ${HEADERS}  Create Dictionary
    ...     Content-Type=application/json
    ...     Authorization=${token}    
    ${RESPOSTA}     Put Request     newsession      services/apexrest/Bari/WS_Account
    ...     data=${json_string}
    ...     headers=${HEADERS}
    Log                             ${RESPOSTA.text}
    Should Be Equal As Strings      ${RESPOSTA.status_code}    200

