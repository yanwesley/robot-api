*** Settings ***
Documentation
...    As usuário lead com conta aprovada,
...    Want retido por não ser Whitelist,
...    So passar pelo fluxo de ativação da conta pagamento.
Default Tags	automacao	sprint29
Resource	${EXECDIR}/Resources/main.resource
Suite Setup   Abrir conexao  https://api.stage.bancobari.com.br
*** Test Cases ***

Verificacao e consulta dos CPF's não vinculados vinculo na whitelist
    [Template]      Validar teste 1 
    07337619928


Verificação da criação do evento Custumer
    [Template]       Validar teste 2
    02183488190      testesdowilliam@bol.com.br



*** Variable ***
${Event['0']}            Barigui.Services.Onboarding.Events.ManualApproval
${Event['1']}            Barigui.Services.Onboarding.Events.SelfieLiveness  
${Event['2']}            ExternalSelfieLivenessSendImage 
${Event['3']}            ExternalSelfieLivenessGetProcess
${Event['4']}            Barigui.Services.Onboarding.Events.FaceMatchRequest
${Event['5']}            ExternalFaceMatchSelfieDocument
${Event['6']}            Barigui.Services.Onboarding.Events.SerproRequest
${Event['7']}            Barigui.Services.Onboarding.Events.BigDataCorpRequest
${Event['8']}            Barigui.Services.Onboarding.Events.FraudScoreRequest
${Event['9']}            ExternalFraudScoreRequest
${Event['10']}           Barigui.Services.Onboarding.Events.OnboardingTerminated
${Event['11']}           Barigui.Services.Onboarding.Events.LeadApproved


*** Keywords *** 

Validar teste 1  
    [Arguments]  ${cpf} 
    Given usuario_realizou_o_fluxo_do_onboarding
    When foi_selecionado_para_realizar_ativacao_de_conta
    Then cpf_estara_vinculado_a_whitelist  ${cpf} 

Validar teste 2  
    [Arguments]  ${cpf}  ${Email} 
	Given usuario_realizou_o_fluxo_do_onboarding
	When teve_seu_status_para_ativacao_de_conta_como_aprovado  ${cpf}  ${Email}
	Then criado_o_evento_do_customer

Given usuario_realizou_o_fluxo_do_onboarding

    Log   Usuário que realizou o Onboarding

When foi_selecionado_para_realizar_ativacao_de_conta

    Log   Usuário foi selecionado pelo banco para a ativação da conta

Then cpf_estara_vinculado_a_whitelist 
    [Arguments]   ${cpf}
    ${consulta1}                     Consulta Dynamo    Barigui.Services.Onboarding_Whitelist     Id      ${cpf}
    Should Be Equal As Strings       ${consulta1[0]['Id']}    ${cpf}
    Log                              ${consulta1[0]['Id']}
    Log                              O cpf consta na WhiteList

When teve_seu_status_para_ativacao_de_conta_como_aprovado 
    [Arguments]   ${cpf}  ${Email}
  
    ${consulta1}                     Consulta Dynamo    Barigui.Services.Onboarding_Whitelist     Id      ${cpf}
    Should Be Equal As Strings       ${consulta1[0]['Id']}    ${cpf}
    Log                              ${consulta1[0]['Id']}


    ${consulta2}                     Consulta Dynamo    Barigui.Services.Onboarding_Leads    Email    ${Email}
    Log    ${consulta2}  
    Should Be Equal As Strings       ${consulta2[0]['OnboardingState']}    32005 
    Log                              ${consulta2[0]['OnboardingState']}
    Set Test Variable  ${Id}           ${consulta2[0]['Id']}

    ${token}  Gera Token Cadastro

    ${HEADERS1}  Create Dictionary   accept=application/json  User-Agent=da652d6a-244f-431f-8ba1-dbeb74f4cde1  Authorization=${token}
    ${RESPOSTAREQUEST1}  Put Request     newSession      onboarding/v1/leads/${cpf}/approved
    ...                                headers=${HEADERS1} 
    Should Be Equal As Strings    ${RESPOSTAREQUEST1.status_code}    202
    Log            ${RESPOSTAREQUEST1.text}
    

Then criado_o_evento_do_customer
    #${tamanho}  Get Length   @{Events}

    :FOR  ${index}  IN RANGE  0  11
    \  #${Evento}  Get From List   ${Events}  ${index}
    \  Log  ${Event['${index}']}
    \  ${consultaEvent}                Consulta Dynamo Two Attributes   Barigui.Services.Onboarding_LeadEvents  Id  ${Id}  EventType  ${Event['${index}']} 
    \  Log  ${consultaEvent}
    \  Should Be True  ${consultaEvent}!=@{EMPTY} 


    
    