*** Settings ***
Documentation
...    Como cliente que possuí cartão Bari
...    Eu quero acessar o portal
...    Para verificar as informações pertinentes ao meu cartão
Default Tags	automacao	sprintX

Resource	${EXECDIR}/Resources/main.resource
Suite Setup         Abrir navegador
Suite Teardown      Fechar navegador

*** Test Cases ***

Login Cliente Cadastrado (uid:734011ea88fb44a68565568286ea2dcb)
	[Template]	Login keyword
	Cadastrado	741.068.090-36	Estamos felizes em te ver!	Senha 	Continuar

Login Cliente Nao Cadastrato (uid:9d8dd3a5e0954df29f477e3ddb4285e8)
	[Template]	Login keyword
	Nao Cadastrado 	253.051.120-95	Olá! Ficamos felizes com o seu interesse, informe o seu e-mail que em breve entraremos em contato. 	E-mail	Enviar

Login CPF Invalido (uid:ce7224aca4ac4657ab6450725d0135e2)
	[Template]	Login keyword
	Inválido 	999.000.999-00	CPF inválido  None  None 

*** Keywords ***

Login keyword
	[Arguments]	${condicao}	${cpf}	${mensagem}	${campo}	${botao}
	Given que_eu_sou_cliente_p1	${condicao}
	When preencho_campo_com_p1	${cpf}
	Then o_sistema_exibe_a_mensagem_p1	${mensagem}
	And caso_nao_seja_invalido_exibe_o_campo_p1_e_o_botao_p2	${campo}	${botao}

