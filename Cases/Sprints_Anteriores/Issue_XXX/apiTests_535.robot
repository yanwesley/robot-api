*** Settings ***

Documentation
...  As usuário Banco Bari, 
...  Want ter garantia de segurança com duplo fator de autenticação, no fluxo de ativação de conta, 
...  So reduzir risco de fraudes garantindo o uso seguro do aplicativo.

Default Tags	automacao	sprint31
Resource	${EXECDIR}/Resources/main.resource
Suite Setup   Abrir conexao  https://api.qa.bancobari.com.br/

*** Test Cases ***

Scenario 01: Informação de liberação de acesso
        [Tags]	backend	regression	automation  
        Given usuário_lead_finalizou_o_fluxo_de_onboarding
        And está_na_white_list
        And o_usuário_habilita_a_funcão_2FA
        Then ele_estará_liberado_para_realizar_transações

*** Keywords *** 

usuário_lead_finalizou_o_fluxo_de_onboarding

#gera guid
    ${guidLead}  Gera Guid
    Set Test Variable  ${guidLead}
    #gera guid   
    ${guidDevice}  Gera Guid
    Set Test Variable  ${guidDevice}
    #gerando cpf
    ${CPFnum}  FakerLibrary.cpf
    ${CPF}  Remove String  ${CPFnum}  .  -
    Set Test Variable  ${CPF}
    #gerando nome
    ${name}  FakerLibrary.name
    Set Test Variable  ${name}
    ${contains}=  Evaluate   "." in """${name}"""    
    Run Keyword If  ${contains}  Split Name  ${name}
    ...  ELSE  No Split Name
    #gerando e-mail
    ${email}  FakerLibrary.email

    #gerando telefone
    ${phone}  FakerLibrary.Phone Number
    ${numbers}=    Generate Random String    length=7    chars=[NUMBERS]
    ${telefone}  catenate  SEPARATOR=  4199  ${numbers}

    #gerando data de nascimento
    ${dtnasc}  FakerLibrary.Date Of Birth  minimum_age=18  maximum_age=70
    
    01_GET_/v1/Leads/contract?types=InformationPrivacy,SCRAcceptance
    02_PUT_{{url}}/v1/Leads/{{lead_id}}  ${guidLead}  ${NOME}  ${CPF}
    03_PUT_{{url}}/v1/Leads/{{lead_id}}/BirthDate  ${guidLead}  ${dtnasc}
    04_PUT_{{url}}/v1/Leads/{{lead_id}}/geolocation  ${guidLead}
    05_PUT_{{url}}/v1/Leads/{{lead_id}}/Email  ${guidLead}  ${email}
    06_PUT_{{url}}/v1/Leads/{{lead_id}}/verifyEmail  ${guidLead}
    07_PUT_{{url}}/v1/Leads/{{lead_id}}/mobilePhoneNumber  ${guidLead}  ${telefone}
    08_PUT_{{url}}/v1/Leads/{{lead_id}}/verifyMobilePhoneNumber ${guidLead}
    09_PUT_{{url}}/v1/Leads/{{lead_id}}/personalInfo  ${guidLead}
    10_PUT_{{url}}/v1/Leads/{{lead_id}}/homeAddress  ${guidLead}
    11_PUT_{{url}}/v1/Leads/{{lead_id}}/commercialAddress  ${guidLead}
    13_PUT_{{url}}/v1/Leads/{{lead_id}}/profession  ${guidLead}
    14_PUT_{{url}}/v1/Leads/{{lead_id}}/income  ${guidLead}
    15_GET_{{url}}/v1/Leads/assetsrange  ${guidLead}
    16_PUT_{{url}}/v1/Leads/{{lead_id}}/assets  ${guidLead}
    17_PUT_{{url}}/v1/Leads/{{lead_id}}/documentPicture  ${guidLead}
    18_PUT_{{url}}/v1/Leads/{{lead_id}}/selfie  ${guidLead}
    21_PUT_{{url}}/v1/Leads/{{lead_id}}/PoliticalExposition  ${guidLead}
    22_PUT_{{url}}/v1/Leads/{{lead_id}}/USPerson  ${guidLead}
    23_GET_{{url}}/v1/Leads/contract?types=AccountOpening  ${guidLead}
    24_PUT_{{url}}/v1/Leads/{{lead_id}}/TermsAcceptance  ${guidLead}

And está_na_white_list
    [Arguments]   ${CPF}  ${email}
  
    ${consulta1}                     Consulta Dynamo    Barigui.Services.Onboarding_Whitelist     Id      ${CPF}
    Should Be Equal As Strings       ${consulta1[0]['Id']}    ${CPF}
    Log                              ${consulta1[0]['Id']}

o_usuário_habilita_a_funcão_2FA

    ${HEADERS1}  Create Dictionary   accept=application/json  User-Agent=8e10fa5a-7c67-4ac8-9a14-db8be2fd2e91  Content-Type=application/json-patch+json  Content-Type=text/plain
    ${PARAMS1}=  Catenate  
    ...     {
    ...         "channelType": "string",
    ...         "address": "string",
    ...         "demographic": {
    ...         "make": "string",
    ...         "model": "string",
    ...         "modelVersion": "string",
    ...         "timezone": "string",
    ...         "locale": "string",
    ...         "appVersion": "string",
    ...         "platform": "string",
    ...         "platformVersion": "string"
    ...         }
    ...     }   

    ${RESPOSTAREQUEST1}  Put Request     newSession      communication/v1/endpoints/${id}
    ...                                headers=${HEADERS1} 
    ...                                data=${PARAMS1}
    Should Be Equal As Strings    ${RESPOSTAREQUEST1.status_code}    202
    Log            ${RESPOSTAREQUEST1.text}


    ${HEADERS2}  Create Dictionary   accept=application/json  User-Agent=bc552b74-d12a-4a57-8689-20861a5dc5d5  
    ${RESPOSTAREQUEST2}  Put Request     newSession      communication/v1/endpoints/${id}/trustedEndpoint
    ...                                headers=${HEADERS2} 
    Should Be Equal As Strings    ${RESPOSTAREQUEST2.status_code}    202
    Log            ${RESPOSTAREQUEST2.text}


    ${HEADERS3}  Create Dictionary   accept=application/json  User-Agent=3086c3e2-7d21-47e1-9fc6-71cf3cb0608f 
    ${RESPOSTAREQUEST3}  Get Request     newSession      mfa/v1/webauthn/makeCredential/${id} 
    ...                                headers=${HEADERS3} 
    Should Be Equal As Strings    ${RESPOSTAREQUEST3.status_code}    200
    Log            ${RESPOSTAREQUEST3.text}


    ${HEADERS4}  Create Dictionary   accept=application/json  User-Agent=8995598e-deb3-470a-81d3-0b3c3a1068df  Content-Type=application/json-patch+json  Content-Type=text/plain
    ${PARAMS4}=  Catenate    
    ...     {
    ...         "id": "string",
    ...         "rawId": "string",
    ...         "type": "public-key",
    ...         "response": {
    ...         "attestationObject": "string",
    ...         "clientDataJson": "string"
    ...         },
    ...         "extensions": {
    ...         "example.extension": {},
    ...          "appid": true,
    ...         "txAuthSimple": "string",
    ...         "txAuthGenericArg": "string",
    ...         "authnSel": true,
    ...         "exts": [
    ...             "string"
    ...         ],
    ...         "uvi": "string",
    ...         "loc": {
    ...             "latitude": 0,
    ...             "longitude": 0,
    ...             "horizontalAccuracy": 0,
    ...             "verticalAccuracy": 0,
    ...             "speed": 0,
    ...             "course": 0,
    ...             "altitude": 0
    ...         },
    ...         "uvm": [
    ...             [
    ...              0
    ...              ]
    ...         ],
    ...         "biometricPerfBounds": true
    ...          }
    ...        }

    ${RESPOSTAREQUEST4}  Put Request     newSession      mfa/v1/webauthn/makeCredential/${id} 
    ...                                  headers=${HEADERS4}
    ...                                  data=${PARAMS4}
    Should Be Equal As Strings    ${RESPOSTAREQUEST4.status_code}    200
    Log            ${RESPOSTAREQUEST4.text}


    ${HEADERS5}  Create Dictionary   accept=application/json  User-Agent=14c277cf-cc21-43f4-9bd5-6c1dde633c1f
    ${RESPOSTAREQUEST5}  Put Request     newSession      mfa/v1/webauthn/${id} 
    ...                                headers=${HEADERS5} 
    Should Be Equal As Strings    ${RESPOSTAREQUEST5.status_code}    200
    Log            ${RESPOSTAREQUEST3.text}