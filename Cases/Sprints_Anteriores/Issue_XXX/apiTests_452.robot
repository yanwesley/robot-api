*** Settings ***
Documentation
...    As usuário ou dependente do aplicativo Banco Bari,
...    Want garantir que na recuperação de senha, a chave primária seja o CPF,
...    So para que não seja feita a validação pelo e-mail do usuário.
Default Tags	automacao	sprint29
Resource	  ${EXECDIR}/Resources/main.resource
Suite Setup   Abrir conexao  https://api.qa.bancobari.com.br
*** Test Cases ***

Consulta para garantia da utilização do paramêtro realizado por CPF
    [Template]    Validar teste
    61179528301   123Abc     SMS
                                                

*** Keywords ***

Validar teste   
    [Arguments]           ${cpf}        ${password}    ${method}
    Given usuario_solicitou_recuperacao_de_senha  ${cpf}   ${password}   ${method}
	When seus_dados_foram_registrados_no_banco_de_dados_pela_solicitacao
	Then campo_chave_do_parametro_devera_ser_o_cpf



Given usuario_solicitou_recuperacao_de_senha
    [Arguments]           ${cpf}    ${password}   ${method}
    Set Test Variable     ${cpf} 

    ${consulta}                     Consulta Dynamo    Barigui.Services.IdentityServer_Users    Id    ${cpf}
    ${passwordAntigoConsulta}             Set Variable     ${consulta[0] ['Password']}
    ${passwordAntigoString}        Convert To String       ${passwordAntigoConsulta}  
    Set Test Variable     ${passwordAntigoString} 



    ${consulta1}           Consulta Dynamo    Barigui.Services.IdentityServer_Users    Id    ${cpf}
    ${token1}              Set Variable     ${consulta1[0] ['PasswordResetToken']}
    Log                    ${token1}
    


    ${HEADERS1}  Create Dictionary   accept=application/json  User-Agent=a6401c40-3f4e-4bc9-8012-6e018fd2337d
    ${RESPOSTAREQUEST1}  Put Request     newSession      auth/v3/accounts/${cpf}/recover-method/${method}
    ...                                headers=${HEADERS1} 
    Should Be Equal As Strings    ${RESPOSTAREQUEST1.status_code}    200
    Log            ${RESPOSTAREQUEST1.text}




    :FOR  ${index}  IN RANGE  999999
    \  ${token2}  Consulta Token  
    \  Exit For Loop If  ${token1}!=${token2}
    \  Log   ${token1}
    \  Log   ${token2}

 

    ${PARAMS2}  Create Dictionary  token=${token2}  
    ${HEADERS2}  Create Dictionary  accept=application/json  Content-Type=application/json-patch+json  User-Agent=8288ad88-1171-4087-a2f6-4d53826cedd1
    ${RESPOSTAREQUEST2}  Put Request   newSession    auth/v3/accounts/${cpf}/validate-token
    ...                                data=${PARAMS2}
    ...                                headers=${HEADERS2} 
    Should Be Equal As Strings    ${RESPOSTAREQUEST2.status_code}    204
    Log            ${RESPOSTAREQUEST2.text}



    ${consulta}           Consulta Dynamo    Barigui.Services.IdentityServer_Users    Id    ${cpf}
    ${token}              Set Variable     ${consulta[0] ['PasswordResetToken']}
    Log                   ${token}

      


    ${PARAMS3}  Create Dictionary  token=${token2}  password=${password}
    ${HEADERS3}  Create Dictionary  accept=application/json  User-Agent=f2afc027-6347-4fbd-a484-9bd517b70394  Content-Type=application/json-patch+json
    ${RESPOSTAREQUEST3}  Post Request  newSession    auth/v3/accounts/${cpf}/password
    ...                                data=${PARAMS3}
    ...                                headers=${HEADERS3} 
    Should Be Equal As Strings    ${RESPOSTAREQUEST3.status_code}    200
    Log            ${RESPOSTAREQUEST3}

When seus_dados_foram_registrados_no_banco_de_dados_pela_solicitacao
    :FOR  ${index}  IN RANGE  999999
        \  ${passwordNovoString}  Consulta Password  
        \  Exit For Loop If  "${passwordNovoString}" != "${passwordAntigoString}"
        \  Log   ${passwordNovoString} 
        \  Log   ${passwordAntigoString}
    

Consulta Token
    ${consulta2}           Consulta Dynamo    Barigui.Services.IdentityServer_Users    Id    ${cpf}
    ${token2}              Set Variable     ${consulta2[0] ['PasswordResetToken']}
    Log                    ${token2}
    [Return]               ${token2}

Consulta Password
    ${consulta}                     Consulta Dynamo    Barigui.Services.IdentityServer_Users    Id    ${cpf}
    ${passwordNovoConsulta}         Set Variable     ${consulta[0] ['Password']}
    Log                    ${passwordNovoConsulta}
    ${passwordNovoString}        Convert To String       ${passwordNovoConsulta}  
    [Return]               ${passwordNovoString}

Then campo_chave_do_parametro_devera_ser_o_cpf
    Log         "As consultas e requisições anteriores foram feitas com CPF como Chave"