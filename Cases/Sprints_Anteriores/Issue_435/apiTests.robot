*** Settings ***
Documentation
...  As usuário do aplicativo Banco Bari,
...  Want ter acesso aos termos de uso, política de privacidade e contratos em versões disponíveis no aplicativo,
...  So atualizar o aceite nos termos de uso, política de privacidade e/ou contratos.
Default Tags   Sprint30
Resource	   ${EXECDIR}/Resources/main.resource

*** Test Cases ***

Aceite e leitura dos 3 tres documentos atualizados
	[Tags]	backend  automation
	Given usuario_lead_que_concluiu_o_fluxo_de_onboarding_criando_o_customer
	When verifica_os_contratos_pendentes_nao_aceitos_pelo_usuario
	Then devera_ser_registrado_os_aceites_dos_contratos
    And atualizacao_das_informacoes_dos_contratos_aceitos

*** Keyword ***

usuario_lead_que_concluiu_o_fluxo_de_onboarding_criando_o_customer
    Log  O usuário será resultado de uma consulta realizada na tabela Barigui.Services.Customer_Customers

verifica_os_contratos_pendentes_nao_aceitos_pelo_usuario
    ${customerId}   Consulta Dynamo    Barigui.Services.Customer_Customers     Id      03881948120
    #Modificar valor fixo do Id acima 
    ${customerId}   Set Variable       ${customerId[0]['Id']}
    ${guid}         Gera Guid
    ${token}        Gera Token Customer
    Set Test Variable   ${guid}
    Set Test Variable   ${token}
    ${PARAMS}       Create Dictionary            
    ${HEADERS}      Create Dictionary            accept=application/json  User-Agent=${guid}  Authorization=${token}  
    Abrir conexao   https://api.qa.bancobari.com.br
    ${RESPOSTAREQUEST}     Get Request           new session         customer/v1/terms/${customerId}/termsOfUse
    ...                                          data=${PARAMS}
    ...                                          headers=${HEADERS}
    Set Test Variable  ${RESPOSTAREQUEST}
    Log   ${RESPOSTAREQUEST.text}
    @{json_length}  Count json  ${RESPOSTAREQUEST.text}
    ${typeDic}  Create List
    Set Test Variable  ${typeDic}
    ${contractDic}  Create List
    Set Test Variable  ${contractDic}
    ${json_value}  Set Variable  ${RESPOSTAREQUEST.json()}
    :FOR  ${int}  IN  @{json_length}    
    \  Insert Into List  ${typeDic}  ${int}  ${json_value[${int}]['type']}
    \  Insert Into List  ${contractDic}  ${int}  ${json_value[${int}]['contract']}
    Log  ${typeDic}
    Log  ${contractDic}
    

devera_ser_registrado_os_aceites_dos_contratos
    ${customerId}   Consulta Dynamo    Barigui.Services.Customer_Customers     Id      03881948120
    ${customerId}   Set Variable       ${customerId[0]['Id']}
    ${guid}         Gera Guid
    ${token}        Gera Token Customer
    Set Test Variable   ${guid}
    Set Test Variable   ${token}
    ${count}  Get Length  ${typeDic}
    :FOR  ${index}  IN RANGE  0  ${count}
    \  ${contract}  Get From List  ${contractDic}  ${index}
    \  ${type}  Get From List  ${typeDic}  ${index}
    \  ${PARAMS}       Create Dictionary            contract=${contract}  type=${type}
    \  ${HEADERS}      Create Dictionary            accept=application/json  User-Agent=${guid}  Authorization=${token}  Content-Type=application/json-patch+json
    \  Abrir conexao   https://api.qa.bancobari.com.br
    \  ${RESPOSTAREQUEST}     Put Request           new session         customer/v1/terms/${customerId}
    \  ...                                          data=${PARAMS}
    \  ...                                          headers=${HEADERS}
    \  Set Test Variable  ${RESPOSTAREQUEST}

atualizacao_das_informacoes_dos_contratos_aceitos
    ${resultAceptTerms}       Consulta Dynamo    Barigui.Services.Customer_Terms     Id      03881948120
    ${resultAceptTerms}       Set Variable       ${resultAceptTerms[0]['TermsAcceptance']}

Count json
    [Arguments]  ${string}
    ${response_list}  Split String  ${string}  type
    ${item_count}  Get Length  ${response_list}
    ${item_count}  Evaluate  ${item_count} - 1
    ${json_count}  Create List
    :FOR  ${index}  IN RANGE  0  ${item_count}
    \  Append To List  ${json_count}  ${index}
    \    
    [Return]        ${json_count}
