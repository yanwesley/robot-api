*** Settings ***
Resource	${EXECDIR}/Resources/main.resource
Suite Setup   Abrir conexao  https://api.qa.bancobari.com.br

*** Test Cases ***

# Verificar se o usuario nao tem o 2FA ativado
# 	Given usuario_que_ja_possui_conta_aberta
# 	And nao_passou_pela_ativacao_do_2_fa
# 	When quando_for_realizado_a_chamada_da_api
# 	Then sera_mostrado_que_o_usuario_precisa_realizar_a_ativacao_do_2_fa

Verificar se o usuario tem o 2FA ativado
	Given usuario_que_ja_possui_conta_aberta
	And passou_pela_ativacao_do_2_fa
	When quando_for_realizado_a_chamada_da_api
	Then sera_mostrado_que_o_usuario_nao_precisa_realizar_a_ativacao_do_2_fa

*** Keywords ***
Given usuario_que_ja_possui_conta_aberta
    Log  usuario possui conta aberta

And nao_passou_pela_ativacao_do_2_fa
    Log  usuario não ativou o 2FA

And passou_pela_ativacao_do_2_fa
    Log  usuario ativou o 2FA

When quando_for_realizado_a_chamada_da_api
    ${token}  Gera Token  06537262950  463166Yan

    ${HEADERS1}  Create Dictionary   accept=application/json  authorization=${token}
    ${RESPOSTAREQUEST1}  Get Request     newSession      featureflag/v1/customerScope
    ...                                headers=${HEADERS1} 
    Should Be Equal As Strings    ${RESPOSTAREQUEST1.status_code}    200
    Log            ${RESPOSTAREQUEST1.text}
    ${resposta}  Set Variable   ${RESPOSTAREQUEST1.json()}
    Set Test Variable    ${resposta}

Then sera_mostrado_que_o_usuario_nao_precisa_realizar_a_ativacao_do_2_fa
    Should Be Equal  ${resposta[1]['code']}  MFA
    ${isActiveConvert}  Convert To String    ${resposta[1]['isActive']}
    Should Be Equal  ${isActiveConvert}  False

Then sera_mostrado_que_o_usuario_precisa_realizar_a_ativacao_do_2_fa
    Should Be Equal  ${resposta[1]['code']}  MFA
    ${isActiveConvert}  Convert To String    ${resposta[1]['isActive']}
    Should Be Equal  ${isActiveConvert}  True
    