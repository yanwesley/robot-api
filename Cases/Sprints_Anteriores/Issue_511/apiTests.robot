*** Settings ***
Resource	${EXECDIR}/Resources/main.resource
Suite Setup   Abrir conexao  https://api.qa.bancobari.com.br

*** Test Cases ***

Scenario: Envio dos dados iniciais via api ao Salesforce Testando a integração 
        #Given usuário  inseriu todas informações dos dados basicos do onboarding via apis
        #When consultar os dados na base do Salesforce
        #Then os dados basicos do usuário constarão na base do Salesforce

    #gera guid
    ${guidLead}  Gera Guid
    Set Test Variable  ${guidLead}
    #gera guid   
    ${guidDevice}  Gera Guid
    Set Test Variable  ${guidDevice}
    #gerando cpf
    ${CPFnum}  FakerLibrary.cpf
    ${CPF}  Remove String  ${CPFnum}  .  -
    Set Test Variable  ${CPF}
    #gerando nome
    ${name}  FakerLibrary.name
    Set Test Variable  ${name}
    ${contains}=  Evaluate   "." in """${name}"""    
    Run Keyword If  ${contains}  Split Name  ${name}
    ...  ELSE  No Split Name
    #gerando e-mail
    ${email}  FakerLibrary.email

    #gerando telefone
    ${phone}  FakerLibrary.Phone Number
    ${numbers}=    Generate Random String    length=7    chars=[NUMBERS]
    ${telefone}  catenate  SEPARATOR=  4199  ${numbers}

    #gerando data de nascimento
    ${dtnasc}  FakerLibrary.Date Of Birth  minimum_age=18  maximum_age=70
    
    01_GET_/v1/Leads/contract?types=InformationPrivacy,SCRAcceptance
    02_PUT_{{url}}/v1/Leads/{{lead_id}}  ${guidLead}  ${NOME}  ${CPF}
    03_PUT_{{url}}/v1/Leads/{{lead_id}}/BirthDate  ${guidLead}  ${dtnasc}
    04_PUT_{{url}}/v1/Leads/{{lead_id}}/geolocation  ${guidLead}
    05_PUT_{{url}}/v1/Leads/{{lead_id}}/Email  ${guidLead}  ${email}
    07_PUT_{{url}}/v1/Leads/{{lead_id}}/mobilePhoneNumber  ${guidLead}  ${telefone}
    ${token}  Gera Token SF
    Integracao SF  ${CPF}  ${name}  ${email}  ${telefone}  ${dtnasc}  ${token}    
    Consulta SF  ${token}  ${CPF}

***Keywords***

Split Name
    [Arguments]  ${name}
    ${nameList}  Split String  ${name}  .
    ${NOME}  Set Variable  ${nameList[1]}
    Set Test Variable  ${NOME}

No Split Name
    ${NOME}  Set Variable  ${name}
    Set Test Variable  ${NOME}

Gera Token SF
    Abrir conexao   https://test.salesforce.com
    ${HEADERS}  Create Dictionary
    ...  Content-Type=application/x-www-form-urlencoded
    ${PARAMS}  Create Dictionary
    ...  grant_type=password
    ...  client_id=3MVG9Vik22TUgUphKhs9dOlQTssExvUo53Ez3Jl1IAYrDcy5Ete.gED_B5Or1jjCyQSv_hKXViKgmnTym9R1T
    ...  client_secret=FA050230A427D30689D08F4DA22F9ABAFCE5AF0602CFC65A5D940DF6D7D5A4F4
    ...  username=admin.integracao@bari.com.br.qabari
    ...  password=P@ssIntegracao@5
    Set Test Variable  ${HEADERS}
    ${RESPOSTAREQUEST}     Post Request     newsession      services/oauth2/token
    ...     data=${PARAMS}
    ...     headers=${HEADERS}
    Log                          ${RESPOSTAREQUEST.text}
    ${json_value}  Set Variable  ${RESPOSTAREQUEST.json()}
    ${token}  Catenate  Bearer  ${json_value['access_token']}
    Set Test Variable  ${token}
    [Return]        ${token}

Consulta SF
    [Arguments]  ${token}  ${CPF}
    Abrir conexao   https://barigui--qabari.my.salesforce.com
    ${payload}   Create Dictionary    contract=string   type=string
    ${json}  Convert Dic to Json  ${payload}  
    ${HEADERS}  Create Dictionary
    ...     Authorization=${token}    
    ${RESPOSTA}     Get Request     newsession      services/apexrest/Bari/WS_Get_Account?individualRegistrationNumber=${CPF}  
    ...     headers=${HEADERS}
    Log                             ${RESPOSTA.text}
    Should Be Equal As Strings      ${RESPOSTA.status_code}    200

Integracao SF
    [Arguments]  ${CPF}  ${name}  ${email}  ${telefone}  ${dtnasc}  ${token}
    ${nome}  Split String  ${name}  
    Abrir Conexao  https://barigui--qabari.my.salesforce.com
    ${json_string}=    catenate
    ...     {
    ...         "email": "${email}",
    ...         "firstName": "${nome[0]}",
    ...         "lastName": "${nome[1]}",
    ...         "mobilePhoneNumber": "${telefone}",
    ...         "individualRegistrationNumber": "${CPF}",
    ...         "birthDate": "${dtnasc}",
    ...         "leadStatus": "new",
    ...         "leadID": "${guidLead}",
    ...         "onboardingStep": "started"
    ...     }
    
    log  ${json_string}    
    ${HEADERS}  Create Dictionary
    ...     Content-Type=application/json
    ...     Authorization=${token}    
    ${RESPOSTA}     Put Request     newsession      services/apexrest/Bari/WS_Account
    ...     data=${json_string}
    ...     headers=${HEADERS}
    Log                             ${RESPOSTA.text}
    Should Be Equal As Strings      ${RESPOSTA.status_code}    200

