*** Settings ***
Documentation
...  As usuário do backoffice de cadastro de clientes do Banco Bari,
...  Want saber se CPF faz parte da lista OFAC,
...  So atender processos regulatórios do Banco Central e impedir a abertura de conta dos usuário presente nas listas.
Default Tags	Sprint27
Resource	${EXECDIR}/Resources/main.resource

*** Test Case ***

Consultando os dados dos usuario que nao esta na lista OFAC
	[Tags]	backend	regression	automation
	Given consultar_usuario_na_lista_ofac
	When tem_cadastro_negativo
	Then ocorre_o_retorno_da_resposta_como_negativo
	And grava_o_status_da_resposta_do_ofac_negativo_na_tabela

Consultando os dados dos usuario que esta na lista OFAC
	[Tags]	backend	regression	automation
	Given consultar_usuario_na_lista_ofac
	When consta_na_lista_de_ofac_com_grau_de_similaridade_maior_que_95
	Then ocorre_o_retorno_da_resposta_como_positivo
	And grava_o_status_da_resposta_como_possivel_positivo_para_ofac
	And envia_para_mesa_de_cadastro

*** Keywords ***

consultar_usuario_na_lista_ofac    
    ${item}                Consulta Dynamo Attribute              Barigui.Services.Customer_Customers    Compliance
    Set Test Variable  ${item}
    log                    ${item}
tem_cadastro_negativo
    Log  Por volume de massas não é possível buscar essa informação específica. 
ocorre_o_retorno_da_resposta_como_negativo
    Log  Como não é algo impeditivo pra o cadastro do cliente nos abstemos de encontrar cliente com cenário negativo.
grava_o_status_da_resposta_do_ofac_negativo_na_tabela
    :FOR    ${COUNT}    IN RANGE    0    5
    \  Valida compliance  ${item}  ${COUNT}  3  OFACANDPEP
consta_na_lista_de_ofac_com_grau_de_similaridade_maior_que_95
    Log  Por volume de massas não é possível buscar essa informação específica. 
ocorre_o_retorno_da_resposta_como_positivo
    Log  Como não é algo impeditivo pra o cadastro do cliente nos abstemos de encontrar cliente com cenário negativo.
grava_o_status_da_resposta_como_possivel_positivo_para_ofac
    :FOR    ${COUNT}    IN RANGE    0    5
    \  Valida compliance  ${item}  ${COUNT}  3  OFACANDPEP
envia_para_mesa_de_cadastro
    Log  A mesa pode buscar esta informação diretamente na base de dados