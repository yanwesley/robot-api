*** Settings ***
Resource	${EXECDIR}/Resources/main.resource
Suite Setup   Abrir conexao  https://api.qa.bancobari.com.br

*** Test Cases ***

Validar o email na troca de device
    bypass na API email

*** Keywords ***

bypass na API email
    ${token}  Gera Token  06537262950  123Abc
    ${id}  Set Variable  06537262950
    ${mode}  set Variable  email 
    ${payload}   Create Dictionary    code=string   
    ${json}  Convert Dic to Json  ${payload}
    ${HEADER}  Create Dictionary   
    ...            accept=application/json  
    ...            authorization=${token}
    ...            Content-Type=application/json-patch+json
    ${RESPOSTAREQUEST}  Put Request     newSession      mfa/v1/webauthn/recovery/${mode}/${id}/code
    ...                                data=${json}
    ...                                headers=${HEADER} 
    Should Be Equal As Strings    ${RESPOSTAREQUEST.status_code}    200
    Log            ${RESPOSTAREQUEST.text}
 