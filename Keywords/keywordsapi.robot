*** Keywords ***
### Requisições

que_eu_defino_o_endpoint_para_consulta_de_cpf
	[Arguments]	${endpoint}
    Set Test Variable  ${endpoint}

eu_defino_o_request_header
    Create Dictionary    content-type=application/json    

faco_uma_chamada_get
    [Arguments]     ${cpf}
    ${RESPOSTAREQUEST}     Get Request     customer     /v1/customers/${cpf}/checkUserInvited
    Log                           ${RESPOSTAREQUEST.text}
    Set Test Variable             ${RESPOSTAREQUEST}

### Validações

recebo_a_resposta_referente_ao_cpf
    [Arguments]     ${resposta}
    Log             ${resposta}
    Dictionary Should Contain Item    ${RESPOSTAREQUEST.json()}    status             ${resposta}	

### Consultas

