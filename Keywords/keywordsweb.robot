*** Keywords ***
que_eu_sou_cliente_p1
    [Arguments]	${p1}=
    Log                              Cliente cadastrado sob o cpf: ${p1}

preencho_campo_com_p1
    [Arguments]	${p1}=
    Input Text                       id=username                            ${p1}

o_sistema_exibe_a_mensagem_p1
    [Arguments]	${p1}=
    Wait Until Element Is Visible    xpath=//*[contains(text(),'${p1}')]    timeout=30                       error=None
    Element Should Be Visible        xpath=//*[contains(text(),'${p1}')]
    

caso_nao_seja_invalido_exibe_o_campo_p1_e_o_botao_p2
    [Arguments]	${p1}=	${p2}=

    Run Keyword Unless               '${p1}'=='None'                        Wait Until Element Is Visible    xpath=//*[contains(text(),'${p1}')]    timeout=10    error=None
    Run Keyword Unless               '${p1}'=='None'                        Element Should Be Visible        xpath=//*[contains(text(),'${p1}')]
    Run Keyword Unless               '${p2}'=='None'                        Wait Until Element Is Visible    xpath=//*[contains(text(),'${p2}')]    timeout=10    error=None
    Run Keyword Unless               '${p2}'=='None'                        Element Should Be Visible        xpath=//*[contains(text(),'${p2}')]


